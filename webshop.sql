-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 14, 2020 at 09:30 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `author_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `author_name`, `url_author`, `author_info`, `avatar_image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 'Quỷ Cổ Nữ', 'quy-co-nu', 'Quỷ Cổ Nữ (鬼古女) là bút danh chung của cặp vợ chồng tác giả người Trung Quốc đang sống ở thung lũng Silicon, California, Mỹ. Quỷ Cổ Nữ được xưng tụng với danh hiệu \"Stephen King Của Trung Quốc\" cùng với số phiếu bình chọn đứng đầu là một trong 10 tiểu thuyết gia kinh dị nổi tiếng nhất.[1]\r\n\r\nVợ là Dư Dương là một kỹ sư phần mềm cấp cao, còn chồng là Dị Minh làm nhà nghiên cứu trong các chương trình y tế cộng đồng. Cả hai cho ra đời hàng loạt các tác phẩm thuộc thể loại kinh dị giả tưởng được phổ biến trên mạng trực tuyến Siêu Vượng (Chao Wang).[2]\r\n\r\nQuỷ Cổ Nữ là cặp đối tác vàng với các tiểu thuyết có nội dung bí ẩn, hồi hộp dưới bút danh sáng tạo. Cả hai vợ chồng tốt nghiệp cùng một trường đại học, đến Mỹ du học vào cuối những năm 90 và đến sống gần San Francisco sau khi định cư. Hai vợ chồng ngoài việc quảng bá sách thì rất ít tiếp xúc với truyền thông. Do thành công của \"Toái Kiểm\" (Khuôn mặt bị vỡ) hay còn được biết đến với tên \"Kỳ án ánh trăng\" năm 2005, cuốn sách đã tạo hiệu ứng \"Năm đầu tiên Trung Quốc hồi hộp chờ đợi\".', '28072020-748-tai-xuong.jpg', NULL, '2020-07-20 02:17:46', '2020-07-28 02:25:42'),
(4, 'Fujiko.F.Fujio', 'fujiko-f-fujio', 'Fujiko F. Fujio (藤子・F・不二雄 Fujiko Efu Fujio) Tên thật của ông là Hiroshi Fujimoto (藤本 弘 Fujimoto Hiroshi) là 1 trong 2 người đã tạo nên bộ truyện tranh Đôrêmon nổi tiếng toàn thế giới.\r\nHiroshi Fujimoto đã bộc lộ năng khiếu hội họa ngay từ bé khi mà mới lớp 5 ông đã tự vẽ nhân vật theo quan sát và tưởng tượng của mình. Vào trung học ông đã bắt đầu vẽ manga cho các nhà xuất bản ở Nhật', 'default.jpg', NULL, '2020-07-28 03:38:09', '2020-07-28 03:38:09'),
(5, 'Kim Dung', 'kim-dung', 'Kim Dung (10 tháng 3 năm 1924 – 30 tháng 10 năm 2018) là một trong những nhà văn có tầm ảnh hưởng nhất đến văn học Trung Quốc hiện đại. Ông còn là người đồng sáng lập của nhật báo Hồng Kông Minh Báo, ra đời năm 1959 và là tổng biên tập đầu tiên của tờ báo này.\r\n\r\nTừ năm 1955 đến năm 1972, ông đã viết tổng cộng 14 cuốn tiểu thuyết và 1 truyện ngắn. Sự nổi tiếng của những bộ truyện đó khiến ông được xem là người viết tiểu thuyết võ hiệp thành công nhất lịch sử. 300 triệu bản in (chưa tính một lượng rất lớn những bản lậu) đã đến tay độc giả của Trung Hoa đại lục, Hồng Kông, Đài Loan, châu Á và đã được dịch ra các thứ tiếng Việt, Hàn, Nhật, Thái, Anh, Pháp, Indonesia. Tác phẩm của ông đã được chuyển thể thành phim truyền hình, trò chơi điện tử.\r\n\r\nTên ông được đặt cho tiểu hành tinh 10930 Jinyong (1998 CR2), là tiểu hành tinh được tìm ra trùng với ngày sinh âm lịch của ông (6 tháng 2).[2] Tháng 2 năm 2006, ông được độc giả gọi là nhà văn được yêu thích nhất tại Trung Quốc. Ông là người sùng đạo Phật, rất yêu thiên nhiên và động vật, đặc biệt ông có nuôi một con chó Trùng Khánh.', 'default.jpg', NULL, '2020-07-28 03:55:29', '2020-07-28 03:55:29'),
(6, 'Vong Ngữ', 'vong-ngu', 'Vong Ngữ hiện là một trong những tác giả văn học mạng vô cùng nổi tiếng, hiện tại ông đang là tác giả Bạch Kim trên Qidian. Trước khi tham gia viết truyện, Vong Ngữ đã tốt nghiệp ở Học viện công nghệ Vô Tích và tiếp tục học đại học ngành Pháp luật; ông từng làm một nhân viên văn phòng bình thường ở Từ Châu.\r\n\r\nHọ tên thật: Đinh Lăng Thao\r\n\r\nBút danh: Vong Ngữ\r\n\r\nNơi sinh: Thành phố Từ Châu – Tỉnh Giang Tô\r\n\r\nNăm sinh: 10/1976\r\n\r\nNghề nghiệp: Tác giả văn học mạng\r\n\r\nTốt nghiệp: Học viện công nghệ Vô Tích', 'default.jpg', NULL, '2020-07-28 03:56:38', '2020-07-28 03:56:38'),
(7, 'Thiên tằm thổ đậu', 'thien-tam-tho-dau', 'Thiên Tằm Thổ Đậu bắt đầu viết tiểu thuyết khi còn mới là học sinh trung học, bắt đầu từ nhà văn bạch kim, đại diện cho các nhà văn mới trên giới văn đàn mạng.\r\n\r\nĐậu Đậu bắt đầu sự nghiệp năm 2008 với tác phẩm đầu tay: “Ma Thú Kiếm Thánh Dị Giới Tung Hoành”. Tác phẩm này được viết vào năm 2007, ra đời đã giúp Đậu Đậu trở thành một trong những nhà văn mạng nổi tiếng hàng đầu Trung Quốc.', 'default.jpg', NULL, '2020-07-28 06:11:31', '2020-07-28 07:41:12'),
(8, 'Diệp Lạc Vô Tâm', 'diep-lac-vo-tam', 'Diệp Lạc Vô Tâm là một nữ tác giả ngôn tình rất được yêu mến, những sáng tác của cô có ảnh hưởng rất lớn tới nền văn học mạng Trung Quốc. Cô được mệnh danh là một trong bốn tứ đại tác giả \"Bảo chứng ngôn tình\" của Trung Quốc nổi tiếng nhất hiện tại bên cạnh Tân Di Ổ, Cố Mạn và Phỉ Ngã Tư Tồn. Các cuốn tiểu thuyết của Diệp Lạc Vô Tâm chủ yếu tập trung vào những câu chuyện tình yêu không mấy suôn sẻ, nhưng lại \"khắc cốt ghi tâm\" suốt đời. Nhất là dàn nam chính đúng chuẩn soái ca khiến fan đồng loạt tan chảy', 'default.jpg', NULL, '2020-07-28 06:17:20', '2020-07-28 07:41:36');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `quatity` int(30) DEFAULT NULL,
  `bought` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `product_id`, `quatity`, `bought`, `status`, `created_at`, `updated_at`) VALUES
(58, 2, 57, 20, '1', NULL, '2020-08-14 02:58:29', '2020-08-14 02:59:01');

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_cetegory` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `category_name`, `url_cetegory`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Truyện ma', 'truyen-ma', NULL, NULL, '2020-07-16 10:39:54', '2020-07-20 07:56:11'),
(2, 'Truyện Trung Quốc', 'truyen-trung-quoc', NULL, NULL, '2020-07-16 10:40:31', '2020-07-28 03:52:49'),
(3, 'Truyện tranh', 'truyen-tranh', NULL, NULL, '2020-07-20 03:09:38', '2020-07-28 02:09:46');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `vote` int(11) NOT NULL DEFAULT 0,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `product_id`, `title`, `content`, `vote`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 61, 'saas', 'sasas', 5, NULL, NULL, '2020-07-29 02:54:56', '2020-07-29 02:54:56'),
(2, 2, 61, 'sâs', 'asấ', 4, NULL, NULL, '2020-07-29 02:58:19', '2020-07-29 02:58:19'),
(3, 2, 63, 'Cũng được', 'Giấy tốt', 4, NULL, NULL, '2020-07-30 02:35:29', '2020-07-30 02:35:29');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `company_name`, `url_company`, `created_at`, `updated_at`) VALUES
(1, 'Kim đồng', 'kim-dong', NULL, NULL),
(2, 'Hồng Đức', 'hong-duc', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detail_import`
--

CREATE TABLE `detail_import` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `import_id` bigint(20) UNSIGNED NOT NULL,
  `quatity` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_import`
--

INSERT INTO `detail_import` (`id`, `product_id`, `import_id`, `quatity`, `price`, `created_at`, `updated_at`) VALUES
(1, 51, 3, 12, 21.00, '2020-08-12 04:22:30', '2020-08-12 04:22:30'),
(2, 52, 3, 21, 2121.00, '2020-08-12 04:22:30', '2020-08-12 04:22:30'),
(4, 52, 5, 1, 10000.00, '2020-08-12 07:04:29', '2020-08-12 07:04:29'),
(5, 21, 6, 10, 21000.00, '2020-08-12 08:43:27', '2020-08-12 08:43:27'),
(6, 51, 7, 10, 10000.00, '2020-08-12 08:43:51', '2020-08-12 08:43:51'),
(7, 52, 7, 20, 1000.00, '2020-08-12 08:43:51', '2020-08-12 08:43:51'),
(8, 55, 8, 12, 100000.00, '2020-08-12 17:12:15', '2020-08-12 17:12:15');

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE `detail_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL,
  `quatity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`id`, `order_id`, `product_id`, `price`, `quatity`, `created_at`, `updated_at`) VALUES
(8, 10, 20, 70000.00, 2, '2020-08-04 08:53:56', '2020-08-04 08:53:56'),
(9, 10, 63, 100000.00, 1, '2020-08-04 08:53:56', '2020-08-04 08:53:56'),
(10, 10, 62, 100000.00, 1, '2020-08-04 08:53:56', '2020-08-04 08:53:56'),
(11, 16, 60, 16000.00, 2, '2020-08-13 06:56:17', '2020-08-13 06:56:17'),
(12, 16, 53, 165000.00, 1, '2020-08-13 06:56:17', '2020-08-13 06:56:17'),
(13, 17, 58, 16000.00, 3, '2020-08-13 07:12:45', '2020-08-13 07:12:45'),
(14, 17, 65, 123567.00, 5, '2020-08-13 07:12:45', '2020-08-13 07:12:45'),
(24, 35, 59, 16000.00, 1, '2020-08-13 18:16:58', '2020-08-13 18:16:58'),
(25, 36, 21, 76000.00, 1, '2020-08-13 18:18:51', '2020-08-13 18:18:51'),
(26, 37, 55, 16000.00, 1, '2020-08-13 18:31:39', '2020-08-13 18:31:39'),
(28, 39, 51, 312000.00, 1, '2020-08-13 18:55:47', '2020-08-13 18:55:47');

-- --------------------------------------------------------

--
-- Table structure for table `enjoy`
--

CREATE TABLE `enjoy` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `search_content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `import`
--

CREATE TABLE `import` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `import`
--

INSERT INTO `import` (`id`, `provider_id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 3, 1, 'accept', '2020-08-12 04:22:30', '2020-08-12 17:01:30'),
(5, 1, 1, 'cancel', '2020-08-12 07:04:29', '2020-08-12 17:04:40'),
(6, 1, 1, NULL, '2020-08-12 08:43:27', '2020-08-12 08:43:27'),
(7, 3, 1, NULL, '2020-08-12 08:43:51', '2020-08-12 08:43:51'),
(8, 1, 1, NULL, '2020-08-12 17:12:15', '2020-08-12 17:12:15');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from_user` bigint(20) UNSIGNED NOT NULL,
  `to_user` bigint(20) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(45, '2014_10_12_100000_create_password_resets_table', 1),
(46, '2014_10_12_100000_create_users_table', 1),
(47, '2019_08_19_000000_create_failed_jobs_table', 1),
(48, '2020_07_01_000000_create_categorys_table', 1),
(49, '2020_07_01_022105_create_authors_table', 1),
(50, '2020_07_01_024815_create_company_table', 1),
(51, '2020_07_01_165720_create_subcategorys_table', 1),
(52, '2020_07_05_040357_create_products_table', 1),
(53, '2020_07_05_073555_create_product_images_table', 1),
(54, '2020_07_05_152655_create_comments_table', 1),
(55, '2020_07_05_162417_create_cart_table', 1),
(56, '2020_07_06_031125_create_promotion_table', 1),
(57, '2020_07_06_095117_create_notification_table', 1),
(58, '2020_07_06_101559_create_persons_table', 1),
(59, '2020_07_06_143240_create_messages_table', 1),
(60, '2020_07_07_025247_create_order_table', 1),
(61, '2020_07_07_025406_create_detail_order_table', 1),
(62, '2020_07_07_031222_create_provider_table', 1),
(63, '2020_07_07_031359_create_import_table', 1),
(64, '2020_07_07_031625_create_detail_import_table', 1),
(65, '2020_07_08_011919_create_enjoy_table', 1),
(66, '2020_07_08_014000_create_reply_comments_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from_user` bigint(20) UNSIGNED NOT NULL,
  `to_user` bigint(20) UNSIGNED NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `promotion_id` bigint(20) UNSIGNED DEFAULT NULL,
  `code_order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ship_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `checkout` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `user_id`, `promotion_id`, `code_order`, `receiver_name`, `receiver_phone`, `ship_address`, `note`, `checkout`, `status`, `created_at`, `updated_at`) VALUES
(10, 2, NULL, NULL, 'Trần Thị C', '0399418833', 'toà nhà keangnam', NULL, 'active', 'cancel', '2020-08-04 08:53:56', '2020-08-13 14:09:08'),
(16, 2, NULL, NULL, 'Nguyễn Duy Huỳnh', '098765432', 'hà nội', NULL, 'noactive', 'cancel', '2020-08-13 06:56:17', '2020-08-13 14:03:48'),
(17, 2, NULL, NULL, 'Nguyễn Huệ', '123456789', 'Quang Trung', NULL, 'noactive', 'cancel', '2020-08-13 07:12:45', '2020-08-13 10:10:52'),
(35, 2, NULL, NULL, 'Trần Văn Test', '098765432', 'Hà Nam', NULL, 'noactive', 'noactive', '2020-08-13 18:16:58', '2020-08-13 18:16:58'),
(36, 2, NULL, NULL, 'Nguyễn Thị Test', '123456789', 'Hà Nội', NULL, 'noactive', 'noactive', '2020-08-13 18:18:51', '2020-08-13 18:18:51'),
(37, 2, NULL, NULL, 'Test phát Cuối', '987655291', 'hehehehe', NULL, 'noactive', 'noactive', '2020-08-13 18:31:39', '2020-08-13 18:31:39'),
(39, 2, NULL, NULL, 'Test Thêm Phát', '0985674635', 'Ở đâu đó', NULL, 'active', 'noactive', '2020-08-13 18:55:47', '2020-08-13 18:56:24');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE `persons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`id`, `user_id`, `name`, `avatar_person`, `gender`, `birth`, `address`, `phone`, `created_at`, `updated_at`) VALUES
(2, 2, 'Nguyễn Văn B', 'default.jpg', NULL, NULL, 'hsdsd', '123456789', '2020-08-05 08:10:22', '2020-08-05 08:10:22');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subcategory_id` bigint(20) UNSIGNED NOT NULL,
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_product` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `length_width` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_number` int(11) NOT NULL DEFAULT 0,
  `price` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.00',
  `quatity` int(30) NOT NULL DEFAULT 0,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `subcategory_id`, `author_id`, `company_id`, `product_name`, `url_product`, `description`, `length_width`, `publication_date`, `page_number`, `price`, `quatity`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(20, 3, 3, 1, 'Hồ tuyệt mệnh', 'ho-tuyet-menh', '<p><strong>TIKI KHUY&Ecirc;N ĐỌC</strong></p>\r\n\r\n<p><strong>Đ&Aacute;NH GI&Aacute; TỪ TIKI.VN:</strong></p>\r\n\r\n<p>D&ugrave; rằng so với những t&aacute;c phẩm đ&igrave;nh đ&aacute;m một thời của Quỷ Cổ Nữ như Kỳ &Aacute;n &Aacute;nh Trăng hay Đau Thương Đến Chết th&igrave; Hồ Tuyệt Mệnh kh&ocirc;ng đ&aacute;ng sợ v&agrave; ma mị bằng nhưng vẫn kh&ocirc;ng thể phủ nhận rằng t&aacute;c phẩm n&agrave;y c&oacute; một sức h&uacute;t rất ri&ecirc;ng.</p>\r\n\r\n<p>Phần dẫn truyện ở đầu t&aacute;c phẩm kh&aacute; r&ugrave;ng rợn v&agrave; li&ecirc;u trai, tuy đi s&acirc;u v&agrave;o mạch truyện t&igrave;nh tiết n&agrave;y đ&atilde; giảm bớt yếu tố ma qu&aacute;i nhưng quả thật độc giả vẫn kh&ocirc;ng thể đặt quyển s&aacute;ch xuống ngay được.</p>\r\n\r\n<p>Điểm nhấn của t&aacute;c phẩm l&agrave; mối quan hệ giữa Na Lan v&agrave; Tần Ho&agrave;i v&agrave; c&aacute;ch mi&ecirc;u tả độc đ&aacute;o cuốn h&uacute;t của t&aacute;c giả về t&iacute;nh c&aacute;ch anh ch&agrave;ng n&agrave;y khiến nhiều người phải t&ograve; m&ograve;, đi s&acirc;u v&agrave;o t&igrave;m hiểu thực hư. V&agrave; lời cảnh b&aacute;o &ldquo;Gặp Tần Ho&agrave;i, lỡ cả một đời người&rdquo; như c&agrave;ng khiến người đọc lo lắng hơn cho số phận của Na Lan, th&oacute;t tim c&ugrave;ng từng cuộc h&agrave;nh tr&igrave;nh đi t&igrave;m sự thật về những c&aacute;i chết b&iacute; ẩn cũng như truyền thuyết từ chiếc hồ Chi&ecirc;u Dương.</p>\r\n\r\n<p>Quỷ Cổ Nữ lu&ocirc;n rất hay v&agrave; ấn tượng trong c&aacute;ch dẫn dắt c&acirc;u chuyện. Những b&iacute; ẩn v&agrave; uẩn kh&uacute;c l&uacute;c đầu đ&atilde; được kh&eacute;p lại bằng những lập luận rất khoa học, logic v&agrave; hiển nhi&ecirc;n l&agrave; kh&ocirc;ng hề c&oacute; ch&uacute;t ma qu&aacute;i b&iacute; ẩn n&agrave;o ở cuối truyện.</p>\r\n\r\n<p>&ldquo;Hồ Tuyệt M&ecirc;nh&rdquo; đ&atilde; l&agrave;m rất tốt nhiệm vụ mở đầu cho series Hồ Sơ Tội &Aacute;c, d&ugrave; đ&ocirc;i chỗ c&ograve;n phạm phải lỗi kỹ thuật như lỗi đ&aacute;nh m&aacute;y hay đ&ocirc;i chỗ sai t&ecirc;n nh&acirc;n vật. Nhưng với những ưu điểm m&agrave; t&aacute;c phẩm mang lại th&igrave; c&oacute; lẽ cũng đủ để c&aacute;c fan của thể loại n&agrave;y h&agrave;i l&ograve;ng. Cuộc h&agrave;nh tr&igrave;nh của c&ocirc; g&aacute;i Na Lan vẫn chưa dừng lại v&agrave; dĩ nhi&ecirc;n tất cả ch&uacute;ng ta vẫn đang chờ đ&oacute;n những bất ngờ tiếp theo từ series n&agrave;y.</p>\r\n\r\n<p>&copy;</p>\r\n\r\n<p>Chuyện l&agrave;, c&oacute; một c&aacute;i hồ chứa hai truyền thuyết. Truyền thuyết một l&agrave; dưới đảo giữa hồ c&oacute; ch&ocirc;n kho b&aacute;u. Truyền thuyết hai l&agrave; hễ tr&ecirc;n hồ xuất hiện người tr&ugrave;m &aacute;o tơi bu&ocirc;ng c&acirc;u m&agrave; cần c&acirc;u kh&ocirc;ng d&acirc;y, th&igrave; sẽ c&oacute; người chết.</p>\r\n\r\n<p>Một năm Gia Tĩnh triều Minh, bốn phương phẳng lặng hai kinh vững v&agrave;ng, c&oacute; một t&ecirc;n đạo ch&iacute;ch quyến rũ được con g&aacute;i th&aacute;i sư đương triều bỏ trốn theo m&igrave;nh. V&igrave; muốn n&agrave;ng được sống sung sướng, hắn mang n&agrave;ng đến hồ n&agrave;y, chuẩn bị t&igrave;m kho b&aacute;u. Tr&ecirc;n đường đi, một đạo sĩ dặn họ n&ecirc;n tr&aacute;nh xa c&aacute;i hồ đ&oacute; ra, nếu để gặp người &aacute;o tơi đi c&acirc;u th&igrave; tức l&agrave; sẽ gặp đại nạn. T&ecirc;n đạo ch&iacute;ch cho rằng m&ecirc; t&iacute;n, vẫn mang tiểu thư đến b&ecirc;n hồ, đợi h&ocirc;m sau sẽ xuống th&aacute;m th&iacute;nh kho b&aacute;u. Đ&ecirc;m h&ocirc;m đ&oacute; mưa to gi&oacute; lớn, tiểu thư kh&ocirc;ng ngủ được, nh&igrave;n ra hồ th&igrave; thấy c&oacute; người tr&ugrave;m &aacute;o tơi bu&ocirc;ng c&acirc;u. H&ocirc;m sau đạo ch&iacute;ch xuống hồ lặn t&igrave;m, tiểu thư đợi tr&ecirc;n bờ, đợi m&atilde;i, đợi m&atilde;i, cuối c&ugrave;ng chỉ thấy x&aacute;c chồng nổi l&ecirc;n.</p>\r\n\r\n<p>Mấy trăm năm sau, v&agrave;o thời hiện đại, trong một căn nh&agrave; b&ecirc;n hồ, giữa đ&ecirc;m mất điện mưa to gi&oacute; lớn, c&oacute; hai c&ocirc; g&aacute;i ngồi đọc c&acirc;u chuyện về t&ecirc;n đạo ch&iacute;ch v&agrave; tiểu thư vợ hắn. L&ograve;ng đầy t&ograve; m&ograve;, nh&igrave;n ra hỏi nhau phải ch&iacute;nh l&agrave; c&aacute;i hồ v&agrave; h&ograve;n đảo kia kh&ocirc;ng, rồi cho rằng to&agrave;n chuyện bịa đặt của t&aacute;c giả cả. Đ&uacute;ng l&uacute;c họ nh&igrave;n ra th&igrave; một &aacute;nh chớp l&oacute;e s&aacute;ng mặt hồ, soi r&otilde; một con thuyền nhỏ. V&agrave; người mặc &aacute;o tơi tr&ecirc;n thuyền. Tim hai c&ocirc; như bị tử thần b&oacute;p chặt, mỗi nhịp đập đều dữ dội kinh khủng l&agrave;m sao. Một, hai, ba, bốn, năm. Tr&ecirc;n thuyền c&oacute; cả thảy năm người mặc &aacute;o tơi! Chưa kịp ho&agrave;n hồn th&igrave; trong nh&agrave; xuất hiện th&ecirc;m một người, vươn đ&ocirc;i tay khẳng khiu b&oacute;p nghiến lấy cổ một c&ocirc;.</p>\r\n\r\n<p>Mấy năm sau, c&oacute; một người ngồi đọc c&acirc;u chuyện về hai c&ocirc; g&aacute;i n&agrave;y, v&agrave; về năm c&aacute;i x&aacute;c trắng nhễ nhại lần lượt nổi l&ecirc;n mặt hồ sau đ&ecirc;m đ&oacute;. Người n&agrave;y bị ma xui quỷ khiến, thấy những kẻ xung quanh thật đ&aacute;ng nghi, v&agrave; bắt đầu để m&igrave;nh cuốn v&agrave;o đủ việc vốn chẳng li&ecirc;n quan đến m&igrave;nh, đồng nghĩa với việc đặt bản th&acirc;n trước những hung hiểm đa dạng: l&uacute;c bị săn đuổi, l&uacute;c bị xe c&aacute;n, l&uacute;c sắp bị đ&acirc;m, l&uacute;c gần chết đuối, l&uacute;c su&yacute;t ăn đạn&hellip;</p>\r\n\r\n<p>V&agrave; tới năm nay, đến lượt ch&uacute;ng ta ngồi giở Hồ tuyệt mệnh, theo d&otilde;i những cuộc dấn th&acirc;n ngoan cố v&agrave; ngoan cường của người ấy&hellip; NA LAN!</p>\r\n\r\n<p>***</p>\r\n\r\n<p>Về t&aacute;c giả:</p>\r\n\r\n<p><strong><a href=\"http://tiki.vn/author/quy-co-nu.html\">Quỷ Cổ Nữ</a></strong>&nbsp;l&agrave; b&uacute;t danh chung của một cặp vợ chồng người Trung Quốc đang sống ở Mỹ. Vợ l&agrave; Dư Dương kỹ sư th&acirc;m ni&ecirc;n về phần mềm m&aacute;y t&iacute;nh. Chồng l&agrave; Dị Minh chuy&ecirc;n gia y học nổi danh. M&ugrave;a xu&acirc;n năm 2004, hai người d&ugrave;ng t&ecirc;n thật, c&ugrave;ng xuất bản cuốn tiểu thuyết d&agrave;i M&ugrave;a xu&acirc;n tr&ecirc;n d&ograve;ng s&ocirc;ng băng, được giới chuy&ecirc;n m&ocirc;n đ&aacute;nh gi&aacute; cao.</p>\r\n\r\n<p>Giữa năm 2004, bằng b&uacute;t danh Quỷ Cổ Nữ, hai vợ chồng tung l&ecirc;n mạng một tiểu thuyết kinh dị nhan đề Kỳ &aacute;n &aacute;nh trăng. T&aacute;c phẩm g&acirc;y chấn động kh&ocirc;ng ngờ, l&ocirc;i cuốn h&agrave;ng triệu độc giả chỉ trong v&ograve;ng v&agrave;i ba th&aacute;ng, đăng tải nửa chừng th&igrave; nhận được lời đề nghị xuất bản từ Nh&agrave; xuất bản Nh&acirc;n D&acirc;n Thượng Hải. Mồng 1 Tết &acirc;m lịch năm 2005, cuốn s&aacute;ch Kỳ &aacute;n &aacute;nh trăng ra mắt bạn đọc, v&agrave; Quỷ Cổ Nữ lập tức bật l&ecirc;n th&agrave;nh ng&ocirc;i sao s&aacute;ng tr&ecirc;n văn đ&agrave;n, th&agrave;nh t&aacute;c giả ti&ecirc;u biểu nhất của d&ograve;ng tiểu thuyết kinh dị Trung Quốc.&nbsp;<strong>Truyện Quỷ Cổ Nữ</strong>&nbsp;lu&ocirc;n được đọc giả h&aacute;o hức đ&oacute;n đọc.</p>\r\n\r\n<p>Hồ tuyệt mệnh l&agrave; phần 1 trong series Hồ sơ tội &aacute;c gồm bảy phần, tập trung khai th&aacute;c yếu tố &aacute;c quỷ t&acirc;m linh. Bằng b&uacute;t ph&aacute;p ma mị l&agrave;m khuynh đảo người đọc, Hồ tuyệt mệnh tuy mới xuất hiện nhưng đ&atilde; trở th&agrave;nh hiện tượng của văn h&oacute;a đọc, kế tục Kỳ &aacute;n &aacute;nh trăng vững v&agrave;ng củng cố địa vị Stephen King Trung Quốc của&nbsp;<strong>Quỷ Cổ Nữ</strong>.</p>\r\n\r\n<p>Gi&aacute; sản phẩm tr&ecirc;n Tiki đ&atilde; bao gồm thuế theo luật hiện h&agrave;nh. Tuy nhi&ecirc;n tuỳ v&agrave;o từng loại sản phẩm hoặc phương thức, địa chỉ giao h&agrave;ng m&agrave; c&oacute; thể ph&aacute;t sinh th&ecirc;m chi ph&iacute; kh&aacute;c như ph&iacute; vận chuyển, phụ ph&iacute; h&agrave;ng cồng kềnh, ...</p>', '15 x 24 cm', '2020-07-11', 404, '70000', 26, NULL, NULL, '2020-07-21 06:58:07', '2020-07-21 06:58:07'),
(21, 3, 3, 1, 'Tiệc báo thù', 'tiec-bao-thu', 'Tiệc Báo Thù Người thứ nhất tan xác Người thứ hai nát bươm Người thứ ba rách tươm Ba người nữa chết cháy… Chuyện xưa không ai thấy Chuyện nay chẳng ai nghe Bọ ngựa rình bắt ve Hay đâu sẻ chực sẵn… Nếu hỏi, trong ba năm nay tác phẩm nào của Quỷ Cổ Nữ là ly kỳ nhất, thì Tiệc báo thù chính là câu trả lời. Một buổi trưa tháng Năm Giang Kinh, trời lành cảnh đẹp, nhưng tại tầng hai hội quán ẩm thực nọ, rèm cửa lại buông sùm sụp, không phải để ngăn tầm nhìn khách khứa ra khoảng không ấm áp bên ngoài, mà để ngăn lũ bóng thám sát lởn vởn ở rìa cửa sổ ngó vào bên trong. Bởi bên trong ấy, là hiện trường của một tấn trò đời. Ở dưới sân, cảnh sát liên tục gọi loa vận động và yêu cầu đối thoại thả con tin. Đến một lúc, đường dây nóng rung lên, họ nhận được tin nhắn từ tầng hai gửi xuống: Muốn thương lượng, gọi Na Lan. Trong lúc ấy, Na Lan lại đang được mời đi ăn trưa tại một hội quán ẩm thực… Là tập thứ tư trong series Hồ sơ tội ác, nhưng Tiệc báo thù là một diện mạo khác hẳn. Men theo phong cách trinh thám cổ điển thuần túy, lồng ghép với hiệu ứng Rashomon - tái hiện sự vụ qua nhiều góc nhìn của các nhân chứng, câu chuyện đã từ bỏ hoàn toàn đám âm ảnh lởn vởn và màn sương truyền thuyết vẫn bao trùm các tập trước, để đi sâu vào những quanh co tăm tối của ân oán và lòng người.', '15 x 24 cm', '2020-07-09', 395, '76000', 20, NULL, NULL, '2020-07-21 06:59:49', '2020-08-12 08:43:27'),
(22, 3, 3, 2, 'Ảo Linh Kỳ', 'ao-linh-ky', '<p><strong>Ảo Linh Kỳ</strong></p>\r\n\r\n<p>ẢO LINH KỲ l&agrave; m&agrave;n t&aacute;i xuất của Quỷ Cổ Nữ, bộ đ&ocirc;i t&aacute;c giả từng g&acirc;y ấn tượng vang dội với Kỳ &aacute;n &aacute;nh trăng v&agrave; suốt mười mấy năm qua vẫn li&ecirc;n tục duy tr&igrave; sức h&uacute;t nhờ việc kết hợp th&agrave;nh c&ocirc;ng ba yếu tố kinh dị, t&acirc;m l&yacute; v&agrave; t&acirc;m linh trong c&aacute;c t&aacute;c phẩm của m&igrave;nh. Với &ldquo;Ảo linh kỳ&rdquo;, Quỷ Cổ Nữ kh&ocirc;ng g&acirc;y ngạc nhi&ecirc;n khi tiếp tục khai th&aacute;c mảnh đất Giang Kinh m&agrave;u mỡ ngồn ngộn chuyện người chuyện ma, quấy nh&atilde;o thực-ảo, c&ugrave;ng những nh&acirc;n vật ở lưng chừng tỉnh t&aacute;o v&agrave; loạn tr&iacute;.</p>\r\n\r\n<p>Nhưng với &ldquo;Ảo linh kỳ&rdquo;, Quỷ Cổ Nữ g&acirc;y bất ngờ khi đẩy ngược Giang Kinh lại năm trăm năm trước, lần đầu ti&ecirc;n trổ t&agrave;i bằng thể loại trinh th&aacute;m-kinh dị đặt trong bối cảnh cổ xưa. Tương truyền, Th&agrave;nh C&aacute;t Tư H&atilde;n uy phong lỗi lạc l&agrave; vậy, thật ra l&agrave; người m&ugrave; chữ. Tuy nhi&ecirc;n trước l&uacute;c chết, &ocirc;ng ta đ&atilde; xoay xở để lại một cuốn s&aacute;ch m&agrave; đời sau gọi l&agrave; Thi&ecirc;n thư, trong đ&oacute; ghi ch&eacute;p b&iacute; quyết chinh phạt thi&ecirc;n hạ để lập n&ecirc;n một đế chế huy ho&agrave;ng.</p>\r\n\r\n<p>Cuốn s&aacute;ch lưu lạc v&agrave;o d&acirc;n gian, b&iacute; quyết th&agrave;nh c&ocirc;ng đ&acirc;u chưa thấy, chỉ thấy n&oacute; gieo rắc những c&aacute;i chết th&ecirc; thảm.</p>\r\n\r\n<p>Chiếc thuyền chở n&oacute;, to&agrave;n bộ thủy thủ đo&agrave;n thiệt mạng, từ bấy con thuyền th&agrave;nh thuyền ma lững thững tr&ocirc;i dạt. Ai gặp phải th&igrave; thất khiếu vỡ n&aacute;t, m&aacute;u chảy cạn m&agrave; chết.</p>\r\n\r\n<p>C&aacute;i hồ giấu n&oacute;, tự dưng mọc ra một ngư d&acirc;n &aacute;o tơi bu&ocirc;ng c&acirc;u, mỗi lần c&acirc;u l&agrave; một lần c&oacute; người hồn l&igrave;a khỏi x&aacute;c.</p>\r\n\r\n<p>D&ugrave; vậy, Thi&ecirc;n thư vẫn khuấy l&ecirc;n l&agrave;n s&oacute;ng truy t&igrave;m của những kẻ &ocirc;m mộng danh vọng-tiền t&agrave;i-quyền lực.</p>\r\n\r\n<p>Từ đ&oacute; dẫn đến những cuộc săn l&ugrave;ng r&aacute;o riết. Chỉ hiềm, từ săn vật b&aacute;u, chẳng bao l&acirc;u đ&atilde; biến tướng th&agrave;nh người săn người&hellip;</p>\r\n\r\n<p>Mở đầu từ hai truyền thuyết, &ldquo;Ảo linh kỳ&rdquo; nhanh ch&oacute;ng lồng ch&uacute;ng v&agrave;o thực tại bằng những &aacute;n mạng t&agrave;n khốc, v&agrave;o những giao ước đẫm m&aacute;u của d&acirc;n băng đảng, v&agrave;o cả những mối quan hệ s&acirc;u k&iacute;n bị thời gian lẫn &acirc;m mưu đạp mờ. Cuối c&ugrave;ng tất thảy đều kh&ocirc;ng tho&aacute;t khỏi định luật tham s&acirc;n si l&agrave; họa hại, m&agrave; hung thủ thực sự th&igrave;, cứ li&ecirc;n tục đảo ch&acirc;n ẩn m&igrave;nh về sau tấm m&agrave;n đen.</p>\r\n\r\n<p>Mạch ph&aacute; &aacute;n được triển khai rất nhanh, dồn dập, nhưng đ&oacute; đ&acirc;y vẫn điểm ch&uacute;t thư th&aacute;i khi ta bắt gặp những nh&acirc;n c&aacute;ch lớn tr&acirc;n trọng lẫn nhau, ch&uacute;t buồn đau khi người tốt bị đem ra t&ugrave;ng xẻo, v&agrave; cả ch&uacute;t vui vẻ ngắn ngủi trước khi một bi kịch khủng khiếp ập tới&hellip;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Gi&aacute; sản phẩm tr&ecirc;n Tiki đ&atilde; bao gồm thuế theo luật hiện h&agrave;nh. Tuy nhi&ecirc;n tuỳ v&agrave;o từng loại sản phẩm hoặc phương thức, địa chỉ giao h&agrave;ng m&agrave; c&oacute; thể ph&aacute;t sinh th&ecirc;m chi ph&iacute; kh&aacute;c như ph&iacute; vận chuyển, phụ ph&iacute; h&agrave;ng cồng kềnh, ...</p>', '15.5 x 24 cm', '2020-07-09', 584, '123000', 21, NULL, NULL, '2020-07-21 07:02:40', '2020-07-28 03:23:02'),
(51, 3, 3, 1, 'Tuyết đoạt hồn', 'tuyet-doat-hon', '<p>D&ugrave; rằng so với những t&aacute;c phẩm đ&igrave;nh đ&aacute;m một thời của Quỷ Cổ Nữ như Kỳ &Aacute;n &Aacute;nh Trăng hay Đau Thương Đến Chết th&igrave; Hồ Tuyệt Mệnh kh&ocirc;ng đ&aacute;ng sợ v&agrave; ma mị bằng nhưng vẫn kh&ocirc;ng thể phủ nhận rằng t&aacute;c phẩm n&agrave;y c&oacute; một sức h&uacute;t rất ri&ecirc;ng.</p>\r\n\r\n<p>Phần dẫn truyện ở đầu t&aacute;c phẩm kh&aacute; r&ugrave;ng rợn v&agrave; li&ecirc;u trai, tuy đi s&acirc;u v&agrave;o mạch truyện t&igrave;nh tiết n&agrave;y đ&atilde; giảm bớt yếu tố ma qu&aacute;i nhưng quả thật độc giả vẫn kh&ocirc;ng thể đặt quyển s&aacute;ch xuống ngay được.</p>\r\n\r\n<p>Điểm nhấn của t&aacute;c phẩm l&agrave; mối quan hệ giữa Na Lan v&agrave; Tần Ho&agrave;i v&agrave; c&aacute;ch mi&ecirc;u tả độc đ&aacute;o cuốn h&uacute;t của t&aacute;c giả về t&iacute;nh c&aacute;ch anh ch&agrave;ng n&agrave;y khiến nhiều người phải t&ograve; m&ograve;, đi s&acirc;u v&agrave;o t&igrave;m hiểu thực hư. V&agrave; lời cảnh b&aacute;o &ldquo;Gặp Tần Ho&agrave;i, lỡ cả một đời người&rdquo; như c&agrave;ng khiến người đọc lo lắng hơn cho số phận của Na Lan, th&oacute;t tim c&ugrave;ng từng cuộc h&agrave;nh tr&igrave;nh đi t&igrave;m sự thật về những c&aacute;i chết b&iacute; ẩn cũng như truyền thuyết từ chiếc hồ Chi&ecirc;u Dương.</p>\r\n\r\n<p>Quỷ Cổ Nữ lu&ocirc;n rất hay v&agrave; ấn tượng trong c&aacute;ch dẫn dắt c&acirc;u chuyện. Những b&iacute; ẩn v&agrave; uẩn kh&uacute;c l&uacute;c đầu đ&atilde; được kh&eacute;p lại bằng những lập luận rất khoa học, logic v&agrave; hiển nhi&ecirc;n l&agrave; kh&ocirc;ng hề c&oacute; ch&uacute;t ma qu&aacute;i b&iacute; ẩn n&agrave;o ở cuối truyện.</p>\r\n\r\n<p>&ldquo;Hồ Tuyệt M&ecirc;nh&rdquo; đ&atilde; l&agrave;m rất tốt nhiệm vụ mở đầu cho series Hồ Sơ Tội &Aacute;c, d&ugrave; đ&ocirc;i chỗ c&ograve;n phạm phải lỗi kỹ thuật như lỗi đ&aacute;nh m&aacute;y hay đ&ocirc;i chỗ sai t&ecirc;n nh&acirc;n vật. Nhưng với những ưu điểm m&agrave; t&aacute;c phẩm mang lại th&igrave; c&oacute; lẽ cũng đủ để c&aacute;c fan của thể loại n&agrave;y h&agrave;i l&ograve;ng. Cuộc h&agrave;nh tr&igrave;nh của c&ocirc; g&aacute;i Na Lan vẫn chưa dừng lại v&agrave; dĩ nhi&ecirc;n tất cả ch&uacute;ng ta vẫn đang chờ đ&oacute;n những bất ngờ tiếp theo từ series n&agrave;y.</p>\r\n\r\n<p>&copy;</p>\r\n\r\n<p>Chuyện l&agrave;, c&oacute; một c&aacute;i hồ chứa hai truyền thuyết. Truyền thuyết một l&agrave; dưới đảo giữa hồ c&oacute; ch&ocirc;n kho b&aacute;u. Truyền thuyết hai l&agrave; hễ tr&ecirc;n hồ xuất hiện người tr&ugrave;m &aacute;o tơi bu&ocirc;ng c&acirc;u m&agrave; cần c&acirc;u kh&ocirc;ng d&acirc;y, th&igrave; sẽ c&oacute; người chết.</p>\r\n\r\n<p>Một năm Gia Tĩnh triều Minh, bốn phương phẳng lặng hai kinh vững v&agrave;ng, c&oacute; một t&ecirc;n đạo ch&iacute;ch quyến rũ được con g&aacute;i th&aacute;i sư đương triều bỏ trốn theo m&igrave;nh. V&igrave; muốn n&agrave;ng được sống sung sướng, hắn mang n&agrave;ng đến hồ n&agrave;y, chuẩn bị t&igrave;m kho b&aacute;u. Tr&ecirc;n đường đi, một đạo sĩ dặn họ n&ecirc;n tr&aacute;nh xa c&aacute;i hồ đ&oacute; ra, nếu để gặp người &aacute;o tơi đi c&acirc;u th&igrave; tức l&agrave; sẽ gặp đại nạn. T&ecirc;n đạo ch&iacute;ch cho rằng m&ecirc; t&iacute;n, vẫn mang tiểu thư đến b&ecirc;n hồ, đợi h&ocirc;m sau sẽ xuống th&aacute;m th&iacute;nh kho b&aacute;u. Đ&ecirc;m h&ocirc;m đ&oacute; mưa to gi&oacute; lớn, tiểu thư kh&ocirc;ng ngủ được, nh&igrave;n ra hồ th&igrave; thấy c&oacute; người tr&ugrave;m &aacute;o tơi bu&ocirc;ng c&acirc;u. H&ocirc;m sau đạo ch&iacute;ch xuống hồ lặn t&igrave;m, tiểu thư đợi tr&ecirc;n bờ, đợi m&atilde;i, đợi m&atilde;i, cuối c&ugrave;ng chỉ thấy x&aacute;c chồng nổi l&ecirc;n.</p>\r\n\r\n<p>Mấy trăm năm sau, v&agrave;o thời hiện đại, trong một căn nh&agrave; b&ecirc;n hồ, giữa đ&ecirc;m mất điện mưa to gi&oacute; lớn, c&oacute; hai c&ocirc; g&aacute;i ngồi đọc c&acirc;u chuyện về t&ecirc;n đạo ch&iacute;ch v&agrave; tiểu thư vợ hắn. L&ograve;ng đầy t&ograve; m&ograve;, nh&igrave;n ra hỏi nhau phải ch&iacute;nh l&agrave; c&aacute;i hồ v&agrave; h&ograve;n đảo kia kh&ocirc;ng, rồi cho rằng to&agrave;n chuyện bịa đặt của t&aacute;c giả cả. Đ&uacute;ng l&uacute;c họ nh&igrave;n ra th&igrave; một &aacute;nh chớp l&oacute;e s&aacute;ng mặt hồ, soi r&otilde; một con thuyền nhỏ. V&agrave; người mặc &aacute;o tơi tr&ecirc;n thuyền. Tim hai c&ocirc; như bị tử thần b&oacute;p chặt, mỗi nhịp đập đều dữ dội kinh khủng l&agrave;m sao. Một, hai, ba, bốn, năm. Tr&ecirc;n thuyền c&oacute; cả thảy năm người mặc &aacute;o tơi! Chưa kịp ho&agrave;n hồn th&igrave; trong nh&agrave; xuất hiện th&ecirc;m một người, vươn đ&ocirc;i tay khẳng khiu b&oacute;p nghiến lấy cổ một c&ocirc;.</p>\r\n\r\n<p>Mấy năm sau, c&oacute; một người ngồi đọc c&acirc;u chuyện về hai c&ocirc; g&aacute;i n&agrave;y, v&agrave; về năm c&aacute;i x&aacute;c trắng nhễ nhại lần lượt nổi l&ecirc;n mặt hồ sau đ&ecirc;m đ&oacute;. Người n&agrave;y bị ma xui quỷ khiến, thấy những kẻ xung quanh thật đ&aacute;ng nghi, v&agrave; bắt đầu để m&igrave;nh cuốn v&agrave;o đủ việc vốn chẳng li&ecirc;n quan đến m&igrave;nh, đồng nghĩa với việc đặt bản th&acirc;n trước những hung hiểm đa dạng: l&uacute;c bị săn đuổi, l&uacute;c bị xe c&aacute;n, l&uacute;c sắp bị đ&acirc;m, l&uacute;c gần chết đuối, l&uacute;c su&yacute;t ăn đạn&hellip;</p>\r\n\r\n<p>V&agrave; tới năm nay, đến lượt ch&uacute;ng ta ngồi giở Hồ tuyệt mệnh, theo d&otilde;i những cuộc dấn th&acirc;n ngoan cố v&agrave; ngoan cường của người ấy&hellip; NA LAN!</p>\r\n\r\n<p>***</p>\r\n\r\n<p>Về t&aacute;c giả:</p>\r\n\r\n<p><strong><a href=\"http://tiki.vn/author/quy-co-nu.html\">Quỷ Cổ Nữ</a></strong>&nbsp;l&agrave; b&uacute;t danh chung của một cặp vợ chồng người Trung Quốc đang sống ở Mỹ. Vợ l&agrave; Dư Dương kỹ sư th&acirc;m ni&ecirc;n về phần mềm m&aacute;y t&iacute;nh. Chồng l&agrave; Dị Minh chuy&ecirc;n gia y học nổi danh. M&ugrave;a xu&acirc;n năm 2004, hai người d&ugrave;ng t&ecirc;n thật, c&ugrave;ng xuất bản cuốn tiểu thuyết d&agrave;i M&ugrave;a xu&acirc;n tr&ecirc;n d&ograve;ng s&ocirc;ng băng, được giới chuy&ecirc;n m&ocirc;n đ&aacute;nh gi&aacute; cao.</p>\r\n\r\n<p>Giữa năm 2004, bằng b&uacute;t danh Quỷ Cổ Nữ, hai vợ chồng tung l&ecirc;n mạng một tiểu thuyết kinh dị nhan đề Kỳ &aacute;n &aacute;nh trăng. T&aacute;c phẩm g&acirc;y chấn động kh&ocirc;ng ngờ, l&ocirc;i cuốn h&agrave;ng triệu độc giả chỉ trong v&ograve;ng v&agrave;i ba th&aacute;ng, đăng tải nửa chừng th&igrave; nhận được lời đề nghị xuất bản từ Nh&agrave; xuất bản Nh&acirc;n D&acirc;n Thượng Hải. Mồng 1 Tết &acirc;m lịch năm 2005, cuốn s&aacute;ch Kỳ &aacute;n &aacute;nh trăng ra mắt bạn đọc, v&agrave; Quỷ Cổ Nữ lập tức bật l&ecirc;n th&agrave;nh ng&ocirc;i sao s&aacute;ng tr&ecirc;n văn đ&agrave;n, th&agrave;nh t&aacute;c giả ti&ecirc;u biểu nhất của d&ograve;ng tiểu thuyết kinh dị Trung Quốc.&nbsp;<strong>Truyện Quỷ Cổ Nữ</strong>&nbsp;lu&ocirc;n được đọc giả h&aacute;o hức đ&oacute;n đọc.</p>\r\n\r\n<p>Hồ tuyệt mệnh l&agrave; phần 1 trong series Hồ sơ tội &aacute;c gồm bảy phần, tập trung khai th&aacute;c yếu tố &aacute;c quỷ t&acirc;m linh. Bằng b&uacute;t ph&aacute;p ma mị l&agrave;m khuynh đảo người đọc, Hồ tuyệt mệnh tuy mới xuất hiện nhưng đ&atilde; trở th&agrave;nh hiện tượng của văn h&oacute;a đọc, kế tục Kỳ &aacute;n &aacute;nh trăng vững v&agrave;ng củng cố địa vị Stephen King Trung Quốc của&nbsp;<strong>Quỷ Cổ Nữ</strong>.</p>\r\n\r\n<p>Gi&aacute; sản phẩm tr&ecirc;n Tiki đ&atilde; bao gồm thuế theo luật hiện h&agrave;nh. Tuy nhi&ecirc;n tuỳ v&agrave;o từng loại sản phẩm hoặc phương thức, địa chỉ giao h&agrave;ng m&agrave; c&oacute; thể ph&aacute;t sinh th&ecirc;m chi ph&iacute; kh&aacute;c như ph&iacute; vận chuyển, phụ ph&iacute; h&agrave;ng cồng kềnh, ...</p>', '17 x 23 cm', '2020-07-17', 404, '312000', 17, NULL, NULL, '2020-07-28 03:25:54', '2020-08-14 01:44:39'),
(52, 3, 3, 2, 'Kỳ Án Ánh Trăng', 'ky-an-anh-trang', '<p><strong>Kỳ &Aacute;n &Aacute;nh Trăng (Phi&ecirc;n Bản Mới)</strong></p>\r\n\r\n<p>Đ&ecirc;m 16 th&aacute;ng S&aacute;u</p>\r\n\r\n<p>Lu&ocirc;n c&oacute; người nhảy lầu&hellip; Đ&atilde; mười s&aacute;u năm rồi, cứ v&agrave;o nửa đ&ecirc;m 16 th&aacute;ng S&aacute;u l&agrave; c&oacute; người tới số, mụ mẫm tr&egrave;o l&ecirc;n bậu cửa ph&ograve;ng 405, lao đầu xuống s&acirc;n v&agrave; chết.</p>\r\n\r\n<p>D&ugrave; được di tản ra ngo&agrave;i để tr&aacute;nh nạn th&igrave; v&agrave;o thời khắc định mệnh đ&oacute;, người tới số vẫn m&ograve; về, tr&egrave;o l&ecirc;n bậu cửa ph&ograve;ng 405, lao đầu xuống s&acirc;n v&agrave; chết.</p>\r\n\r\n<p>D&ugrave; được canh g&aacute;c ngăn chặn th&igrave; v&agrave;o thời khắc định mệnh đ&oacute;, người tới số vẫn t&igrave;m c&aacute;ch lọt v&agrave;o, tr&egrave;o l&ecirc;n bậu cửa ph&ograve;ng 405, lao đầu xuống s&acirc;n v&agrave; chết.</p>\r\n\r\n<p>Người tới số năm nay l&agrave; Diệp Hinh. Kh&ocirc;ng cam chịu lời nguyền tr&aacute;i ngang n&agrave;y, c&ocirc; t&igrave;m gặp người duy nhất tho&aacute;t nạn trong mười s&aacute;u năm qua để học hỏi kinh nghiệm, người ta liền tr&egrave;o l&ecirc;n bậu cửa sổ ngay trước mắt c&ocirc;, lao đầu xuống s&acirc;n v&agrave; chết.Đến l&uacute;c Diệp Hinh bế tắc bu&ocirc;ng tay, đ&atilde; sẵn s&agrave;ng cho việc tr&egrave;o l&ecirc;n bậu cửa, th&igrave; một người kh&aacute;c lại tranh suất của c&ocirc;, chuẩn bị lao đầu xuống s&acirc;n để chết&hellip;</p>\r\n\r\n<p>Nền s&acirc;n, bậu cửa, v&agrave; ph&ograve;ng 405&hellip; v&igrave; lẽ g&igrave; lại h&uacute;t kh&aacute;ch viếng thăm địa ngục tới vậy?</p>\r\n\r\n<p>Bi kịch sinh ra từ &ldquo;&Aacute;nh Trăng&rdquo;.</p>\r\n\r\n<p><strong>Quỷ Cổ Nữ&nbsp;</strong>l&agrave; b&uacute;t danh chung của một cặp vợ chồng người Trung Quốc đang sống ở Mỹ. Giữa năm 2004, họ tung l&ecirc;n mạng một tiểu thuyết kinh dị nhan đề Kỳ &aacute;n &aacute;nh trăng. T&aacute;c phẩm g&acirc;y chấn động kh&ocirc;ng ngờ, l&ocirc;i cuốn h&agrave;ng triệu độc giả chỉ trong v&ograve;ng v&agrave;i ba th&aacute;ng, đến khi cuốn s&aacute;ch ra mắt bạn đọc th&igrave; Quỷ Cổ Nữ lập tức bật l&ecirc;n th&agrave;nh ng&ocirc;i sao s&aacute;ng tr&ecirc;n văn đ&agrave;n, th&agrave;nh t&aacute;c giả ti&ecirc;u biểu nhất của d&ograve;ng tiểu thuyết kinh dị Trung Quốc.</p>\r\n\r\n<p>Gi&aacute; sản phẩm tr&ecirc;n Tiki đ&atilde; bao gồm thuế theo luật hiện h&agrave;nh. Tuy nhi&ecirc;n tuỳ v&agrave;o từng loại sản phẩm hoặc phương thức, địa chỉ giao h&agrave;ng m&agrave; c&oacute; thể ph&aacute;t sinh th&ecirc;m chi ph&iacute; kh&aacute;c như ph&iacute; vận chuyển, phụ ph&iacute; h&agrave;ng cồng kềnh, ...</p>', '17 x 23 cm', '2020-07-09', 528, '12300', 13, NULL, NULL, '2020-07-28 03:26:54', '2020-08-12 08:43:51'),
(53, 3, 3, 1, 'Đau Thương Đến Chết', 'dau-thuong-den-chet', '<p><em>Đa t&igrave;nh l&agrave; niềm đau</em></p>\r\n\r\n<p><em>Nhạy cảm l&agrave; nỗi mệt</em></p>\r\n\r\n<p><em>Si t&acirc;m l&agrave; hủy diệt</em></p>\r\n\r\n<p><em>Lương thiện chỉ khổ m&igrave;nh.</em></p>\r\n\r\n<p><em>&ldquo;Hễ v&agrave;o hang Thập Tịch, sẽ đau thương đến chết&hellip;&rdquo;</em></p>\r\n\r\n<p>Bất chấp lời nguyền độc địa, mười s&aacute;u người v&igrave; những l&yacute; do kh&aacute;c nhau đ&atilde; dấn th&acirc;n v&agrave;o hang, để rồi lần lượt bu&ocirc;ng m&igrave;nh xuống nanh vuốt tử thần. Kẻ ng&atilde; v&agrave;o b&agrave;n v&agrave; k&eacute;o đ&acirc;m ngập cổ, kẻ bị nắp quan t&agrave;i phang bật ngửa, kẻ ng&atilde; lầu l&uacute;c lau k&iacute;nh cửa, kẻ chết ngạt v&igrave; kh&iacute; gas&hellip; Tất cả đều l&agrave;: đau thương đến chết.</p>\r\n\r\n<p>C&ograve;n lại người cuối c&ugrave;ng, cật lực trốn chạy họa s&aacute;t th&acirc;n, nhưng trong những ng&agrave;y sống s&oacute;t cũng nếm trải m&ugrave;i vị chẳng dễ d&agrave;ng g&igrave;. Người chết đ&ograve;i chat c&ugrave;ng, hồn ma về gặp mặt, lưu manh thường b&aacute;m s&aacute;t, c&ocirc;n đồ đến t&ocirc;ng xe&hellip; Nhưng với bản t&iacute;nh ngoan cường, kh&ocirc;ng tin dị đoan, lại đặc biệt th&ocirc;ng minh, c&ocirc; đ&atilde; lần lần b&oacute;c trần được sự thật. Bức m&agrave;n u &aacute;m dần rơi xuống, ch&acirc;n tướng bắt đầu lộ ra.</p>\r\n\r\n<p>Những tưởng mất m&aacute;t đau thương sẽ chấm dứt ở đ&acirc;y, nhưng lời nguyền chưa kịp gieo, con người đ&atilde; tự giết nhau bởi l&ograve;ng tham, t&igrave;nh y&ecirc;u v&agrave; th&ugrave; hận. Lưỡi h&aacute;i tử thần chưa muốn h&atilde;m. B&iacute; mật khủng khiếp c&ograve;n chực chờ.</p>\r\n\r\n<p>V&agrave; lần n&agrave;y mới thực sự l&agrave;, tuyệt vọng!<br />\r\n<br />\r\n<br />\r\n&nbsp;</p>\r\n\r\n<p>Gi&aacute; sản phẩm tr&ecirc;n Tiki đ&atilde; bao gồm thuế theo luật hiện h&agrave;nh. Tuy nhi&ecirc;n tuỳ v&agrave;o từng loại sản phẩm hoặc phương thức, địa chỉ giao h&agrave;ng m&agrave; c&oacute; thể ph&aacute;t sinh th&ecirc;m chi ph&iacute; kh&aacute;c như ph&iacute; vận chuyển, phụ ph&iacute; h&agrave;ng cồng kềnh, ...</p>', '17 x 23 cm', '2020-07-09', 784, '165000', 25, NULL, NULL, '2020-07-28 03:30:12', '2020-07-28 03:30:12'),
(54, 3, 3, 2, 'Tơ Đồng Rỏ Máu', 'to-dong-ro-mau', '<p>Trăng s&aacute;ng nơi đầu n&uacute;i</p>\r\n\r\n<p>C&agrave;nh l&igrave;a c&acirc;y&nbsp;<strong>rụng rơi</strong></p>\r\n\r\n<p>Chim h&atilde;i h&ugrave;ng im<strong>&nbsp;</strong>tiếng,</p>\r\n\r\n<p>B&ecirc;n cầu dấu&nbsp;<strong>xương phơi</strong>&hellip;</p>\r\n\r\n<p>L&uacute;a ng&aacute;t hương m&ugrave;a mới</p>\r\n\r\n<p>&Aacute;t đi m&ugrave;i&nbsp;<strong>th&acirc;y tr&ocirc;i</strong></p>\r\n\r\n<p>Ếch k&ecirc;u đ&ecirc;m rộn r&atilde;</p>\r\n\r\n<p>Kh&ocirc;ng xua được chơi vơi.</p>\r\n\r\n<p>C&aacute;ch đ&acirc;y mấy trăm năm, dưới thời nh&agrave; Minh, ở một ch&acirc;u nọ li&ecirc;n tiếp xảy ra &aacute;n mạng thế n&agrave;y. C&aacute;c c&ocirc; g&aacute;i lần lượt mất t&iacute;ch, khi t&igrave;m được thi thể họ th&igrave; thấy c&aacute;i x&aacute;c n&agrave;o cũng cụt mất một ng&oacute;n tay. Một bộ kho&aacute;i của Đ&ocirc;ng Xưởng dốc hết t&acirc;m huyết điều tra vụ &aacute;n. Mấy chục năm tr&ocirc;i đi đều tốn c&ocirc;ng v&ocirc; &iacute;ch, đến một ng&agrave;y nọ, sắp tới l&uacute;c rửa tay g&aacute;c kiếm, đ&atilde; qu&aacute; tuyệt vọng, &ocirc;ng ta b&egrave;n thực hiện lần ch&oacute;t, b&agrave;y một con mồi, giăng một c&aacute;i bẫy, bẫy t&ecirc;n thủ phạm tới chặt tay giết người.</p>\r\n\r\n<p>Hung thủ vẫn tho&aacute;t. Con mồi bị giết. Bộ kho&aacute;i nhất thời phẫn uất, ngất lịm đi. Khi tỉnh lại, như bị quỷ &aacute;m, &ocirc;ng ta l&ecirc; bước đến hốc nh&agrave;, l&ocirc;i ra một c&aacute;i tr&aacute;p vẫn giấu k&iacute;n, mở ra. Giữa m&ugrave;i mốc h&ocirc;i lưu cữu nhiều năm, lẫn v&agrave;o m&ugrave;i thịt đang bắt đầu ph&acirc;n hủy. Giữa những kh&uacute;c xương ng&oacute;n tay đ&atilde; trắng kh&ocirc;, l&agrave; một ng&oacute;n tay gần đ&acirc;y mới chặt.</p>\r\n\r\n<p>Năm trăm năm sau, Giang Kinh lại nổi l&ecirc;n vụ &aacute;n giết người h&agrave;ng loạt c&oacute; t&ecirc;n &ldquo;ng&oacute;n tay khăn m&aacute;u&rdquo;. Nhiều c&ocirc; g&aacute;i đ&aacute;ng thương bị mất t&iacute;ch, gia đ&igrave;nh họ thoạt ti&ecirc;n nhận được bưu kiện, b&ecirc;n trong nh&uacute;m ng&oacute;n tay đ&atilde; bị chặt của họ. Nhưng v&agrave;i năm, thậm ch&iacute; v&agrave;i chục năm tr&ocirc;i đi, vẫn chưa ph&aacute;t hiện hoặc thấy x&aacute;c họ bị trả lại. Một nghi phạm bị bắt. L&atilde;o khẳng định vụ &aacute;n sẽ vẫn tiếp diễn, nhưng kh&ocirc;ng cho biết chi tiết, chỉ khăng khăng đ&ograve;i gặp Na Lan. V&agrave; khi c&ocirc; đến, l&atilde;o đưa ra c&aacute;c c&acirc;u đố kh&aacute;c nhau. Phải giải được loạt c&acirc;u đố n&agrave;y, c&ocirc; mới mong chặn được vụ &aacute;n, t&igrave;m được x&aacute;c những người bị hại bao nhi&ecirc;u năm qua. Na Lan m&ugrave; mờ đi theo chỉ dẫn, mỗi kh&aacute;m ph&aacute; lại đẩy c&ocirc; l&ecirc;n một mức độ bấn loạn v&agrave; hiểm nguy mới, nhưng kh&ocirc;ng bấn loạn v&agrave; hiểm nguy n&agrave;o s&aacute;nh được với kết cục chờ c&ocirc; ph&iacute;a cuối chặng đường.</p>\r\n\r\n<p>L&agrave; tập thứ ba trong series&nbsp;<strong><a href=\"http://tiki.vn/sach-truyen-tieng-viet/sach-van-hoc/kinh-di.html\">truyện kinh dị</a></strong>&nbsp;<em>Hồ sơ tội &aacute;c</em>,&nbsp;<strong><em>Th&acirc;y cụt ng&oacute;n</em></strong>&nbsp;l&agrave; một cao tr&agrave;o, một cục diện mới khẳng định b&uacute;t lực h&ugrave;ng hậu của&nbsp;<strong><a href=\"http://tiki.vn/author/quy-co-nu.html\">Quỷ Cổ Nữ</a></strong>. Trong tập n&agrave;y, người b&iacute; hiểm lu&ocirc;n theo d&otilde;i Na Lan từ&nbsp;<em>Hồ tuyệt mệnh</em>&nbsp;sang&nbsp;<em>Tuyết đoạt hồn</em>&nbsp;đ&atilde; được h&eacute; lộ một phần trước bạn đọc. Nhưng b&ecirc;n cạnh người đ&oacute;, c&ograve;n &iacute;t nhất ba người b&iacute; hiểm nữa cũng tham gia v&agrave;o tr&ograve; theo d&otilde;i dai dẳng kh&ocirc;ng thể chịu nổi ấy, phủ th&ecirc;m lớp sương ngầu đục l&ecirc;n đất Giang Kinh vốn vẫn ẩn chứa v&ocirc; v&agrave;n truyền thuyết v&agrave; b&atilde;o tố của l&ograve;ng người n&agrave;y.</p>\r\n\r\n<p>Gi&aacute; sản phẩm tr&ecirc;n Tiki đ&atilde; bao gồm thuế theo luật hiện h&agrave;nh. Tuy nhi&ecirc;n tuỳ v&agrave;o từng loại sản phẩm hoặc phương thức, địa chỉ giao h&agrave;ng m&agrave; c&oacute; thể ph&aacute;t sinh th&ecirc;m chi ph&iacute; kh&aacute;c như ph&iacute; vận chuyển, phụ ph&iacute; h&agrave;ng cồng kềnh, ...</p>', '17 x 23 cm', '2020-07-12', 233, '72980', 21, NULL, NULL, '2020-07-28 03:32:13', '2020-07-28 03:32:13'),
(55, 7, 4, 1, 'Nobita Và Chuyến Phiêu Lưu Vào Xứ Quỷ', 'nobita-va-chuyen-phieu-luu-vao-xu-quy', '<p>Tập truyện &ldquo;Nobita V&agrave; Chuyến Phi&ecirc;u Lưu V&agrave;o Xứ Quỷ&rdquo; l&agrave; nguy&ecirc;n t&aacute;c của bộ phim hoạt h&igrave;nh c&ugrave;ng t&ecirc;n. C&acirc;u chuyện lấy bối cảnh l&agrave; một thế giới ph&eacute;p thuật do Nobita tạo ra, thế giới n&agrave;y tồn tại song song với thế giới hiện thực. Tại đ&acirc;y, nh&oacute;m bạn Doraemon, Nobita đ&atilde; chiến đấu anh dũng để cứu tr&aacute;i đất khỏi mối nguy hiểm cận kề. Ch&uacute;ng ta h&atilde;y bước v&agrave;o thế giới ph&eacute;p thuật k&igrave; lạ n&agrave;y v&agrave; phi&ecirc;u lưu c&ugrave;ng nh&oacute;m bạn Doraemon nh&eacute;!</p>', '11.3 x 17.6 cm', '2020-07-17', 191, '16000', 9, NULL, NULL, '2020-07-28 03:40:46', '2020-08-12 17:12:15'),
(56, 7, 4, 2, 'Nobita Và Những Pháp Sư Gió Bí Ẩn', 'nobita-va-nhung-phap-su-gio-bi-an', '<p><strong>M&ocirc; tả sản phẩm:</strong></p>\r\n\r\n<p>Nobita V&agrave; Những Ph&aacute;p Sư Gi&oacute; B&iacute; Ẩn l&agrave; t&aacute;c phẩm thứ 23 trong series Doraemon Truyện D&agrave;i. C&acirc;u chuyện bắt đầu khi con g&aacute;i của thần gi&oacute; xuất hiện ở Tokyo v&agrave; được Nobita đem về nu&ocirc;i với c&aacute;i t&ecirc;n Fuko. Fuko ẩn m&igrave;nh trong lốt gấu b&ocirc;ng để ngụy trang, kh&ocirc;ng may c&ocirc; b&eacute; lại bị Suneo ph&aacute;t hiện v&agrave; muốn bắt l&agrave;m của ri&ecirc;ng. Ng&agrave;y h&ocirc;m sau, Doraemon v&agrave; c&aacute;c bạn đưa Fuko đến một v&ugrave;ng thảo nguy&ecirc;n để chơi đ&ugrave;a. Họ t&igrave;nh cờ cứu được b&eacute; Sun, em của Temujin - thần d&acirc;n Vương quốc Gi&oacute; tho&aacute;t khỏi sự truy đuổi của bộ tộc b&atilde;o tố. Để cảm ơn, Temujin đ&atilde; đưa nh&oacute;m bạn đến thăm xứ sở của m&igrave;nh. Đ&uacute;ng với t&ecirc;n gọi, ở đ&oacute; người ta lao động v&agrave; đi lại to&agrave;n nhờ bằng sức gi&oacute;. Sau một ng&agrave;y chơi đ&ugrave;a, nh&oacute;m Doraemon tạm để Fuko ở lại v&agrave; quay về nh&agrave;. Kh&ocirc;ng may Suneo bị linh hồn của ph&aacute;p sư Uranda - Tộc trưởng Bộ tộc B&atilde;o tố nhập v&agrave;o người. T&ecirc;n Uranda trong lốt của Suneo đ&atilde; quay trở về l&agrave;ng Gi&oacute; để bắt Fuko, hắn ph&aacute;t hiện ra một vật thể c&oacute; h&igrave;nh dạng như Fuko v&agrave; đặt t&ecirc;n l&agrave; Gorado...</p>', '17 x 23 cm', '2020-07-18', 200, '16000', 24, NULL, NULL, '2020-07-28 03:43:47', '2020-07-28 03:43:47'),
(57, 7, 4, 2, 'Nobita Và Chuyến Tàu Tốc Hành Ngân Hà', 'nobita-va-chuyen-tau-toc-hanh-ngan-ha', '<p>&ldquo;Đo&agrave;n t&agrave;u b&iacute; ẩn&rdquo; đ&atilde; khởi h&agrave;nh v&agrave;o vũ trụ bao la! Đ&iacute;ch đến của đo&agrave;n t&agrave;u ch&iacute;nh l&agrave; khu vui chơi giải tr&iacute; &ldquo;Đảo ước mơ&rdquo;. C&aacute;c h&agrave;nh kh&aacute;ch v&ocirc; c&ugrave;ng th&iacute;ch th&uacute; v&igrave; được vui chơi thoải m&aacute;i ở nhiều tiểu h&agrave;nh tinh kh&aacute;c nhau, m&agrave; kh&ocirc;ng ngờ m&igrave;nh đang l&agrave; con mồi của một lo&agrave;i sinh vật c&oacute; t&ecirc;n: Yadori.<br />\r\nĐo&agrave;n t&agrave;u đ&atilde; bị bọn Yadori ph&aacute; hoại. Liệu Nobita v&agrave; c&aacute;c bạn c&oacute; thể trở về Tr&aacute;i Đất an to&agrave;n? Ch&uacute;ng ta c&ugrave;ng tham gia v&agrave;o chuyến du lịch vũ trụ mạo hiểm n&agrave;y nh&eacute;!</p>', '17 x 23 cm', '2020-07-18', 100, '16000', 20, NULL, NULL, '2020-07-28 03:44:55', '2020-07-28 03:44:55'),
(58, 7, 4, 2, 'Nobita Và Lâu Đài Dưới Đáy Biển', 'nobita-va-lau-dai-duoi-day-bien', '<p>Nobita V&agrave; L&acirc;u Đ&agrave;i Dưới Đ&aacute;y Biển lấy bối cảnh về đại dương s&acirc;u thẳm, ở đ&acirc;y nh&oacute;m bạn Nobita phải đương đầu với mối nguy hiểm đe dọa to&agrave;n thể sinh vật tr&ecirc;n Tr&aacute;i Đất...</p>', '17 x 23 cm', '2020-07-17', 100, '16000', 21, NULL, NULL, '2020-07-28 03:46:16', '2020-07-28 03:46:16'),
(59, 7, 4, 1, 'Nobita Và Chuyến Phiêu Lưu Vào Xứ Quỷ', 'nobita-va-chuyen-phieu-luu-vao-xu-quy', '<p>Tập truyện N&ocirc;bita V&agrave; Chuyến Phi&ecirc;u Lưu V&agrave;o Xứ Quỷ l&agrave; nguy&ecirc;n t&aacute;c của bộ phim hoạt h&igrave;nh c&ugrave;ng t&ecirc;n. C&acirc;u chuyện lấy bối cảnh l&agrave; một thế giới ph&eacute;p thuật do N&ocirc;bita tạo ra, thế giới n&agrave;y tồn tại song song với thế giới hiện thực. Tại đ&acirc;y, nh&oacute;m bạn Đ&ocirc;r&ecirc;mon, N&ocirc;bita đ&atilde; chiến đấu anh dũng để cứu tr&aacute;i đất khỏi mối nguy hiểm cận kề. Ch&uacute;ng ta h&atilde;y bước v&agrave;o thế giới ph&eacute;p thuật k&igrave; lạ n&agrave;y v&agrave; phi&ecirc;u lưu c&ugrave;ng nh&oacute;m bạn Đ&ocirc;r&ecirc;mon nh&eacute;!</p>', '17 x 23 cm', '2020-07-07', 100, '16000', 19, NULL, NULL, '2020-07-28 03:47:32', '2020-07-28 03:47:32'),
(60, 7, 4, 2, 'Nobita Và Vương Quốc Trên Mây', 'nobita-va-vuong-quoc-tren-may', '<p>Nobita v&agrave; vương quốc tr&ecirc;n m&acirc;y l&agrave; t&aacute;c phẩm thứ 13 trong loạt phim hoạt h&igrave;nh Doraemon rất được y&ecirc;u th&iacute;ch, chuyển thể từ nguy&ecirc;n t&aacute;c truyện tranh c&ugrave;ng t&ecirc;n của t&aacute;c giả FUJIKO F FUJIO. (bộ phim được c&ocirc;ng chiếu v&agrave;o m&ugrave;a xu&acirc;n năm 1992).</p>\r\n\r\n<p>Doraemon c&ugrave;ng với nh&oacute;m bạn Nobita đ&atilde; x&acirc;y dựng một vương quốc trong mơ tr&ecirc;n những đ&aacute;m m&acirc;y. Nhưng kh&ocirc;ng ngờ, nh&oacute;m bạn lại lạc v&agrave;o thi&ecirc;n quốc, nơi m&agrave; những lo&agrave;i động vật tưởng như đ&atilde; tuyệt chủng tr&ecirc;n mặt đất đang sinh sống một c&aacute;ch hạnh ph&uacute;c... V&agrave; c&ograve;n cả những con người nơi thi&ecirc;n quốc ấy nữa, họ đến từ đ&acirc;u&hellip;?</p>\r\n\r\n<p>Ch&uacute;ng ta h&atilde;y bước v&agrave;o vương quốc bồng bềnh tr&ecirc;n những đ&aacute;m m&acirc;y c&ugrave;ng với những người bạn nh&eacute;!</p>', '17 x 23 cm', '2020-07-18', 120, '16000', 28, NULL, NULL, '2020-07-28 03:49:11', '2020-07-28 03:49:11'),
(61, 5, 6, 1, 'Phàm nhân tu tiên', 'pham-nhan-tu-tien', '<p>Ph&agrave;m Nh&acirc;n Tu Ti&ecirc;n l&agrave; một c&acirc;u chuyện&nbsp;<em>Ti&ecirc;n Hiệp</em>&nbsp;kể về H&agrave;n Lập - Một người b&igrave;nh thường nhưng lại gặp v&ocirc; v&agrave;n cơ duy&ecirc;n để bước đi tr&ecirc;n con đường tu ti&ecirc;n, kh&ocirc;ng phải anh h&ugrave;ng - cũng chẳng phải tiểu nh&acirc;n, H&agrave;n Lập từng bước khẳng định m&igrave;nh... Liệu H&agrave;n Lập v&agrave; người y&ecirc;u c&oacute; thể c&ugrave;ng bước tr&ecirc;n con đường tu ti&ecirc;n v&agrave; c&oacute; một c&aacute;i kết ho&agrave;n mỹ? Những thử th&aacute;ch n&agrave;o đang chờ đợi bọn họ?<br />\r\n<br />\r\n- Truyện kết cấu kh&aacute; hợp l&yacute;, t&igrave;nh tiết kh&ocirc;ng qu&aacute; chậm, kh&ocirc;ng qu&aacute; nhanh, diễn tả kh&aacute; đặc biệt, lời văn tr&ocirc;i chảy, nh&acirc;n vật t&iacute;nh c&aacute;ch đặc th&ugrave;. Nh&acirc;n vật ch&iacute;nh, H&agrave;n Lập, mang hơi hướng kh&aacute; c&ocirc; độc. Bạn n&agrave;o th&iacute;ch đọc Tru Ti&ecirc;n hoặc Thương Thi&ecirc;n chắc sẽ th&iacute;ch&nbsp;<em>Ph&agrave;m Nh&acirc;n Tu Ti&ecirc;n Truyện</em>. Mời bạn đọc c&ugrave;ng thưởng thức v&agrave; d&otilde;i theo bước ch&acirc;n của H&agrave;n Lập!</p>', '30  x 20 cm', '2020-07-18', 1000, '1000000', 21, NULL, NULL, '2020-07-28 03:59:16', '2020-07-28 03:59:16'),
(62, 5, 6, 2, 'HUYỀN GIỚI CHI MÔN', 'huyen-gioi-chi-mon', '<p>Trời gi&aacute;ng Thần vật! Dị huyết phụ thể!<br />\r\n<br />\r\nQuần Ti&ecirc;n sợ h&atilde;i! Vạn Ma tr&aacute;nh lui!<br />\r\n<br />\r\nMột thiếu ni&ecirc;n xuất&nbsp;th&acirc;n&nbsp;từ đại lục Đ&ocirc;ng Ch&acirc;u.<br />\r\n<br />\r\nMệnh sinh tử gắn liền với Hồng Phấn Kh&ocirc; L&acirc;u.<br />\r\n<br />\r\n&Yacute; ch&iacute; ki&ecirc;n định quyết trở th&agrave;nh đệ nhất cường giả.<br />\r\n<br />\r\nTạo n&ecirc;n truyền thuyết uy vũ khắp ng&acirc;n h&agrave;, đại n&aacute;o khắp ba c&otilde;i.</p>', '17 x 23 cm', '2020-07-04', 10000, '100000', 24, NULL, NULL, '2020-07-28 04:00:37', '2020-07-28 04:00:37'),
(63, 5, 7, 1, 'VŨ ĐỘNG CÀN KHÔN', 'vu-dong-can-khon', '<p>Giới Thiệu Truyện<br />\r\n<br />\r\n<strong>Vũ Động C&agrave;n Kh&ocirc;n - Từ xưa đến nay, con người lu&ocirc;n c&oacute; kh&aacute;t vọng vươn l&ecirc;n đỉnh cao, để đổi lấy sự th&agrave;nh c&ocirc;ng họ đ&atilde; hy sinh biết bao nhi&ecirc;u mồ h&ocirc;i v&agrave; nước mắt. Tu Luyện ch&iacute;nh l&agrave; trộm đi &acirc;m dương, đoạt lấy tạo h&oacute;a, chuyển đổi Niết B&agrave;n, nắm giữ sinh tử, chưởng quản Lu&acirc;n Hồi.</strong><br />\r\n<br />\r\n- C&oacute; người theo đuổi quyền lực v&agrave; sức mạnh, c&oacute; người th&igrave; truy cầu t&igrave;nh y&ecirc;u, nhưng sống trong thời loạn kh&ocirc;ng thể kh&ocirc;ng c&oacute; thực lực trong tay, muốn c&oacute; chỗ đứng hay bảo vệ những người thương y&ecirc;u đều cần phải c&oacute; thực lực, đ&oacute; l&agrave; ch&acirc;n l&yacute; kh&ocirc;ng bao giờ thay đổi tự cổ ch&iacute; kim.<br />\r\n<br />\r\n- C&acirc;u chuyện l&agrave; một trang s&aacute;ch n&oacute;i về cuộc đời của một tu luyện giả, b&ecirc;n cạnh đ&oacute; c&ograve;n mi&ecirc;u tả cuộc sống đời thường, những suy nghĩ b&igrave;nh thường, nhưng nếu như số mệnh đ&atilde; sắp đặt bản th&acirc;n phải đi tr&ecirc;n một con đường đầy gian khổ v&agrave; trắc trở th&igrave; sao? Con người đặc biệt lu&ocirc;n c&oacute; số mệnh đặc biệt, bất kể xuất th&acirc;n, bất kể địa vị. Ch&uacute;ng ta h&atilde;y c&ugrave;ng d&otilde;i theo những bước ch&acirc;n của họ, kh&ocirc;ng chỉ đơn giản l&agrave; xem những pha đ&aacute;nh nhau đẹp mắt, hay những chi&ecirc;u thức tr&aacute;ng lệ, cũng kh&ocirc;ng phải chỉ để b&agrave;n luận đến những &acirc;m mưu hay kế s&aacute;ch th&acirc;m độc, h&egrave;n hạ bỉ ổi, tuyệt vời ra sao. Thứ ch&uacute;ng ta cần l&agrave; một c&aacute;i g&igrave; đ&oacute; lớn hơn thế, tất cả mọi thứ h&ograve;a quyện lại với nhau tạo ra một sự li&ecirc;n kết tuyệt vời, v&agrave; sự li&ecirc;n kết đ&oacute; ch&iacute;nh l&agrave; V&otilde; Động C&agrave;n Kh&ocirc;n. C&ograve;n chờ g&igrave; nữa, c&aacute;c bạn h&atilde;y nhanh đọc lướt qua phần giới thiệu để c&ugrave;ng đắm m&igrave;nh trong thế giới của&nbsp;<strong>Vũ Động C&agrave;n Kh&ocirc;n</strong>, thưởng thức c&aacute;i hay v&agrave; hấp dẫn kh&ocirc;ng thể cưỡng lại của t&aacute;c phẩm.<br />\r\n<br />\r\n- C&acirc;u chuyện kể về một thiếu ni&ecirc;n L&acirc;m Động, vốn l&agrave; thiếu ni&ecirc;n b&igrave;nh thường, cha của hắn l&agrave; một cao thủ nhưng bị người kh&aacute;c đ&aacute;nh trọng thương kh&oacute; c&oacute; thể phục hồi, từ đ&oacute; địa vị trong gia đ&igrave;nh ng&agrave;y c&agrave;ng giảm s&uacute;t. D&ugrave; bị khinh thị v&agrave; chế giễu nhưng L&acirc;m Động với sự quyết t&acirc;m kh&ocirc;ng ngừng nghỉ, r&egrave;n luyện đi&ecirc;n cuồng chờ ng&agrave;y b&aacute;o th&ugrave;.<br />\r\n<br />\r\n- &Ocirc;ng trời c&oacute; mắt kh&ocirc;ng phụ người c&oacute; l&ograve;ng, L&acirc;m Động may mắn gặp được kỳ ngộ, nhặt được một vi&ecirc;n đ&aacute; kỳ lạ, m&agrave; c&ocirc;ng dụng của n&oacute; nếu lọt ra ngo&agrave;i th&igrave; chắc chắn sẽ khiến thế gian đi&ecirc;n cuồng, rốt cuộc vi&ecirc;n đ&aacute; đ&oacute; c&oacute; t&aacute;c dụng g&igrave;? V&agrave; xuất xứ từ đ&acirc;u?<br />\r\n<br />\r\n- Từng bước đi tr&ecirc;n con đường cường giả, đối mặt với những địch thủ đỉnh cấp, liệu L&acirc;m Động sẽ th&agrave;nh c&ocirc;ng hay thất bại? Sẽ trở th&agrave;nh một cường giả đứng tr&ecirc;n vạn người, được họ k&iacute;nh ngưỡng hay cũng giống như cha m&igrave;nh kh&ocirc;ng thể ng&oacute;c đầu l&ecirc;n nổi... Mời c&aacute;c bạn c&ugrave;ng theo d&otilde;i!</p>', '17 x 23 cm', '2020-07-02', 1000, '100000', 12, NULL, NULL, '2020-07-28 04:02:59', '2020-07-28 06:11:53'),
(64, 6, 5, 1, 'Anh hùng xạ điêu', 'anh-hung-xa-dieu', '<p>Anh h&ugrave;ng xạ đi&ecirc;u l&agrave; cuốn tiểu thuyết v&otilde; hiệp nổi tiếng của nh&agrave; văn Kim Dung - Trung Quốc. Đ&acirc;y tưởng như chỉ l&agrave; một t&aacute;c phẩm giải tr&iacute; th&ocirc;ng tục nhưng bằng t&agrave;i năng của m&igrave;nh, t&aacute;c giả Kim Dung đ&atilde; n&acirc;ng n&oacute; l&ecirc;n th&agrave;nh một t&aacute;c phẩm văn chương c&oacute; gi&aacute; trị, cho người đọc c&oacute; c&aacute;ch nh&igrave;n mới về những vấn đề đ&atilde; cũ của con người v&agrave; thế giới. Cuốn tiểu thuyết v&otilde; hiệp n&agrave;y mang bối cảnh của lịch sử Trung Quốc thời Tống - Li&ecirc;u, thời kỳ nh&acirc;n d&acirc;n Trung Quốc đang r&ecirc;n xiết dưới g&oacute;t gi&agrave;y ngoại tộc. Cuộc đời v&agrave; t&iacute;nh c&aacute;ch c&aacute;c nh&acirc;n vật trong t&aacute;c phẩm tạo n&ecirc;n những bi kịch hiếm c&oacute;, l&ocirc;i cuốn rất nhiều thế hệ độc giả. Xin mời c&aacute;c bạn h&atilde;y c&ugrave;ng t&igrave;m đọc Anh h&ugrave;ng xạ đi&ecirc;u để c&oacute; c&aacute;ch nh&igrave;n mới về con người v&agrave; thế giới. Xin giới thiệu đến bạn bộ tiểu thuyết v&otilde; hiệp nổi tiếng của Kim Dung: Anh H&ugrave;ng Xạ Đi&ecirc;u, do Nh&agrave; xuất bản Văn Học xuất bản. Đ&acirc;y l&agrave; bản dịch mới của dịch giả Cao Tự Thanh theo đ&uacute;ng nguy&ecirc;n t&aacute;c đ&atilde; được Kim Dung chỉnh l&yacute;, n&acirc;ng cao. Nhiều chi tiết, nh&acirc;n vật ho&agrave;n to&agrave;n kh&aacute;c biệt so với bản dịch cũ.</p>\r\n\r\n<p>Bộ s&aacute;ch được chia l&agrave;m 8 tập, in b&igrave;a mềm. Giống như nhiều t&aacute;c phẩm của Kim Dung, Anh H&ugrave;ng Xạ Đi&ecirc;u mang bối cảnh lịch sử Trung Quốc thời Tống - Kim. Truyện n&oacute;i về hai gia đ&igrave;nh họ Dương v&agrave; Họ Qu&aacute;ch; trong đ&oacute; n&oacute;i về t&iacute;nh c&aacute;ch của hai nh&acirc;n vật ch&iacute;nh l&agrave; Qu&aacute;ch Tĩnh: một qu&acirc;n tử h&aacute;n, đầu đội trời ch&acirc;n đạp đất v&agrave; Dương Khang: một ngụy qu&acirc;n tử ham sang phụ kh&oacute;, nhận giặc l&agrave;m cha. Xen lẫn với những cuộc giao đấu tranh h&ugrave;ng l&agrave; cuộc t&igrave;nh trắc trở nhưng đầy thơ mộng của Qu&aacute;ch Tĩnh v&agrave; Ho&agrave;ng Dung (một c&ocirc; b&eacute; th&ocirc;ng minh, tinh nghịch, dễ thương v&agrave; đầy c&aacute; t&iacute;nh). Anh H&ugrave;ng Xạ Đi&ecirc;u sửa chữa lần n&agrave;y c&oacute; thay đổi rất nhiều. Bỏ một số t&igrave;nh tiết về lịch sử v&agrave; nh&acirc;n vật kh&ocirc;ng cần thiết như về con tiểu hồng m&atilde;, trận đ&aacute;nh giữa c&oacute;c v&agrave; b&ograve; cạp, Thiết chưởng bang l&agrave;m &aacute;c, bỏ nh&acirc;n vật Tần Nam Cầm, hợp nhất n&agrave;ng v&agrave;o với nh&acirc;n vật Mục Niệm Từ. Lại th&ecirc;m một số t&igrave;nh tiết mới, như Trương Thập Ngũ kể chuyện, Kh&uacute;c Linh Phong trộm tranh ở đoạn mở đầu, Ho&agrave;ng Dung &eacute;p người khi&ecirc;ng kiệu v&agrave; gặp mưa ở Trường Lĩnh, Ho&agrave;ng Thường soạn Cửu &acirc;m ch&acirc;n kinh v.v&hellip; Anh H&ugrave;ng Xạ Đi&ecirc;u từ chỗ l&agrave; một tiểu thuyết th&ocirc;ng tục giải tr&iacute; đăng b&aacute;o nhiều kỳ nay được chỉnh l&yacute; n&acirc;ng l&ecirc;n th&agrave;nh một t&aacute;c phẩm văn chương c&oacute; thể gợi &yacute; cho người đọc những g&oacute;c nh&igrave;n mới về nhiều vấn đề cũ của con người v&agrave; thế giới.</p>', '17x 23  cm', '2020-07-05', 1000, '100000', 23, NULL, NULL, '2020-07-28 04:05:03', '2020-07-28 04:05:03'),
(65, 6, 5, 2, 'Thiên Long Bát Bộ', 'thien-long-bat-bo', '<p>T&aacute;c phẩm lấy bối cảnh thời Tống Triết T&ocirc;ng giai đoạn đ&aacute;nh dấu chế độ phong kiến Trung Quốc chuyển từ thịnh sang suy. S&aacute;u nước: L&yacute;, Tống, Li&ecirc;u, Kim, Y&ecirc;n &nbsp;l&uacute;c th&igrave; li&ecirc;n kết đồng minh, l&uacute;c lại nh&ograve;m ng&oacute;, th&ocirc;n t&iacute;nh lẫn nhau. Trong mớ b&ograve;ng bong m&acirc;u thuẫn ấy, nổi bật l&ecirc;n xung đột hai nước Tống - Li&ecirc;u với đỉnh điểm tập trung v&agrave;o KIều Phong (nay l&agrave; Ti&ecirc;u Phong) &nbsp;- nh&acirc;n vật c&oacute; số phận tận c&ugrave;ng bất hạnh v&agrave; nh&acirc;n c&aacute;ch tuyệt vời cao thượng. C&oacute; thể n&oacute;i,&nbsp;<strong>Thi&ecirc;n Long B&aacute;t Bộ&nbsp;</strong>đ&atilde; vượt qua giới hạn của tiểu thuyết lịch sử truyền thống, đồng thời cũng vượt qua giới hạn của tiểu thuyết v&otilde; hiệp, l&agrave;m n&ecirc;n đỉnh cao trong sự nghiệp của Kim Dung.</p>\r\n\r\n<p>Gi&aacute; sản phẩm tr&ecirc;n Tiki đ&atilde; bao gồm thuế theo luật hiện h&agrave;nh. Tuy nhi&ecirc;n tuỳ v&agrave;o từng loại sản phẩm hoặc phương thức, địa chỉ giao h&agrave;ng m&agrave; c&oacute; thể ph&aacute;t sinh th&ecirc;m chi ph&iacute; kh&aacute;c như ph&iacute; vận chuyển, phụ ph&iacute; h&agrave;ng cồng kềnh, ...</p>', '17 x 23 cm', '2020-07-11', 300, '123567', 21, NULL, NULL, '2020-07-28 04:06:31', '2020-07-28 04:06:31'),
(66, 6, 5, 2, 'Lộc đỉnh ký', 'loc-dinh-ky', '<p>Trong loạt s&aacute;ch kiếm hiệp nổi tiếng của Kim Dung,&nbsp;<strong>Lộc Đỉnh K&yacute;&nbsp;</strong>l&agrave; một trong những t&aacute;c phẩm được y&ecirc;u th&iacute;ch nhất. Nếu ở c&aacute;c t&aacute;c phẩm kh&aacute;c, nh&acirc;n vật ch&iacute;nh l&agrave; c&aacute;c hiệp kh&aacute;ch v&otilde; c&ocirc;ng cao cường, nh&acirc;n t&acirc;m hiệp cốt, trừ gian diệt bạo, th&igrave;&nbsp;<strong>Lộc đỉnh k&yacute;</strong>&nbsp;mang m&agrave;u sắc mới lạ, thu h&uacute;t ở chỗ nh&acirc;n vật ch&iacute;nh xuất th&acirc;n h&egrave;n k&eacute;m v&agrave; ho&agrave;n to&agrave;n kh&ocirc;ng phải người ch&iacute;nh trực.</p>\r\n\r\n<p>Vi Tiểu Bảo l&agrave; một nh&acirc;n vật c&oacute; khắc họa kh&aacute; đặc biệt, tuy kh&ocirc;ng biết chữ, chẳng biết v&otilde; c&ocirc;ng, nhưng nhờ c&oacute; miệng lưỡi trơn như mỡ, &oacute;c thực dụng, t&iacute;nh &iacute;ch kỷ, tiểu nh&acirc;n điển h&igrave;nh cộng với đầu &oacute;c linh hoạt ứng biến nhanh nhạy m&agrave; đạt được nhiều th&agrave;nh c&ocirc;ng, danh lợi. Vi Tiểu Bảo c&oacute; những n&eacute;t hao hao giống những nh&acirc;n vật ch&iacute;nh m&agrave; Kim Dung đ&atilde; d&agrave;n dựng: trọng t&igrave;nh nghĩa bạn b&egrave;, bị đưa đẩy v&agrave;o những t&igrave;nh thế tiến tho&aacute;i lưỡng nan, y&ecirc;u một cuộc sống b&igrave;nh dị... nhưng cũng bao gồm những t&iacute;nh kh&aacute;c như tiểu nh&acirc;n gian xảo, mưu m&ocirc; thủ đoạn... Qua đ&oacute; Kim Dung cũng x&acirc;y dựng một nh&acirc;n vật điển h&igrave;nh cho một bộ phận d&acirc;n tộc Trung Quốc tương phản với AQ của Lỗ Tấn.</p>\r\n\r\n<p>Gi&aacute; sản phẩm tr&ecirc;n Tiki đ&atilde; bao gồm thuế theo luật hiện h&agrave;nh. Tuy nhi&ecirc;n tuỳ v&agrave;o từng loại sản phẩm hoặc phương thức, địa chỉ giao h&agrave;ng m&agrave; c&oacute; thể ph&aacute;t sinh th&ecirc;m chi ph&iacute; kh&aacute;c như ph&iacute; vận chuyển, phụ ph&iacute; h&agrave;ng cồng kềnh, ...</p>', '23 x 17 cm', '2020-07-23', 200, '2333333', 21, NULL, NULL, '2020-07-28 04:07:53', '2020-07-28 04:07:53');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`, `thumbnail_image`, `priority`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(38, 20, '28072020-910-ho-tuyet-menh-1-500x794.jpg', '28072020-974-ho-tuyet-menh-1-500x794.jpg', '0', 'avatar', NULL, '2020-07-22 08:22:28', '2020-07-28 03:06:59'),
(39, 21, '28072020-222-tiec-bao-thu.jpg', '28072020-99-tiec-bao-thu.jpg', '0', 'avatar', NULL, '2020-07-28 02:26:32', '2020-07-28 03:16:50'),
(40, 21, '28072020-756-tai-xuong.jpg', '28072020-923-tai-xuong.jpg', '0', 'show', NULL, '2020-07-28 03:16:04', '2020-07-28 03:16:50'),
(41, 22, '28072020-255-ao-linh-ky-quy-co-nu.jpg', '28072020-420-ao-linh-ky-quy-co-nu.jpg', '0', 'avatar', NULL, '2020-07-28 03:21:26', '2020-07-28 03:21:26'),
(42, 51, '28072020-910-ho-tuyet-menh-1-500x794.jpg', '28072020-974-ho-tuyet-menh-1-500x794.jpg', '0', 'show', NULL, '2020-07-28 03:25:55', '2020-08-14 01:43:44'),
(43, 52, '28072020-375-kaat.jpg', '28072020-119-kaat.jpg', '0', 'avatar', NULL, '2020-07-28 03:26:54', '2020-07-28 03:26:54'),
(44, 53, '28072020-973-dtdc.jpg', '28072020-188-dtdc.jpg', '0', 'avatar', NULL, '2020-07-28 03:30:12', '2020-07-28 03:30:12'),
(45, 54, '28072020-567-to-dong-ro-mau-500x800.jpg', '28072020-966-to-dong-ro-mau-500x800.jpg', '0', 'avatar', NULL, '2020-07-28 03:32:13', '2020-07-28 03:32:13'),
(46, 55, '28072020-392-phieuluuvaoxuquy.jpg', '28072020-842-phieuluuvaoxuquy.jpg', '0', 'avatar', NULL, '2020-07-28 03:40:46', '2020-07-28 03:40:46'),
(47, 56, '28072020-597-phapsugio.jpg', '28072020-111-phapsugio.jpg', '0', 'avatar', NULL, '2020-07-28 03:43:47', '2020-07-28 03:43:47'),
(48, 57, '28072020-853-tautocthanh.jpg', '28072020-119-tautocthanh.jpg', '0', 'avatar', NULL, '2020-07-28 03:44:55', '2020-07-28 03:44:55'),
(49, 58, '28072020-134-laudaiduoidaybien.jpg', '28072020-178-laudaiduoidaybien.jpg', '0', 'avatar', NULL, '2020-07-28 03:46:16', '2020-07-28 03:46:16'),
(50, 59, '28072020-630-phieuluuvaoxuquy.jpg', '28072020-930-phieuluuvaoxuquy.jpg', '0', 'avatar', NULL, '2020-07-28 03:47:32', '2020-07-28 03:47:32'),
(51, 60, '28072020-247-vuongquoctrenmay.jpg', '28072020-285-vuongquoctrenmay.jpg', '0', 'avatar', NULL, '2020-07-28 03:49:11', '2020-07-28 03:49:11'),
(52, 61, '28072020-205-pham-nhan-tu-tien-vong-ngu.jpg', '28072020-756-pham-nhan-tu-tien-vong-ngu.jpg', '0', 'avatar', NULL, '2020-07-28 03:59:16', '2020-07-28 03:59:16'),
(53, 62, '28072020-718-tai-xuong.jpg', '28072020-934-tai-xuong.jpg', '0', 'avatar', NULL, '2020-07-28 04:00:37', '2020-07-28 04:00:37'),
(54, 63, '28072020-116-images.jpg', '28072020-895-images.jpg', '0', 'avatar', NULL, '2020-07-28 04:02:59', '2020-07-28 04:02:59'),
(55, 64, '28072020-207-tai-xuong-1.jpg', '28072020-100-tai-xuong-1.jpg', '0', 'avatar', NULL, '2020-07-28 04:05:03', '2020-07-28 04:05:03'),
(56, 65, '28072020-310-thien-long-bat-bo-final-01.jpg', '28072020-610-thien-long-bat-bo-final-01.jpg', '0', 'avatar', NULL, '2020-07-28 04:06:31', '2020-07-28 04:06:31'),
(57, 66, '28072020-478-loc-dinh-ky-01-1-u547-d20160401-t085455.jpg', '28072020-97-loc-dinh-ky-01-1-u547-d20160401-t085455.jpg', '0', 'avatar', NULL, '2020-07-28 04:07:53', '2020-07-28 04:07:53'),
(58, 51, '14082020-221-22072020-89-tai-xuong.jpg', '14082020-728-22072020-89-tai-xuong.jpg', '0', 'avatar', NULL, '2020-08-14 01:43:37', '2020-08-14 01:43:44');

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `promotion_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promotion_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion_percent` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion_price` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion_max` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_start` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_end` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `promotion_name`, `promotion_code`, `promotion_percent`, `promotion_price`, `promotion_max`, `status`, `date_start`, `date_end`, `created_at`, `updated_at`) VALUES
(4, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', '2020-07-02', '2020-07-25', NULL, '2020-07-20 02:59:47'),
(6, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', '2020-07-16', '2020-07-23', NULL, '2020-07-22 04:23:34'),
(7, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', NULL, NULL, NULL, NULL),
(8, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', NULL, NULL, NULL, '2020-07-17 01:58:17'),
(10, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', NULL, NULL, NULL, NULL),
(11, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', NULL, NULL, NULL, NULL),
(12, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', NULL, NULL, NULL, NULL),
(13, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', NULL, NULL, NULL, NULL),
(15, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', NULL, NULL, NULL, NULL),
(16, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', NULL, NULL, NULL, NULL),
(17, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', NULL, NULL, NULL, NULL),
(18, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', NULL, NULL, NULL, NULL),
(19, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', NULL, NULL, NULL, NULL),
(20, 'Tháng 9', NULL, '121212', '212112', '212121', 'apply', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `provider`
--

CREATE TABLE `provider` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provider`
--

INSERT INTO `provider` (`id`, `provider_name`, `provider_phone`, `provider_address`, `provider_email`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Văn A', '0123456789', 'Cái gì đó', '2@gmail.com', '2020-08-11 07:48:02', '2020-08-11 10:21:06'),
(3, 'Nguyễn Văn B', '0399212832', '58 Tố hữu', 'hello@gmail.com', '2020-08-12 02:12:27', '2020-08-12 02:12:27');

-- --------------------------------------------------------

--
-- Table structure for table `reply_comments`
--

CREATE TABLE `reply_comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subcategorys`
--

CREATE TABLE `subcategorys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `subcategory_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_subcategory_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategorys`
--

INSERT INTO `subcategorys` (`id`, `category_id`, `subcategory_name`, `url_subcategory_name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 1, 'Dân gian', 'dan-gian', NULL, NULL, '2020-07-16 10:40:06', '2020-07-20 07:57:18'),
(3, 1, 'Phương Đông', 'phuong-dong', NULL, NULL, '2020-07-22 02:22:31', '2020-07-28 02:06:57'),
(4, 1, 'Phương Tây', 'phuong-tay', NULL, NULL, '2020-07-28 02:07:16', '2020-07-28 02:07:16'),
(5, 2, 'Tiên hiệp', 'tien-hiep', NULL, NULL, '2020-07-28 02:07:49', '2020-07-28 03:53:11'),
(6, 2, 'Kiếm hiệp', 'kiem-hiep', NULL, NULL, '2020-07-28 02:09:36', '2020-07-28 03:53:19'),
(7, 3, 'Nhật bản', 'nhat-ban', NULL, NULL, '2020-07-28 02:10:16', '2020-07-28 02:10:16'),
(8, 3, 'Cổ tích Việt Nam', 'co-tich-viet-nam', NULL, NULL, '2020-07-28 02:10:36', '2020-07-28 02:10:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'customer',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `block` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `email`, `email_verified_at`, `password`, `social_id`, `avatar`, `status`, `block`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'nguyenthanh', '123@gmail.com', NULL, '$2y$10$hMzomshEOMJhKaJb8RzyHO4ZWRsyLpTUizkiHYbAthOhi08eujbh6', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-15 09:49:48', '2020-07-15 09:49:48'),
(2, 'customer', 'khachhang', '1234@gmail.com', NULL, '$2y$10$JsVChs7MgyTfkZAIJECY.elmA8OK.i.QAF7iwBGR0w8P.zxs3gpeS', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-28 06:07:35', '2020-07-28 06:07:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `authors_author_name_unique` (`author_name`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_user_id_foreign` (`user_id`),
  ADD KEY `cart_product_id_foreign` (`product_id`);

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_product_id_foreign` (`product_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_import`
--
ALTER TABLE `detail_import`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_import_product_id_foreign` (`product_id`),
  ADD KEY `detail_import_import_id_foreign` (`import_id`);

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_order_order_id_foreign` (`order_id`),
  ADD KEY `detail_order_product_id_foreign` (`product_id`);

--
-- Indexes for table `enjoy`
--
ALTER TABLE `enjoy`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `enjoy_user_id_unique` (`user_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `import`
--
ALTER TABLE `import`
  ADD PRIMARY KEY (`id`),
  ADD KEY `import_provider_id_foreign` (`provider_id`),
  ADD KEY `import_user_id_foreign` (`user_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messages_from_user_foreign` (`from_user`),
  ADD KEY `messages_to_user_foreign` (`to_user`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_from_user_foreign` (`from_user`),
  ADD KEY `notification_to_user_foreign` (`to_user`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_user_id_foreign` (`user_id`),
  ADD KEY `order_promotion_id_foreign` (`promotion_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persons_user_id_unique` (`user_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_subcategory_id_foreign` (`subcategory_id`),
  ADD KEY `products_author_id_foreign` (`author_id`),
  ADD KEY `products_company_id_foreign` (`company_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reply_comments`
--
ALTER TABLE `reply_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reply_comments_comment_id_foreign` (`comment_id`);

--
-- Indexes for table `subcategorys`
--
ALTER TABLE `subcategorys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subcategorys_category_id_foreign` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `detail_import`
--
ALTER TABLE `detail_import`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `enjoy`
--
ALTER TABLE `enjoy`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `import`
--
ALTER TABLE `import`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `persons`
--
ALTER TABLE `persons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `provider`
--
ALTER TABLE `provider`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `reply_comments`
--
ALTER TABLE `reply_comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subcategorys`
--
ALTER TABLE `subcategorys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `cart_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `detail_import`
--
ALTER TABLE `detail_import`
  ADD CONSTRAINT `detail_import_import_id_foreign` FOREIGN KEY (`import_id`) REFERENCES `import` (`id`),
  ADD CONSTRAINT `detail_import_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD CONSTRAINT `detail_order_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`),
  ADD CONSTRAINT `detail_order_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `enjoy`
--
ALTER TABLE `enjoy`
  ADD CONSTRAINT `enjoy_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `import`
--
ALTER TABLE `import`
  ADD CONSTRAINT `import_provider_id_foreign` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`),
  ADD CONSTRAINT `import_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_from_user_foreign` FOREIGN KEY (`from_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `messages_to_user_foreign` FOREIGN KEY (`to_user`) REFERENCES `users` (`id`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_from_user_foreign` FOREIGN KEY (`from_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `notification_to_user_foreign` FOREIGN KEY (`to_user`) REFERENCES `users` (`id`);

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_promotion_id_foreign` FOREIGN KEY (`promotion_id`) REFERENCES `promotion` (`id`),
  ADD CONSTRAINT `order_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `persons`
--
ALTER TABLE `persons`
  ADD CONSTRAINT `persons_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`),
  ADD CONSTRAINT `products_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `products_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategorys` (`id`);

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `reply_comments`
--
ALTER TABLE `reply_comments`
  ADD CONSTRAINT `reply_comments_comment_id_foreign` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`);

--
-- Constraints for table `subcategorys`
--
ALTER TABLE `subcategorys`
  ADD CONSTRAINT `subcategorys_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categorys` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
