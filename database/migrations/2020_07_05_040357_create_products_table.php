<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('subcategory_id');
            $table->unsignedBigInteger('author_id');
            $table->unsignedBigInteger('company_id');
            $table->string('product_name', 255);
            $table->string('url_product');
            $table->text('description');
            $table->string('length_width');
            $table->string('publication_date');
            $table->integer('page_number')->default(0);
            $table->string('price',100)->default(0);
            $table->integer('quatity')->default(0);
            $table->string('status')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('subcategory_id')->references('id')->on('subcategorys');
            $table->foreign('author_id')->references('id')->on('authors');
            $table->foreign('company_id')->references('id')->on('company');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
