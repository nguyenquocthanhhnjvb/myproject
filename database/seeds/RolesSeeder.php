<?php

use Illuminate\Database\Seeder;
use App\Models\RolesModel;
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RolesModel::create(['roleName' => 'admin']);
        RolesModel::create(['roleName' => 'user']);
    }
}
