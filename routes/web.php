<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', 'ProductsController@index');

// Route::get('cart', 'ProductsController@cart');

// Route::get('add-to-cart/{id}', 'ProductsController@addToCart');

// Route::patch('update-cart', 'ProductsController@update');

// Route::delete('remove-from-cart', 'ProductsController@remove');

// Route::get('/admin','AdminController@index')->middleware('checklogin:admin')->name('adminIndex');


Route::get('/logout', 'Admin\LoginController@logout')->name('logout');

Route::get('/login', 'Admin\LoginController@index');

Route::post('/login', 'Admin\LoginController@login')->name('login');

Route::get('/register', 'Admin\RegisterController@index');

Route::post('/register','Admin\RegisterController@register')->name('register');

Route::get('/forgot', 'Admin\ForgotController@index');


//frontend
Route::get('/home', 'Index\HomeController@index')->name('home');

Route::get('/', 'Index\HomeController@index')->name('home');

Route::post('/search', 'Index\HomeController@search')->name('search');

Route::get('/productinfo/{name}/{id}', 'Index\HomeController@productInfo')->name('proInfo');

Route::get('/productlist', 'Index\HomeController@productlist')->name('proList');

Route::get('/productlist/{where}/{name}/{id}/{key}', 'Index\HomeController@productlist')->name('proListGetWhere');

Route::get('/cart', 'Index\CartController@index')->name('cart');

Route::post('/cart/loginModal', 'Index\CartController@loginModal')->name('cartlogin');

Route::get('/checkout', 'Index\OrderController@index')->name('checkout');

Route::post('/checkout', 'Index\OrderController@addOrder')->name('addOrder');

Route::post('/addComments/{userid}/{prdid}','Index\CommentsController@addComments')->name('addComments');

Route::post('/buy', 'Index\CartController@addPrd')->name('addPrd');

Route::post('/UcartQuatity', 'Index\CartController@UcartQuatity')->name('UcartQuatity');

Route::post('/DcartPrd', 'Index\CartController@DcartPrd')->name('DcartPrd');

Route::get('/userinfo', 'Index\UserinfoController@index')->name('userinfo');

Route::post('/userinfor/order/{id}','Index\UserinfoController@cancel')->name('cancel')->where('id', '[0-9]+');

Route::get('/userinfor/orderdetail/{id}','Index\UserinfoController@orderdetail')->name('orderdetail')->where('id', '[0-9]+');

Route::post('/userinfo/person','Index\UserinfoController@person')->name('person');


// admin Route
Route::middleware('checklogin:admin')->group(function () {
	Route::prefix('boss')->group(function () {
		Route::get('/', function () {
			return view('backend.adminpage');
		});

		// Danh mục
		Route::get('/listcategory','Admin\CategoryController@index')->name('category');
		Route::get('/category','Admin\CategoryController@addcategory_sub')->name('add');
		Route::post('/category','Admin\CategoryController@addcategory')->name('addcategory');
		Route::post('/subcategory','Admin\CategoryController@addSubCategory')->name('addsubcategory');

		Route::post('/Fsub/{id}','Admin\CategoryController@findSubCategory')->name('findsub')->where('id', '[0-9]+');
		Route::post('/Esub/{id}','Admin\CategoryController@EditSub')->name('EditSub')->where('id', '[0-9]+');
		Route::post('/Dsub/{id}','Admin\CategoryController@DeleteSub')->name('DeleteSub')->where('id', '[0-9]+');

		Route::post('/Fcate/{id}','Admin\CategoryController@findCategory')->name('findCate')->where('id', '[0-9]+');
		Route::post('/Ecate/{id}','Admin\CategoryController@EditCate')->name('EditCate')->where('id', '[0-9]+');
		Route::post('/Dcate/{id}','Admin\CategoryController@DeleteCate')->name('DeleteCate')->where('id', '[0-9]+');

		// Tác giả
		Route::get('/authors','Admin\authorController@index')->name('author');
		Route::get('/addauthors','Admin\authorController@formAdd')->name('formAdd');
		Route::post('/authors','Admin\authorController@addAuthor')->name('addAuthor');
		Route::post('/Dauthors/{id}','Admin\authorController@DeleteAuthor')->name('DeleteAuthor')->where('id', '[0-9]+');
		Route::post('/Eauthors/{id}','Admin\authorController@EditAuthor')->name('EditAuthor')->where('id', '[0-9]+');

		// Nhà xuất bản
		Route::get('/company','Admin\CompanyController@index')->name('company');
		Route::post('/company','Admin\CompanyController@AddCompany')->name('AddCompany');
		Route::post('/Dcompany/{id}','Admin\CompanyController@DeleteCompany')->name('DeleteCompany')->where('id', '[0-9]+');
		Route::post('/Ecompany/{id}','Admin\CompanyController@EditCompany')->name('EditCompany')->where('id', '[0-9]+');
		Route::post('/Fcompany/{id}','Admin\CompanyController@FindCompany')->name('FindCompany')->where('id', '[0-9]+');

		// Khuyến mãi
		Route::get('/listpromotion','Admin\PromotionController@index')->name('listPro');
		Route::get('/promotion','Admin\PromotionController@AddProform')->name('addProform');
		Route::post('/promotion','Admin\PromotionController@AddPromotion')->name('AddPromotion');
		Route::post('/Dpromotion/{id}','Admin\PromotionController@DeletePro')->name('DeletePro')->where('id', '[0-9]+');
		Route::post('/Epromotion/{id}','Admin\PromotionController@EditPro')->name('EditPro')->where('id', '[0-9]+');
		Route::post('/Fpromotion/{id}','Admin\PromotionController@FindPro')->name('FindPro')->where('id', '[0-9]+');
		Route::post('/Bpromotion/{id}','Admin\PromotionController@blockPro')->name('blockPro')->where('id', '[0-9]+');

		// Sản phẩm
		Route::get('/listProducts','Admin\ProductsController@index')->name('listProducts');
		Route::get('/addproduct','Admin\ProductsController@formAddProduct')->name('formAddProduct');
		Route::post('/addproduct','Admin\ProductsController@AddProduct')->name('AddProduct');
		Route::get('/productdetal/{id}','Admin\ProductsController@ProductDetal')->name('productdetal')->where('id', '[0-9]+');
		Route::post('/Dproduct/{id}','Admin\ProductsController@DeleteProduct')->name('DeleteProduct')->where('id', '[0-9]+');
		Route::post('/Eproduct/{id}','Admin\ProductsController@EditProduct')->name('EditProduct')->where('id', '[0-9]+');

		Route::post('/DproductImage/{id}','Admin\ProductsController@DeleteProductImg')->name('DeleteProductImg')->where('id', '[0-9]+');
		Route::post('/FproductImage/{id}','Admin\ProductsController@FindProductImgByID')->name('FindProductImg')->where('id', '[0-9]+');
		Route::post('/EproductImage/{id}','Admin\ProductsController@EditProductImg')->name('EditProductImg')->where('id', '[0-9]+');
		Route::post('/BproductImage/{id}','Admin\ProductsController@BlockProductImg')->name('BlockProductImg')->where('id', '[0-9]+');
		Route::post('/AproductImage/{id}','Admin\ProductsController@AvatarProductImg')->name('AvatarProductImg')->where('id', '[0-9]+');
		Route::post('/AMproductImage/{id}','Admin\ProductsController@AddMoreProductImg')->name('AddMoreProductImg')->where('id', '[0-9]+');

		// nhà cung cấp
		Route::get('/listprovider','Admin\ProviderController@index')->name('listprovider');
		Route::get('/provider','Admin\ProviderController@addform')->name('provider');
		Route::post('/provider','Admin\ProviderController@addprovider')->name('addprovider');
		Route::post('/Dprovider/{id}','Admin\ProviderController@Dprovider')->name('Dprovider')->where('id', '[0-9]+');
		Route::post('/Fprovider/{id}','Admin\ProviderController@Fprovider')->name('Fprovider')->where('id', '[0-9]+');
		Route::post('/Eprovider/{id}','Admin\ProviderController@Eprovider')->name('Eprovider')->where('id', '[0-9]+');

		//nhập hàng
		Route::get('/import','Admin\ImportController@addform')->name('import');
		Route::get('/listimport','Admin\ImportController@index')->name('listimport');
		Route::post('/addimport','Admin\ImportController@addimport')->name('addimport');
		Route::post('/changestatus/{id}/{status}','Admin\ImportController@Changestatus')->name('Changestatus')->where('id', '[0-9]+');
		Route::get('/importdetail/{id}','Admin\ImportController@importdetail')->name('importdetail')->where('id', '[0-9]+');

		//Đơn hàng
		Route::get('/order','Admin\OrderController@index')->name('Alistorder');
		Route::post('/order/changestatus/{id}/{status}','Admin\OrderController@changestatus')->name('Changestatus');
		Route::get('/order/orderdetail/{id}','Admin\OrderController@orderdetail')->name('Aorderdetail')->where('id', '[0-9]+');
	});
});
