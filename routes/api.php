<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/home', 'Api\HomeController@home');
Route::post('/loadmore', 'Api\HomeController@loadmore');
Route::post('/prddetail/{id}', 'Api\ProductController@productDetail');
// Route::post('/prdlike', 'Index\HomeController@loadmore');

Route::group(
    [
        'middleware' => 'api.auth',
        'prefix' => 'auth',
    ], function () {
        Route::post('me', 'Api\AuthController@me');
        Route::post('logout', 'Api\AuthController@logout');
        Route::post('like', 'Api\EnjoyController@like');
        Route::post('unlike', 'Api\EnjoyController@unLike');
});
Route::post('login', 'Api\AuthController@login');
