$(document).ready(function(){
	$('.dataTables-example').DataTable({
		pageLength: 25,
		responsive: true,
		dom: '<"html5buttons"B>lTfgitp',
		buttons: [
		{extend: 'excel', title: 'ExampleFile'},
		{extend: 'pdf', title: 'ExampleFile'},

		{extend: 'print',
		customize: function (win){
			$(win.document.body).addClass('white-bg');
			$(win.document.body).css('font-size', '10px');

			$(win.document.body).find('table')
			.addClass('compact')
			.css('font-size', 'inherit');
		}
	}
	]
});

	// upload image avatar tác giả
	$('.avatar').on('click', function(){
		$('.fileupload:first').trigger('click');
	});
	$(".fileupload:first").change(function () {
		if (this.files && this.files[0])
		{
			var reader = new FileReader();
			reader.onload = imageIsL;
			reader.readAsDataURL(this.files[0]);
		}
	});
	function imageIsL(e) {
		$('.avatar').attr('src', e.target.result);
	};

	// Xoá tác giả
	$('.deleteAuthor').on('click',function(e){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		e.preventDefault();
		let id = $(this).val();
		$.ajax({
			url:'/boss/Dauthors/' + id,
			type:'POST',
			data:{
				"id": id
			}
		}).done(function(result){
			if (result) {
				$('.close_modal').click();
				$('#'+id+'_data').slideUp();
				toastr.success('','Xoá thành công!');
			}
			else{
				toastr.warning('','Đã sảy ra lỗi!');
			}
		});
	});

	// Sửa danh mục con
	$(document).on('click','button[name=editSubCate]',function(e){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: $(this).val(),
			type: 'post',
			dataType : 'json',
			success: function(arr_document) {
    				// console.log(arr_document);
    				$('#tensub').val(arr_document['subcategory_name']);
    				$('#formModal').attr('action',arr_document['action']);
    				$('select[name=category_id]').val(arr_document['category_id']);
    			}
    		});
	});

	// Sửa danh mục
	$(document).on('click','button[name=editCate]',function(e){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: $(this).val(),
			type: 'post',
			dataType : 'json',
			success: function(arr_document) {
				$('#tencate').val(arr_document['category_name']);
				$('#formParentModal').attr('action',arr_document['action']);
			}
		});
	});

	//sửa nhà cung cấp
	$(document).on('click','button[name=editPRV]',function(e){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: $(this).val(),
			type: 'post',
			dataType : 'json',
			success: function(arr_document) {
				$('#ncc').val(arr_document['provider_name']);
				$('#sdt').val(arr_document['provider_phone']);
				$('#email').val(arr_document['provider_email']);
				$('#address').val(arr_document['provider_address']);
				$('#formModal').attr('action',arr_document['action']);
			}
		});
	});

	//Thêm đơn nhập
	$(document).on('click','button[name=addimport]', function(e){
		e.preventDefault();
		$('#addimport').show();
		let stt = $('tbody tr').length + 1;
		let html = '';
		let point = /^([0-9\.0-9])+$/;


		html += '<tr>';
		html += '<td class="text-center">'+stt+'</td>';
		$('#prd').children().addClass('select2');
		html += '<td class="text-center">'+$('#prd').html()+'</td>';
		$('#prd').children().removeClass('select2');
		html += '<td class="text-center">';
		html += '<input type="text" name="quatity[]" id="quatity_'+stt+'" class="form-control" placeholder="Số lượng" value="" required="">';
		html += '</td>';
		html += '<td class="text-center">';
		html += '<input type="text" name="price[]" id="price'+stt+'" class="form-control" placeholder="Giá nhập" value="" required="">';
		html += '</td>';
		html += '<td class="text-center"><strong id="TT_'+stt+'"></strong></td>';
		html += '<td class="text-center"><button type="button" class="btn btn-success btn-xs pull-right" name="delLine2" title="Xoá dòng"><i class="fa fa-minus-square" aria-hidden="true"></i></button></td>';
		html += '</tr>';
		if ($('tbody tr:last').length == 0){
			$('tbody').html(html);
		}
		else{
			$('tbody tr:last').after(html);
		}

		$(".select2").select2({
            width:"100%",
            placeholder: "Chọn sản phẩm",
            allowClear: true
        });
	});

	$(document).on('change','input[name="quatity[]"], input[name="price[]"]',function(){
		let point = /^([0-9\.0-9])+$/;
		let stt = $('tbody tr').length;
		if (!point.test($(this).val())){
			alert("Giá nhập hoặc số lượng không đúng định dạng!");
			$('#addimport').hide();
		}
		else{
			if ($('#quatity_'+stt).val() != "" && $('#price'+stt).val() != "") {
				$('#TT_'+stt).html($('#quatity_'+stt).val() * $('#price'+stt).val());
				$('#TT_'+stt).each(function(index){
					let money = $(this).html();
					$(this).html(money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+ ' ₫');
				});
			}
			$('#addimport').show();
		}
	});

	$(document).on('click','button[name=delLine2]', function(){
		$(this).parent().parent().remove();
		if($('tbody tr').length == 0)
			$('#addimport').hide();
	});


	// Thêm nxb
	$(document).on('click','button[name=addCom]', function(e){
		e.preventDefault();
		$('button[type=submit]').removeAttr('disabled').css('cursor','pointer');
		let html = '';
		let stt = $('tbody tr').length + 1;
		let point = /^([0-9\.0-9])+$/;


		html += '<tr>';
		html += '<td class="text-center">'+stt+'</td>';
		html += '<td class="text-center">';
		html += '<input type="text" name="company_name[]" class="form-control" placeholder="Tên nhà xuất bản" value="" autofocus="" required="">';
		html += '</td>';
		html += '<td class="text-center"><button type="button" class="btn btn-success btn-xs pull-right" name="delLine" title="Xoá dòng"><i class="fa fa-minus-square" aria-hidden="true"></i></button></td>';
		html += '</tr>';
		if ($('tbody tr:last').length == 0){
			$('tbody').html(html);
		}
		else{
			$('tbody tr:last').after(html);
		}
	});

	$(document).on('click','button[name=delLine]', function(){
		$(this).parent().parent().remove();
		$('button[type=submit]').attr('disabled','true').css('cursor','not-allowed');
	});

	// Sửa nxb
	$(document).on('click','button[name=editCom]',function(e){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: $(this).val(),
			type: 'post',
			dataType : 'json',
			success: function(arr_document) {
				$('#tennxb').val(arr_document['company_name']);
				$('#formModal').attr('action',arr_document['action']);
			}
		});
	});

	// Sửa khuyến mãi
	$(document).on('click','button[name=editProm]',function(e){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: $(this).val(),
			type: 'post',
			dataType : 'json',
			success: function(arr_document) {
				$('#tenkm').val(arr_document['promotion_name']);
				$('#phantramkm').val(arr_document['promotion_percent']);
				$('#hoadonkm').val(arr_document['promotion_price']);
				$('#maxkm').val(arr_document['promotion_max']);
				$('#hethankm').val(arr_document['date_end']);
				$('#batdaukm').val(arr_document['date_start']);
				$('#formModal').attr('action',arr_document['action']);
			}
		});
	});

	// block khuyến mãi
	$(document).on('click','button[name=block]',function(){
		let i = $(this).data('id');
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: $(this).val(),
			type: 'post',
			success: function(arr_document) {
				$('button[data-id='+i+']').children('i').remove();
				if(arr_document['status'] == 'apply'){
					$('button[data-id='+i+']').append("<i class='fa fa-lock' aria-hidden='true'></i>");
					$('span[data-id='+i+']').removeClass('label-danger ').addClass('label-info ').html('Đang áp dụng');
				}
				else{
					$('button[data-id='+i+']').append("<i class='fa fa-unlock' aria-hidden='true'></i>");
					$('span[data-id='+i+']').removeClass('label-info').addClass('label-danger ').html('Tạm ngừng');
				}
			}
		});
	});

	// block ảnh sản phẩm
	$(document).on('click','button[name=blockimg]',function(){
		let i = $(this).data('id');
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: $(this).val(),
			type: 'post',
			success: function(arr_document) {
				$('button[data-id='+i+']').children('i').remove();
				if(arr_document['status'] == 'show'){
					$('button[data-id='+i+']').append("<i class='fa fa-lock' aria-hidden='true'></i>");
					$('span[data-id='+i+']').removeClass('label-danger ').addClass('label-success ').html('Đang hiển thị');
				}
				else{
					$('button[data-id='+i+']').append("<i class='fa fa-unlock' aria-hidden='true'></i>");
					$('span[data-id='+i+']').removeClass('label-info').addClass('label-danger ').html('Tạm ẩn');
				}
			}
		});
	});

	//sửa sản phẩm
	$(document).on('click','button[name=editImg]',function(e){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: $(this).val(),
			type: 'post',
			dataType : 'json',
			success: function(arr_document) {
				let imgPath = $('#productImg').attr('src');
				$('#productImg').attr('src',imgPath+'/'+arr_document['thumbnail_image']);
				$('#changeImgForm').attr('action',arr_document['action']);
			}
		});
	});

});