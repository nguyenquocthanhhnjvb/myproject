
$(window).on('load',function() {
	$('body').removeClass('preloading');
	$('#preload').delay(100).fadeOut('fast');
});
new WOW().init();
$(window).scroll(function() {
	if ($(window).scrollTop() > 300) {
		$('.backtotop').show();
	} else {
		$('.backtotop').hide();
	}
});

$(document).ready(function(){
	$('.avatar').on('click', function(){
		$('.fileupload:first').trigger('click');
	});
	$(".fileupload:first").change(function () {
		if (this.files && this.files[0])
		{
			var reader = new FileReader();
			reader.onload = imageIsL;
			reader.readAsDataURL(this.files[0]);
		}
	});
	function imageIsL(e) {
		$('.avatar').attr('src', e.target.result);
	};

	$("#search-input").click(function(e) {
		e.stopPropagation();
		if ($(this).val().trim() == "") 
			$('#search-block').hide();
		else
			$('#search-block').show();
	});

	$('body').click(function(){
		if ($('#search-block').css('display') == 'block') {
    		$('#search-block').hide();
		}
	});

	$(document).on('keyup','#search-input',function(){
		$('#search-block').show();
		let searchText = $(this).val();

		if(searchText.trim() != ""){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url: '/search',
				type: 'post',
				dataType : 'json',
				data:{
					"searchText": searchText
				},
				success: function(arr_document) {
					let html = "";
	    				if (arr_document.length > 0) {
	    					for (let i = 0; i < arr_document.length; i++) {
	    						html +='<li><a class="list-group-item pt-2 pb-2" href="/productinfo/'+arr_document[i].url_product+'/'+arr_document[i].id+'">'+ arr_document[i].product_name +'</a></li>';
	    					}
	    					$('#search-block ul').html(html);
	    				}
	    			}
    		});
		}
		else {
			$('#search-block ul').html("");
			$('#search-block').hide();
		}
	});
});