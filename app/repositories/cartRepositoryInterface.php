<?php
namespace App\repositories;

interface cartRepositoryInterface
{
	public function getAllBuyNow($userId);

	public function findPrdInCart($userId, $prdId);

}