<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\categoryRepositoryInterface;
class categoryRepository extends eloquentRepository implements categoryRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
    	return \App\Models\categoryModel::class;
    }
    public function getlist_sub_cate()
    {
    	$result = $this->_model->with('subcategorys')->get()->toArray();
        // $list = array();
        // foreach ($result as $key => $value) {
        //     array_push($list,$value);
        // }
        return $result;
        // print_r($result);
    }
}