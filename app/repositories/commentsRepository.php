<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\commentsRepositoryInterface;
class commentsRepository extends eloquentRepository implements commentsRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
    	return \App\Models\commentsModel::class;
    }
}