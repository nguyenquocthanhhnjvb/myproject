<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\orderRepositoryInterface;
use App\Models\orderModel;
use DB;

class orderRepository extends eloquentRepository implements orderRepositoryInterface
{
	public function getModel()
	{
		return orderModel::class;
	}

	public function getListOrder($userId,$paginate){

		$orders = $this->_model->where('user_id',$userId)->orderBy('created_at', 'desc')->paginate($paginate)->withPath('?href=order');
		if($orders->isNotEmpty()){
			foreach ($orders as $key => $order) {

				$total = 0;

				$orders[$key]['detail'] = DB::table('detail_order')
					->select('detail_order.*','products.product_name','products.url_product')
					->join('products','detail_order.product_id','=','products.id')
					->where('detail_order.order_id',$order->id)->get();

				foreach ($order->detail as $detailKey => $prd) {
                    $total += ( $prd->quatity * $prd->price );
                }
                $orders[$key]['total'] = $total;
			}
		}
		return $orders;
	}

	public function getAllOrder($paginate){
		$orders = $this->_model->orderBy('created_at', 'desc')->paginate($paginate);
		if($orders->isNotEmpty()){
			foreach ($orders as $key => $order) {

				$total = 0;

				$orders[$key]['detail'] = DB::table('detail_order')
					->select('detail_order.*','products.product_name','products.url_product',)
					->join('products','detail_order.product_id','=','products.id')
					->where('detail_order.order_id',$order->id)->get();

				foreach ($order->detail as $detailKey => $prd) {
                    $total += ( $prd->quatity * $prd->price );
                }
                $orders[$key]['total'] = $total;
			}
		}
		return $orders;
	}

	public function orderdetail($id){

		$orders = $this->_model->where('id',$id)->get();
		if($orders->isNotEmpty()){
			foreach ($orders as $key => $order) {

				$orders[$key]['detail'] = DB::table('detail_order')
					->select('detail_order.*','products.product_name','products.url_product','product_images.thumbnail_image')
					->join('products','detail_order.product_id','=','products.id')
					->join('product_images','products.id','=','product_images.product_id')
					->where('product_images.status','avatar')
					->where('detail_order.order_id',$order->id)->get();
			}
		}
		return $orders->first();
	}	
}