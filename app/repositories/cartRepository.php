<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\cartRepositoryInterface;
use App\Models\cartModel;
use App\myfunction\seoname;
use App\Models\productsModel;
use App\Models\commentsModel;
class cartRepository extends eloquentRepository implements cartRepositoryInterface
{
    use seoname;

    public function getModel()
    {
    	return cartModel::class;
    }

    public function findPrdInCart($userId, $prdId){
        $data = $this->_model->where('user_id', $userId)->where('product_id', $prdId)->get();
        if ($data->isNotEmpty())
            return $data[0];
        else
            return false;
    }

    public function getAllBuyNow($userId){
    	$result = $this->_model->where('user_id', $userId)->with('product')->get();

        if($result->isNotEmpty()){
            foreach ($result as $key => $value) {

                //tạo trường price lấy từ mảng with(product) vào $result
                $value['price'] = $result[$key]['product']['price'];
                 //tạo trường thumbnail_image lấy ảnh avt từ bảng ảnh vào $result   
                $value['thumbnail_image'] = $this->getAvatar($value->product_id);

            }
        }

        // dd($result);
        return $result;
    }

    public function total($data){
        if(!is_array($data)) return "0";
        $total = 0;
        foreach ($data as $key => $value) {
            $total = ($value['price'] * $value['quatity']) + $total;
        }
        return $total;
    }
}