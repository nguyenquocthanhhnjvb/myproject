<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\subcategoryRepositoryInterface;
class subcategoryRepository extends eloquentRepository implements subcategoryRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return \App\Models\SubcategoryModel::class;
    }
}