<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\UserRepositoryInterface;
class UserRepository extends eloquentRepository implements UserRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return \App\User::class;
    }
}