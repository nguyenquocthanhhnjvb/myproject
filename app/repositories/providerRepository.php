<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\providerRepositoryInterface;
use App\Models\providerModel;
class providerRepository extends eloquentRepository implements providerRepositoryInterface
{

    public function getModel()
    {
    	return providerModel::class;
    }
}