<?php
namespace App\repositories;

interface productRepositoryInterface
{
	public function getProductWithInfo();

	public function getProductDetal($id);

	public function getProductWithInfo_random($limit);

	public function getProductOffet($offset,$limit);

	public function productDetail($id);
}