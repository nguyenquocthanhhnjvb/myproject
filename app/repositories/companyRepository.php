<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\companyRepositoryInterface;
use App\Models\companyModel;
class companyRepository extends eloquentRepository implements companyRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
    	return companyModel::class;
    }
    public function insert(array $attributes){
    	return companyModel::insert($attributes);
    }
}