<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\personRepositoryInterface;
use App\Models\personModel;

class personRepository extends eloquentRepository implements personRepositoryInterface
{
	public function getModel()
	{
		return personModel::class;
	}

	public function findInfoByUser_id($userId){
		return $this->_model->where('user_id',$userId)->get();
	}
}