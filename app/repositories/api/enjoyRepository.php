<?php
namespace App\repositories\api;

use App\repositories\contacts\eloquentRepository;
use App\repositories\api\enjoyRepositoryInterface;

class enjoyRepository extends eloquentRepository implements enjoyRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
    	return \App\Models\enjoy::class;
    }
    public function unLike($prdId,$userId){
        $this->_model->where('user_id', $userId)
                    ->where('product_id', $prdId)
                    ->delete();
    }

    public function listProductLike($userId){
        return $this->_model->where('user_id',$userId)->get('product_id');
    }

    public function listProductLike_byUser($userId,$id){
        return $this->_model->where('user_id',$userId)
                            ->where('product_id',$id)
                            ->get();
    }
    
}