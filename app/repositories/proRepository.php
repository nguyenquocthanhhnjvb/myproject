<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\proRepositoryInterface;
use App\Models\PromotionModel;
class proRepository extends eloquentRepository implements proRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
    	return PromotionModel::class;
    }
    public function getAllOrderByDESC(){
    	return PromotionModel::orderBy('status', 'DESC')->orderBy('date_end', 'DESC')->get();
    }
}