<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\importRepositoryInterface;
use App\Models\importModel;
class importRepository extends eloquentRepository implements importRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
    	return importModel::class;
    }
    public function getInfo(){

    	$imports = $this->_model->with('provider')->orderBy('created_at', 'desc')->get();
    	if($imports->isNotEmpty()){
    		foreach ($imports as $key => $import) {

                $total = 0;

    			$imports[$key]['detail'] = $this->_model
    			->select('products.product_name','detail_import.*')
    			->join('detail_import','import.id','=','detail_import.import_id')
    			->join('products','detail_import.product_id','=','products.id')
    			->where('import.id',$import->id)->get();
                
                foreach ($import->detail as $detailKey => $prd) {
                    $total += ( $prd->quatity * $prd->price );
                }
                $imports[$key]['total'] = $total;
    		}

    	}
    	return $imports;
    }

    public function getInfoOne($id){
        $import = $this->_model->with('provider')->where('id',$id)->get();
        if($import->isNotEmpty()){

            $import[0]['detail'] = $this->_model
                    ->select('products.product_name','detail_import.*')
                    ->join('detail_import','import.id','=','detail_import.import_id')
                    ->join('products','detail_import.product_id','=','products.id')
                    ->where('import.id',$id)->get();
        }
        return $import->first();
    }
}
