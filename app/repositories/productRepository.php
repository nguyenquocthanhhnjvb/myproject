<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\productRepositoryInterface;
use App\Models\productsModel;
class productRepository extends eloquentRepository implements productRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
    	return productsModel::class;
    }

    public function getProductWithInfo()
    {
    	return $this->_model->with('author','company','product_images','subcategorys')->get()->toArray();
    }

    public function getProductDetal($id){
        return $this->_model->where('id',$id)->with('author','company','product_images','subcategorys','comments')->get()->toArray();
    }

    public function getProductWithInfo_random($limit){
        return $this->_model->inRandomOrder()
                        ->limit($limit)
                        ->with('author','company','product_images','subcategorys','comments')->get()->toArray();
    }

    public function getProductByCategory($id,$limit){
        return $this->_model->where('subcategory_id',$id)->with('comments')->inRandomOrder()->limit($limit)->with('product_images')->get()->toArray();
    }

    public function getPrdPaginate($where,$id){
        if($where == 'Nowhere'){
            return $this->_model->with('product_images')->paginate(10);
        }
        else{   
            return $this->_model->with('product_images')->where($where,$id)->paginate(10);
        }
    }

    public function search($searchText){
        return $this->_model->select('products.id','products.product_name','url_product')
            ->where('products.product_name', 'like', '%' .$searchText. '%')->get();
    }

    public function getProductOffet($offset,$limit){
        return $this->_model->offset($offset)->limit($limit)
                    ->with('author','company','product_images','subcategorys','comments')->get();
    }

    public function productDetail($id){
        return $this->_model->where('id',$id)->with('product_images','author','company','subcategorys')->get();
    }
    
}