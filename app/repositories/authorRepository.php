<?php
namespace App\repositories;

use App\repositories\contacts\eloquentRepository;
use App\repositories\authorRepositoryInterface;
class authorRepository extends eloquentRepository implements authorRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
    	return \App\Models\authorsModel::class;
    }

    public function getAuthorInfo_random($limit){
    	$result = $this->_model->inRandomOrder()->limit($limit)->get()->toArray();
    	return $result;
    }
}