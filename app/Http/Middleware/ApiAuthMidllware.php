<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuthMidllware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         $user = auth()->user();
            if (!$user) {
                return response()->json([
                    'status'  => false,
                    'message' => 'Unauthorized',
                ]);
            }
            return $next($request);
    }
}
