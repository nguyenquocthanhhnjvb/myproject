<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class CheckLogin
{

    public function handle($request, Closure $next , $role)
    {

        if (Auth::check()) {
            if (Auth::user()->role != 'admin') {
                return redirect('/');
            }
        }else return redirect('/');

        return $next($request);
        
    }
}
