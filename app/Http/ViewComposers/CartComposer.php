<?php

namespace App\Http\ViewComposers;
use Illuminate\View\View;
use App\service\cartService;
use Cart;
class CartComposer 
{
	
	protected $getAllBuyNow = [];

	public function __construct(cartService $cartService){
		if (auth()->check()) {
			$this->getAllBuyNow = $cartService->getAllBuyNow(auth()->user()->id);
		}
		// dd($this->getAllBuyNow);
	}



	public function compose(View $view){
		if (auth()->check()) {
			$view->with('getAllBuyNow', $this->getAllBuyNow);
		}else{
			$view->with('getAllBuyNow', Cart::content());	
		}
	}
}