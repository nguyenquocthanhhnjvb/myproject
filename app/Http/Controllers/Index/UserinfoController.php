<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\order;
use App\service\personService;
use App\service\orderService;

class UserinfoController extends Controller
{


	protected $personService;
	protected $orderService;

	public function __construct(
		personService $personService,
		orderService $orderService ){

		$this->orderService = $orderService;
		$this->personService = $personService;
	}

	public function index(Request $request)
	{
		if (!auth()->check() || auth()->user()->role != 'customer') {
			return redirect()->route('home')->send();
		}

		$userId = auth()->user()->id;
		$page = $request->query('href',"order");

		switch ($page) {
			// case 'person':
			// $person = $this->personService->findInfoByUser_id($userId);
			// return view('frontend.person',compact('person'));
			// break;
			case 'order':
			$orders = $this->orderService->getListOrder($userId,10);
				// dd($orders);
			return view('frontend.orderinfo',compact('orders'));
			break;
			case 'chat':
			return "chat";
			break;
			case 'cmt':
			return "cmt";
			break;
			default:
			return view('frontend.person');
			break;
		}
	}

	public function person(order $Request){
		try{
			$this->personService->addInfoPerson($Request);
			$notification = array(
				'message'   => 'Cập nhật thành công thành công!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function cancel($id){
		try{
			$this->orderService->cancel($id);
			$notification = array(
				'message'   => 'Đã huỷ đơn!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function orderdetail($id){
		$role = auth()->user()->role;

		if (!auth()->check() || $role != 'customer') {
			return redirect('/');
		}

		$orderdetail = $this->orderService->orderdetail($id);
		if(empty($orderdetail))
			abort(404);
		return view('frontend.orderdetail',compact('orderdetail'));
	}
}
