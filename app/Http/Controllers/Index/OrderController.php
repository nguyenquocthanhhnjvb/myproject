<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\service\orderService;
use App\Http\Requests\Admin\order;
use DB;
class OrderController extends Controller
{

	protected $orderService;

	public function __construct(orderService $orderService ){

		$this->orderService = $orderService;

	}

	public function index(Request $request){
		if ($request->query('vnp_SecureHash')){
			$this->returnVNPay($request);
		}
		if (!auth()->check() || auth()->user()->role != 'customer') {
			return redirect()->route('home')->send();
		}
		return view('frontend.checkout');
	}

	public function addOrder(order $Request){

		$checkout = $Request->checkout;
		switch ($checkout) {
			case 'offline':
			$this->checkoutOf($Request); 
			break;
			
			case 'online':
			$this->checkoutOn($Request); 
			break;
		}
	}

	public function checkoutOf($Request){
		try{
			$notsuccess = $this->orderService->checkoutOf($Request);

			if ($notsuccess == 0) {
				$notification = array(
					'message'   => 'Xác nhận đơn hàng thành công!',
					'type'      => 'success',
				);
				return redirect()->route('userinfo')->with($notification)->send();;
			}
			else{
				$notification = array(
					'message'   => 'Có '.$notsuccess.' sản phẩm chưa được xác nhận vì hết hàng!',
					'type'      => 'warning',
				);
				return redirect()->route('cart')->with($notification)->send();;
			}

		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
			return back()->with($notification);
		}
	}

	public function checkoutOn($request){
		$result = $this->orderService->checkoutOn_getID($request);
		// dd($result);
		$orderID = 0;
		if($result){
			$orderID = $result;
		}
		// session(['cost_id' => $request->id]);
		session(['url_prev' => url()->previous()]);
        $vnp_TmnCode = "52VNV6C9"; //Mã website tại VNPAY 
        $vnp_HashSecret = "BWDSUFIAPSNHMEMRVCMULVLFKJMMSMDP"; //Chuỗi bí mật
        $vnp_Url = "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
        $vnp_Returnurl = "http://thekite.local/checkout";
        $vnp_TxnRef = $orderID; //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = "Thanh toan hoa don.";
        $vnp_OrderType = 'billpayment';
        $vnp_Amount = $request->total * 100;
        $vnp_Locale = 'vn';
        $vnp_BankCode = 'NCB';
        $vnp_IpAddr = request()->ip();

        $inputData = array(
        	"vnp_Version" => "2.0.0",
        	"vnp_TmnCode" => $vnp_TmnCode,
        	"vnp_Amount" => $vnp_Amount,
        	"vnp_Command" => "pay",
        	"vnp_CreateDate" => date('YmdHis'),
        	"vnp_CurrCode" => "VND",
        	"vnp_IpAddr" => $vnp_IpAddr,
        	"vnp_Locale" => $vnp_Locale,
        	"vnp_OrderInfo" => $vnp_OrderInfo,
        	"vnp_OrderType" => $vnp_OrderType,
        	"vnp_ReturnUrl" => $vnp_Returnurl,
        	"vnp_TxnRef" => $vnp_TxnRef,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
        	$inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
        	if ($i == 1) {
        		$hashdata .= '&' . $key . "=" . $value;
        	} else {
        		$hashdata .= $key . "=" . $value;
        		$i = 1;
        	}
        	$query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
           // $vnpSecureHash = md5($vnp_HashSecret . $hashdata);
        	$vnpSecureHash = hash('sha256', $vnp_HashSecret . $hashdata);
        	$vnp_Url .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;
        }
        // dd($vnp_Url);
        return redirect($vnp_Url)->send();
    }

    public function returnVNPay($request){


    	$vnp_SecureHash = $request->query('vnp_SecureHash');
		$vnp_HashSecret = "BWDSUFIAPSNHMEMRVCMULVLFKJMMSMDP"; //Chuỗi bí mật
		$dataReturn = $request->all();
		$inputData = array();
		foreach ($dataReturn as $key => $value) {
			if (substr($key, 0, 4) == "vnp_") {
				$inputData[$key] = $value;
			}
		}
		unset($inputData['vnp_SecureHashType']);
		unset($inputData['vnp_SecureHash']);
		ksort($inputData);
		$i = 0;
		$hashData = "";
		foreach ($inputData as $key => $value) {
			if ($i == 1) {
				$hashData = $hashData . '&' . $key . "=" . $value;
			} else {
				$hashData = $hashData . $key . "=" . $value;
				$i = 1;
			}
		}
		$secureHash = hash('sha256',$vnp_HashSecret . $hashData);
		if ($secureHash == $vnp_SecureHash) {
			if ($request->query('vnp_ResponseCode') == '00') {
				$maDonHang = $inputData['vnp_TxnRef'];
				$updateData = array(
					'checkout' => 'active'
				);
				$this->orderService->update($maDonHang, $updateData);
				$notsuccess = DB::table('cart')->count();

				if ($notsuccess == 0) {
					$notification = array(
						'message'   => 'Xác nhận đơn hàng thành công!',
						'type'      => 'success',
					);
					return redirect()->route('userinfo')->with($notification)->send();
				}
				else{
					$notification = array(
						'message'   => 'Có '.$notsuccess.' sản phẩm chưa được xác nhận vì hết hàng!',
						'type'      => 'warning',
					);
					return redirect()->route('userinfo')->with($notification)->send();
				}
			}
			else {
				$notification = array(
					'message'   => 'Giao dịch không thành công!',
					'type'      => 'warning',
				);
				return redirect()->route('cart')->with($notification)->send();
			}
		}
		else {
			$notification = array(
				'message'   => 'Chữ ký không hợp lệ. Vui lòng thử lại!',
				'type'      => 'warning',
			);
			return redirect()->route('cart')->with($notification)->send();
		}
	}
}
