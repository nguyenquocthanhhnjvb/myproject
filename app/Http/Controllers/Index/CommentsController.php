<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\comments;
use App\service\commentsService;
class CommentsController extends Controller
{
	protected $commentsService;

	public function __construct(commentsService $commentsService){
		$this->commentsService = $commentsService;
	}

	public function addComments(comments $Request,$userid,$prdid){
		if(auth()->check()){
			
			try{
				$this->commentsService->addComments($Request,$userid,$prdid);
				$notification = array(
					'message'   => 'Gửi nhận xét thành công!',
					'type'      => 'success',
				);
			} catch(\Exception $e){
				$notification = array(
					'message'   => 'Đã xảy ra lỗi!',
					'type'      => 'warning',
					'error'     => $e->getMessage(),
				);
			}
			return back()->with($notification);
		}
		else{
			$notification = array(
				'message'   => 'Bạn phải đăng nhập để xử dụng tính năng này!',
				'type'      => 'warning',
			);
			return back()->with($notification);
		}
	}
}
