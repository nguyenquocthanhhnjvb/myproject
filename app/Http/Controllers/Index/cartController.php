<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\service\cartService;
use App\Http\Requests\Admin\login;
use App\Models\productsModel;
class CartController extends Controller
{


	protected $cartService;

	public function __construct(cartService $cartService){
		$this->cartService = $cartService;
	}

	public function index(){

		$PrdInCartNoLogin = $this->cartService->PrdInCartNoLogin();
		// dd($PrdInCartNoLogin);
		return view('frontend.cart',compact('PrdInCartNoLogin'));
	}

	public function addPrd(Request $request){


		$id = $request->id;

		$product = productsModel::find($id);

		if(!$product)
			abort(404);
		else
			return $this->cartService->addPrd($request,$product);
	}

	public function UcartQuatity(Request $request){
		return $this->cartService->UcartQuatity($request);
	}

	public function DcartPrd(Request $request){
		return $this->cartService->DcartPrd($request);
	}

	public function loginModal(login $Request){
		return $this->cartService->loginModal($Request);
	}
}
