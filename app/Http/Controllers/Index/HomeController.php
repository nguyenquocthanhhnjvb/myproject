<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\service\FHomeService;
use Auth;
class HomeController extends Controller
{

	protected $FHomeService;
	public function __construct(FHomeService $FHomeService){
		$this->FHomeService = $FHomeService;
	}
	public function index(){
		$data = [
			'product' 	=> $this->getProductWithInfo_random(),
			'author'	=> $this->getAuthorInfo_random(),
		];
		return view('frontend.home',compact('data'));
	}

	public function home(){
		$data = [
			'products' 	=> $this->FHomeService->getProductOffet(0,10),
			'authors'	=> $this->getAuthorInfo_random(),
		];
		return response()->json($data);
	}

	public function loadmore(Request $Request){
		$offset = $Request->offset;
		$limit = $Request->limit;
		
		$data =  $this->FHomeService->getProductOffet($offset,$limit);
		return response()->json($data);
	}

	public function productInfo($name,$id){
		$productinfo = $this->FHomeService->FindProductByID($id);
		// dd($productinfo);
		return view('frontend.productinfo',compact('productinfo'));
	}

	public function productlist(Request $Request){
		$where 	= $Request->where;
		$id 	= $Request->id;
		$key 	= $Request->key;
		$name 	= $Request->name;
		
		$data = $this->FHomeService->getNavBarInfo($where,$id,$name,$key);
		// dd($data);
		return view('frontend.listproduct',compact('data'));
	}

	public function getAuthorInfo_random(){
		return $this->FHomeService->getAuthorInfo_random(6);
	}

	public function getProductWithInfo_random(){
		return $this->FHomeService->getProductWithInfo_random(10);
	}

	public function search(Request $Request){
		return $this->FHomeService->search($Request);
	}
}
