<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\service\FHomeService;

class HomeController extends Controller
{

    protected $FHomeService;
	public function __construct(FHomeService $FHomeService){
		$this->FHomeService = $FHomeService;
	}

    public function home(){
		$data = [
			'products' 	=> $this->FHomeService->getProductOffet(0,10),
			'authors'	=> $this->FHomeService->getAuthorInfo_random(6),
		];
		return response()->json($data);
    }
    
    // public function getAuthorInfo_random(){
    // 	$result = authorsModel::inRandomOrder()->limit(6)->get()->toArray();
    // 	return $result;
    // }

    public function loadmore(Request $Request){
		$offset = $Request->offset;
		$limit = $Request->limit;
		
		$data =  $this->FHomeService->getProductOffet($offset,$limit);
		return response()->json($data);
	}
}
