<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\service\api\productDetailService;
use App\service\api\enjoyService;

class ProductController extends Controller
{

    protected $productDetail;
    protected $enjoyService;
    public function __construct(productDetailService $productDetail,enjoyService $enjoyService){
        $this->productDetail = $productDetail;
        $this->enjoyService = $enjoyService;
	}

    public function productDetail($id){

        $result = $this->productDetail->productDetail($id);
        
        $result[0]['like'] = false;
        if(auth()->user())
        {
            $userId = auth()->user()->id;

            $listProductLike = $this->enjoyService->listProductLike_byUser($userId,$id);

            if($listProductLike->isNotEmpty() && $result[0]->id == $listProductLike[0]['product_id']){
                $result[0]['like'] = true;
            }
        }
        foreach($result[0]->product_images as $key => $image){
                if($image['status'] == 'avatar'){
                        $result[0]['thumbnailImage'] = $image['thumbnail_image'];
                }
            }
            return response()->json($result[0]);
    }   
}
