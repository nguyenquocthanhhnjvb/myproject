<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\enjoy;
use App\service\api\enjoyService;


class EnjoyController extends Controller
{

    protected $enjoyService;

	public function __construct(enjoyService $enjoyService){
		$this->enjoyService = $enjoyService;
	}


    public function like(Request $request){
        return $this->enjoyService->like($request);
    }

    public function unLike(Request $request){
        return $this->enjoyService->unLike($request);
    }
}
