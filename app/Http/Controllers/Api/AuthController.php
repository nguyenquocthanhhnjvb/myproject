<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{


    public function login(Request $request){
         $token = Auth::attempt($request->only('name','password'));
         if (!$token) {
           return response()->json([
                'message' =>  'Unauthenticated',
                'status' => false
           ]);
         }

         return response()->json([
                'access_token' =>  $token,
                'user' => [
                    'name' =>auth()->user()->name,
                    'email' => auth()->user()->email,
                ],
                'status' => true
         ]);
    }
    public function me()
    {
        return response()->json(auth()->user());
    }
    public function logout(){
        auth()->logout();
    }
    // protected function respondWithToken($token)
    // {
    //     return response()->json([
    //         'access_token' => $token,
    //         'token_type' => 'bearer',
    //         // 'expires_in' => auth()->factory()->getTTL() - 59
    //     ]);
    // }
}
