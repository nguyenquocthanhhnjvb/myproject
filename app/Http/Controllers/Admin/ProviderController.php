<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\provider;
use App\service\providerService;
class ProviderController extends Controller
{
	protected $providerService;

	public function __construct(providerService $providerService){
		$this->providerService = $providerService;
	}

	public function index(){
		$providers = $this->providerService->getAll();
		// dd($providers);
		return view('backend.listprovider',compact('providers'));
	}

	public function addform(){
		return view('backend.addprovider');
	}

	public function addprovider(provider $Request){


		try{
			$this->providerService->addprovider($Request);
			$notification = array(
				'message'   => 'Thêm thành công!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function Dprovider($id){
		try{
			$this->providerService->Dprovider($id);
			$notification = array(
				'message'   => 'Xoá thành công!',
				'type'      => 'success',
			);
			
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function Fprovider($id){
		return $this->providerService->Fprovider($id);
	}

	public function Eprovider(provider $Request,$id){
		try {
			$this->providerService->Eprovider($id,$Request);
			$notification = array(
				'message' 	=> 'Sửa thành công!',
				'type' 		=> 'success',
			);
		} catch (\Exception $e) {
			$notification = array(
				'message' 	=> 'Đã xảy ra lỗi!',
				'type' 		=> 'warning',
				'error'		=> $e->getMessage(),
			);
		}
		return back()->with($notification);
	}
}
