<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\service\productService;
use App\Http\Requests\Admin\products;
class ProductsController extends Controller
{
	protected $productService;
	public function __construct(productService $productService){
		$this->productService = $productService;
	}

	public function index(){
		$Products = $this->productService->getAll();	
		return view('backend.ListProduct',compact('Products'));
	}

	public function formAddProduct(){
		$data = $this->productService->get_Cate_Author_Company();
		return view('backend.AddProducts',compact('data'));
	}

	public function DeleteProduct($id){
		try{
			$this->productService->DeleteProduct($id);
			$notification = array(
				'message'   => 'Xoá thành công!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function ProductDetal($id){
		$Cate_Author_Company = $this->productService->get_Cate_Author_Company();
		$result = $this->productService->getProductDetal($id);
		$ProductDetal = $result[0];
		$ProductDetal['C_A_C'] = $Cate_Author_Company;
		if(empty($ProductDetal))
			return abort(404);
		return view('backend.ProductDetal',compact('ProductDetal'));
	}

	public  function AddProduct(products $Request){
		try{
			$this->productService->AddProduct($Request);
			$notification = array(
				'message'   => 'Thêm thành công!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function DeleteProductImg($id){
		try{
			$this->productService->DeleteProductImg($id);
			$notification = array(
				'message'   => 'Xoá thành công!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function AddMoreProductImg(Request $Request, $id){
		try{
			$this->productService->AddMoreProductImg($Request, $id);
			$notification = array(
				'message'   => 'Thêm thành công!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function FindProductImgByID($id){
		return $this->productService->FindProductImgByID($id);
	}


	public function BlockProductImg($id){
		return $this->productService->BlockProductImg($id);
	}


	public function EditProductImg(Request $Request, $id){

		try{
			$this->productService->EditProductImg($Request, $id);
			$notification = array(
				'message'   => 'Đổi hình ảnh thành công!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function AvatarProductImg($id){
		try{
			$this->productService->AvatarProductImg($id);
			$notification = array(
				'message'   => 'Đổi ảnh bìa thành công!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function EditProduct(products $Request, $id){
		try{
			$this->productService->EditProduct($Request,$id);
			$notification = array(
				'message'   => 'Sửa thành công!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}
}
