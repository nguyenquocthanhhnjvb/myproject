<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\service\companyService;
class CompanyController extends Controller
{

	protected $companyService;
	public function __construct(companyService $companyService){
		$this->companyService = $companyService;
	}

	public function index(){
		$list = $this->companyService->getAll();
		return view('backend.ListCompany',compact('list'));
	}
	public function AddCompany(Request $Request){
		try{
			$this->companyService->AddCompany($Request);
			$notification = array(
				'message'   => 'Thêm thành công!',
				'type'      => 'success',
			);
			return back()->with($notification);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
			return back()->with($notification);
		}	
	}
	public function DeleteCompany($id){
		try{
			$this->companyService->DeleteCompany($id);
			$notification = array(
				'message'   => 'Xoá thành công!',
				'type'      => 'success',
			);
			
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}
	public function EditCompany(Request $Request,$id){
		try{
			$this->companyService->EditCompany($Request,$id);
			$notification = array(
				'message'   => 'Sửa thành công!',
				'type'      => 'success',
			);
			return back()->with($notification);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
			return back()->with($notification);
		}
	}
	public function FindCompany($id){
		return $this->companyService->FindCompany($id);
	}
}
