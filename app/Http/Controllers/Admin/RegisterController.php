<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\repositories\UserRepositoryInterface;
use App\Http\Requests\Admin\register;
use Illuminate\Support\Facades\Hash;
class RegisterController extends Controller
{
	protected $userRepository;
	public function __construct(UserRepositoryInterface $userRepository){
		$this->userRepository = $userRepository;
	}
    public function index()
    {
    	return view('backend.register');
    }
    public function register(register $Request)
    {
    	$data = [
            'name' => $Request->name,
            'email' => $Request->email,
            'password' => Hash::make($Request->password),
        ];
        try{
            $this->userRepository->create($data);
            $notification = array(
                'message'   => 'Đăng ký thành công!',
                'type'      => 'success',
            );
            return redirect()->route('login')->with($notification);
        } catch(\Exception $e){
            $notification = array(
                'message'   => 'Đã xảy ra lỗi!',
                'type'      => 'warning',
                'error'     => $e->getMessage(),
            );
            return back()->with($notification);
        }

    }
}
