<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\service\orderService;

class OrderController extends Controller
{

	protected $orderService;

	public function __construct(orderService $orderService ){

		$this->orderService = $orderService;

	}
	public function index(){
		$orders = $this->orderService->getAllOrder(10);
		return view('backend.listorder',compact('orders'));
	}

	public function changestatus($id, $status){
		try{
			$this->orderService->changestatus($id,$status);
			$notification = array(
				'message'   => 'Chuyển trạng thái thành công!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function orderdetail($id){
		$orderdetail = $this->orderService->orderdetail($id);
		// dd($orderdetail);
		if(empty($orderdetail))
			abort(404);
		return view('backend.orderdetail',compact('orderdetail'));
	}
}
