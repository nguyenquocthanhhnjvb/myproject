<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\service\importService;
class ImportController extends Controller
{
	protected $importService;

	public function __construct(importService $importService){
		$this->importService = $importService;
	}

    public function index(){
    	$imports = $this->importService->getInfo();
    	// dd($imports);
    	return view('backend.listimport',compact('imports'));
    }

    public function importdetail($id){
        $import = $this->importService->getInfoOne($id);
        // dd($import);
        return view('backend.importdetail',compact('import'));
    }

    public function addform(){
    	$providers 	= $this->importService->getProvider();
    	$products 	= $this->importService->getProduct();
    	return view('backend.addimport',compact('providers','products'));
    }

    public function addimport(Request $Request){

    	$result = $this->importService->addimport($Request);

    	if($result){
    		$notification = array(
                'message'   => 'Thêm thành công!',
                'type'      => 'success',
            );
            return redirect()->route('listimport')->with($notification);
        }else{
            $notification = array(
                'message'   => 'Đã xảy ra lỗi!',
                'type'      => 'warning',
                'error'     => $result,
            );
        return back()->with($notification);
    }
}

public function Changestatus($id,$status){
    try{
        $this->importService->Changestatus($id,$status);
        $notification = array(
            'message'   => 'Đã thay đổi!',
            'type'      => 'success',
        );
        return back()->with($notification);
    } catch(\Exception $e){
        $notification = array(
            'message'   => 'Đã xảy ra lỗi!',
            'type'      => 'warning',
            'error'     => $e->getMessage(),
        );
        return back()->with($notification);
    }
}
}
