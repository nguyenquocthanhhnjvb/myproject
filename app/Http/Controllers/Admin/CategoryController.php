<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\service\categoryService;
use App\Http\Requests\Admin\category;
use App\Http\Requests\Admin\subcategory;
class CategoryController extends Controller
{
	protected $categoryService;
	public function __construct(categoryService $categoryService){
		$this->categoryService = $categoryService;
	}

	public function index(){
		$list = $this->categoryService->getlist_sub_cate();
		return view('backend.ListCategory',compact('list'));
	}

	public function addcategory(category $Request){
		try{
			$this->categoryService->addCategory($Request);
			$notification = array(
				'message' 	=> 'Thêm thành công!',
				'type' 		=> 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message' 	=> 'Đã xảy ra lỗi!',
				'type' 		=> 'warning',
				'error'		=> $e->getMessage(),
			);
		}
		return back()->with($notification);
	}
	public function addSubCategory(subcategory $Request){

		try {
			$this->categoryService->addSubCategory($Request);
			$notification = array(
				'message' 	=> 'Thêm thành công!',
				'type' 		=> 'success',
			);
		} catch (\Exception $e) {
			$notification = array(
				'message' 	=> 'Đã xảy ra lỗi!',
				'type' 		=> 'warning',
				'error'		=> $e->getMessage(),
			);
		}
		return back()->with($notification);
	}
	public function addcategory_sub()
	{
		$categorys = $this->categoryService->getAll();
		return view('backend.AddCategorys',compact('categorys'));
	}


	//category

	public function findCategory($id){
		return $this->categoryService->findCategory($id);
	}
	public function DeleteCate($id){
		try{

			$this->categoryService->DeleteCate($id);
			$notification = array(
				'message'   => 'Xoá thành công!',
				'type'      => 'success',
			);
			
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function EditCate(category $Request,$id){
		
		try {
			$this->categoryService->EditCate($Request,$id);
			$notification = array(
				'message' 	=> 'Sửa thành công!',
				'type' 		=> 'success',
			);
		} catch (\Exception $e) {
			$notification = array(
				'message' 	=> 'Đã xảy ra lỗi!',
				'type' 		=> 'warning',
				'error'		=> $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	// subcategory

	public function findSubCategory($id){
		return $this->categoryService->findSubCategory($id);
	}	
	public function DeleteSub($id){
		try{
			$this->categoryService->DeleteSub($id);
			$notification = array(
				'message'   => 'Xoá thành công!',
				'type'      => 'success',
			);
			
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}
	public function EditSub(subcategory $Request,$id){

		try {
			$this->categoryService->EditSub($Request,$id);
			$notification = array(
				'message' 	=> 'Sửa thành công!',
				'type' 		=> 'success',
			);
		} catch (\Exception $e) {
			$notification = array(
				'message' 	=> 'Đã xảy ra lỗi!',
				'type' 		=> 'warning',
				'error'		=> $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

}
