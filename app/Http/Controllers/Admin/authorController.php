<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\service\authorService;
use App\myfunction\seoname;
use App\Http\Requests\Admin\authors;
class authorController extends Controller
{
	use seoname;
	protected $authorService;
	public function __construct(authorService $authorService){
		$this->authorService = $authorService;
	}

	public function index()
	{
        $list = $this->authorService->getAll();
        return view('backend.ListAuthor',compact('list'));
    }
        public function formAdd()
    {
        return view('backend.AddAuthor');
    }
    public function addAuthor(authors $Request){
        try{
            $this->authorService->addAuthor($Request);
            $notification = array(
                'message'   => 'Thêm thành công!',
                'type'      => 'success',
            );
            if($Request->hasFile('author_image')){
                $file = $Request->author_image;
                $idAuthor = $Create->id;
                $filename = $this->saveFile($file, 'Image/authorImage');
                if($filename != false){
                    $data['avatar_image']  = $filename;
                    $this->authorRepository->update($idAuthor,$data);
                }
            }
            
        } catch(\Exception $e){
            $notification = array(
                'message'   => 'Đã xảy ra lỗi!',
                'type'      => 'warning',
                'error'     => $e->getMessage(),
            );
        }
        return back()->with($notification);
    }
    public function DeleteAuthor($id){
       return $this->authorService->DeleteAuthor($id);
    }
    public function EditAuthor(authors $Request, $id){
        $result = $this->authorService->EditAuthor($Request, $id);
        if($result == false){
            $notification = array(
                'message'   => 'Đã xảy ra lỗi!',
                'type'      => 'warning',
            );
        }else{
           $notification = array(
            'message'   => 'Sửa thành công!',
            'type'      => 'success',
        );
           
       }
       return back()->with($notification);
   }

}
