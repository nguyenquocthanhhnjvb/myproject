<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\service\promotionService;
use App\Http\Requests\Admin\promotion;
class PromotionController extends Controller
{
	protected $promotionService;

	public function __construct(promotionService $promotionService){
		$this->promotionService = $promotionService;
	}

	public function index(){
		$result = $this->promotionService->getAll();
		return view('backend.ListPromotion',compact('result'));
	}

	public function AddProform(){
		return view('backend.AddPromotion');
	}

	public function AddPromotion(promotion $Request){
		try{
			$this->promotionService->AddPromotion($Request);
			$notification = array(
				'message'   => 'Thêm thành công!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function DeletePro($id){
		try{
			$this->promotionService->DeletePro($id);
			$notification = array(
				'message'   => 'Xoá thành công!',
				'type'      => 'success',
			);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
		}
		return back()->with($notification);
	}

	public function FindPro($id){
		return $this->promotionService->FindPro($id);
	}

	public function EditPro(promotion $Request,$id){
		try{
			$this->promotionService->EditPro($Request,$id);
			$notification = array(
				'message'   => 'Sửa thành công!',
				'type'      => 'success',
			);
			return back()->with($notification);
		} catch(\Exception $e){
			$notification = array(
				'message'   => 'Đã xảy ra lỗi!',
				'type'      => 'warning',
				'error'     => $e->getMessage(),
			);
			return back()->with($notification);
		}
	}

	public function blockPro($id){
		return $this->promotionService->blockPro($id);
	}
}
