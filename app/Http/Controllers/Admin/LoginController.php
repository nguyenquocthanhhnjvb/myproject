<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\repositories\UserRepositoryInterface;
use App\Http\Requests\Admin\login;
use Cart;
use Auth;
use App\repositories\cartRepositoryInterface;
class LoginController extends Controller
{
	protected $userRepository;
	protected $cartRepository;

	public function __construct(UserRepositoryInterface $userRepository,cartRepositoryInterface $cartRepository){
		$this->userRepository = $userRepository;
		$this->cartRepository = $cartRepository;
	}
	public function index()
	{
		return view('backend.login');
	}
	public function login(login $Request)
	{
		$data = [
			'name' 		=> $Request->name,
			'password'	=> $Request->password,
		];
		
		if (Auth::guard()->attempt($data)) {
			if(auth()->user()->role == 'customer'){
				if(Cart::count() != 0){
					foreach (Cart::content() as $key => $value) {
						$this->cartRepository->create([
							'user_id'	 => auth()->user()->id,
							'product_id' => $value->id,
							'quatity' 	 => $value->qty,
							'bought' 	 => true,
						]);
						Cart::remove($value->rowId);
					}
				}
			}
			return redirect('/boss');
		}else{
			return back()->withErrors(['warning' => 'Tài khoản hoặc mật khẩu không chính xác!']);
		}
	}
	public function logout()
	{
		if(isset(auth()->user()->role) && auth()->user()->role == 'admin'){
			Auth::guard()->logout();
			return redirect('/');
		}else{
			Auth::guard()->logout();
			return back();
		}
	}
}
