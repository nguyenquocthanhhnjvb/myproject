<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class provider extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        $id = $this->route()->id;
        return [
            'provider_name'     => ['bail','required',Rule::unique('provider')->ignore($id)],
            'provider_phone'    => ['bail','required','numeric'],
            'provider_email'    => ['bail','required','email',],
            'provider_address'  => ['required'],
        ];
    }
    public function messages()
    {
        return [
            'provider_name.required'    => 'Vui lòng điền vào trường này!',
            'provider_phone.required'   => 'Vui lòng điền vào trường này!',
            'provider_email.required'   => 'Vui lòng điền vào trường này!',
            'provider_address.required' => 'Vui lòng điền vào trường này!',
            'provider_name.unique'      => 'NCC đã tồn tại!',
            'provider_phone.numeric'    => 'Số điện thoại phải là số!',
            'provider_email.email'      => 'Định dang email không chính xác!',
        ];
    }
}
