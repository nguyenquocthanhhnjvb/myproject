<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class authors extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        $id = $this->route()->id;
        return [
            'author_name'      => ['bail','required',Rule::unique('authors')->ignore($id)],
            'author_info'      => ['bail','max:2000'],
            'author_image'     =>['image','max:2048'],
        ];
    }
    public function messages()
    {
        return [
            'author_name.required'  => 'Vui lòng điền vào trường này!',
            'author_name.unique'    => 'Tác giả đã tồn tại!',
            'author_info.max'       => 'Văn bản tối đa 2000 Ký tự!',
            'author_image.max'      => 'Kích thước file vượt quá 2MB!',
            'author_image.image'    => 'File Upload phải là ảnh!',
        ];
    }
}
