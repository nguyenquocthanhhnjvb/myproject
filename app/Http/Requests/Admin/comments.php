<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class comments extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rateStar'      => ['required'],
            'rateTitle'     => ['required','max:200'],
            'rateContent'   => ['required'],
        ];
    }

     public function messages()
    {
        return [
        'rateStar.required'     => 'Vui lòng điền vào trường này!',
        'rateContent.required'  => 'Vui lòng điền vào trường này!',
        'rateTitle.required'    => 'Vui lòng điền vào trường này!',
        'rateTitle.max'         => 'Tối đa 200 ký tự!',
        ];
    }
}
