<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class products extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name'      => ['required','max:120','min:10'],
            'subcategory'       => ['required'],
            'author'            => ['required'],
            'company'           => ['required'],
            'description'       => ['bail','required'],
            'price'             => ['bail','required','numeric'],
            'length_width'      => ['bail','required','max:120'],
            'quatity'           => ['bail','required','numeric'],
            'publication_date'  => ['bail','required','date','before_or_equal:'.date("Y-m-d")],
            'page_number'       => ['bail','required','numeric'],
            'product_image.*'   => ['image','mimes:jpeg,png,jpg,gif,svg'],
        ];
    }

    public function messages()
    {
        return [
            'product_name.required'             => 'Vui lòng điền vào trường này!',
            'product_name.max'                  => 'Độ dài tối đa 120 ký tự!',
            'product_name.min'                  => 'Độ dài tối thiểu 10 ký tự!',

            'subcategory.required'              => 'Vui lòng điền vào trường này!',
            'author.required'                   => 'Vui lòng điền vào trường này!',
            'company.required'                  => 'Vui lòng điền vào trường này!',

            'description.required'              => 'Người mua có thể sẽ an tâm hơn khi thông tin sản phẩm được viết chi tiết!',

            'price.numeric'                     => 'Phải nhập số!',
            'price.required'                    => 'Vui lòng điền vào trường này!',

            'quatity.numeric'                   => 'Phải nhập số!',
            'quatity.required'                  => 'Vui lòng điền vào trường này!',

            'length_width.required'             => 'Vui lòng điền vào trường này!',
            'length_width.max'                  => 'Độ dài tối đa 120 ký tự!',

            'publication_date.required'         => 'Vui lòng điền vào trường này!',
            'publication_date.date'             => 'Phải nhập ngày tháng năm!',
            'publication_date.before_or_equal'  => 'Ngày xuất bản không thể lớn hơn ngày hiện tại!',
            
            'page_number.numeric'               => 'Phải nhập số!',
            'page_number.required'              => 'Vui lòng điền vào trường này!',

            'product_image.image'                => 'File Upload phải là ảnh!',
        ];
    }
}
