<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class promotion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pro_name'      => ['bail','required'],
            'pro_percent'   => ['bail','required','numeric'],
            'pro_price'     => ['bail','required','numeric'],
            'pro_max'       => ['bail','required','numeric'],
            'pro_dateStart' => ['bail','required','date','before_or_equal:pro_dateEnd'],
            'pro_dateEnd'   => ['bail','required','date','after_or_equal:'.date("Y-m-d"),'after_or_equal:pro_dateStart'],
        ];
    }
    public function messages()
    {
        return [
            'pro_name.required'             => 'Vui lòng điền vào trường này!',
            'pro_percent.required'          => 'Vui lòng điền vào trường này!',
            'pro_price.required'            => 'Vui lòng điền vào trường này!',
            'pro_max.required'              => 'Vui lòng điền vào trường này!',
            'pro_dateEnd.required'          => 'Vui lòng điền vào trường này!',
            'pro_percent.numeric'           => 'Phải nhập số!',
            'pro_price.numeric'             => 'Phải nhập số!',
            'pro_max.numeric'               => 'Phải nhập số!',
            'pro_dateEnd.date'              => 'Phải nhập ngày tháng năm!',
            'pro_dateEnd.after_or_equal'    => 'Không thể nhỏ hơn ngày hiện tại hoặc ngày bắt đầu!',
            'pro_dateStart.date'            => 'Phải nhập ngày tháng năm!',
            'pro_dateStart.before_or_equal' => 'Không thể lớn hơn ngày kết thúc!',
        ];
    }
}
