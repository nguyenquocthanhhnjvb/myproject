<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class order extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => ['required','min:10'],
            'phone'     => ['required','numeric'],
            'address'   => ['required'],
            'avatar'     =>['image','max:2048'],
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => 'Vui lòng điền vào trường này!',
            'phone.required'        => 'Vui lòng điền vào trường này!',
            'phone.numeric'         => 'Số điện thoại phải là số!',
            'address.required'      => 'Vui lòng điền vào trường này!',
            'name.min'              => 'Độ dài tối thiểu 10 ký tự!',
            'avatar.max'            => 'Kích thước file vượt quá 2MB!',
            'avatar.image'          => 'File Upload phải là ảnh!',
        ];
    }
}
