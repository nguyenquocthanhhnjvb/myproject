<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class register extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => ['bail','required','min:6','unique:users','regex:/^[a-zA-Z0-9\s]/'],
            'email'     => ['bail','required','email','unique:users'],
            'password'  => ['bail','required','confirmed','min:3','regex:/^[a-zA-Z0-9\s]/'],
        ];
    }

    public function messages()
    {
        return [
        'name.required'     => 'Vui lòng điền vào trường này!',
        'email.required'    => 'Vui lòng điền vào trường này!',
        'email.email'       => 'Email không đúng định dạng!',
        'email.unique'      => 'Email đã được đăng ký!',
        'name.unique'       => 'Tên tài khoản đã được đăng ký!',
        'password.regex'    => 'Mật khẩu phải là số,ký tự tiếng việt không dấu,không chứa ký tự đặc biệt!',
        'name.regex'        => 'Tài khoản phải là số,ký tự tiếng việt không dấu,không chứa ký tự đặc biệt!',
        'password.required' => 'Vui lòng điền vào trường này!',
        'password.confirmed'=> 'Mật khẩu xác nhận không đúng!',
        'name.min'          => 'Tài khoản phải có ít nhất 6 ký tự!',
        'password.min'      => 'Mật khẩu phải có ít nhất 3 ký tự!',
        ];
    }
}
