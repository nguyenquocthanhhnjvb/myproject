<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class subcategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->id;
        return [
            'category_id'       => 'bail|required',
            'subcategory_name'  => ['bail','required',Rule::unique('subcategorys')->ignore($id)],
        ];
    }
    public function messages(){
        return [
            'required' => 'Vui lòng điền vào trường này.',
            'unique' => 'Danh mục đã tồn tại.',
        ];
    }
}
