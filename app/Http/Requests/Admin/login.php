<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class login extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => ['bail','required'],
            'password'  => ['bail','required'],
        ];
    }

     public function messages()
    {
        return [
        'name.required'     => 'Vui lòng điền vào trường này!',
        'password.required' => 'Vui lòng điền vào trường này!',
        ];
    }
}
