<?php 
namespace App\Providers; 

use Illuminate\Support\ServiceProvider; 
// use App\repositories\UserRepository; 
// use App\repositories\UserRepositoryInterface; 
// use App\repositories\contacts\elequentRepository; 
// use App\repositories\contacts\RepositoryInterfacce; 

class RepositoryServiceProvider extends ServiceProvider 
{
   public function register() 
   { 
        $this->app->bind(
        'App\repositories\UserRepositoryInterface', 
        'App\repositories\UserRepository', );

        $this->app->bind(
        'App\repositories\categoryRepositoryInterface', 
        'App\repositories\categoryRepository', );

        $this->app->bind(
        'App\repositories\subcategoryRepositoryInterface', 
        'App\repositories\subcategoryRepository', );

        $this->app->bind(
        'App\repositories\authorRepositoryInterface', 
        'App\repositories\authorRepository', );

        $this->app->bind(
        'App\repositories\companyRepositoryInterface', 
        'App\repositories\companyRepository', );

        $this->app->bind(
        'App\repositories\proRepositoryInterface', 
        'App\repositories\proRepository', );

        $this->app->bind(
        'App\repositories\productRepositoryInterface', 
        'App\repositories\productRepository', );

        $this->app->bind(
        'App\repositories\commentsRepositoryInterface', 
        'App\repositories\commentsRepository', );

        $this->app->bind(
        'App\repositories\cartRepositoryInterface', 
        'App\repositories\cartRepository', );

        $this->app->bind(
        'App\repositories\orderRepositoryInterface', 
        'App\repositories\orderRepository', );

        $this->app->bind(
        'App\repositories\personRepositoryInterface', 
        'App\repositories\personRepository', );

        $this->app->bind(
        'App\repositories\providerRepositoryInterface', 
        'App\repositories\providerRepository', );

        $this->app->bind(
        'App\repositories\importRepositoryInterface', 
        'App\repositories\importRepository', );

        $this->app->bind(
        'App\repositories\api\enjoyRepositoryInterface', 
        'App\repositories\api\enjoyRepository', );

    }
}