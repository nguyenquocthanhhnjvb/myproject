<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class product_imagesModel extends Model
{
    protected $fillable = [
		'product_id','image','thumbnail_image','priority','status',
	];
	protected $table = 'product_images';

	public function product(){
		return $this->belongsTo('App\Models\productsModel','product_id');
	}
}
