<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class cartModel extends Model
{
	protected $fillable = [
		'user_id','product_id','quatity','bought','status',
	];

	protected $table = "cart";

	public function product()
	{
		return $this->belongsTo('App\Models\productsModel','product_id');
	}


}
