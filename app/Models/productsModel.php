<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class productsModel extends Model
{
	protected $fillable = [
		'subcategory_id','author_id','company_id','product_name','url_product','description',
		'length_width','publication_date','page_number','price','quatity','status',
	];
	protected $table = 'products';

	public function author()
	{
		return $this->belongsTo('App\Models\authorsModel','author_id');
	}

	public function company()
	{
		return $this->belongsTo('App\Models\companyModel','company_id');
	}

	public function subcategorys()
	{
		return $this->belongsTo('App\Models\SubcategoryModel','subcategory_id');
	}

	public function product_images()
	{
		return $this->hasMany('App\Models\product_imagesModel','product_id','id');
	}

	public function comments()
	{
		return $this->hasMany('App\Models\commentsModel','product_id','id');
	}

	public function carts()
	{
		return $this->hasMany('App\Models\cartModel','product_id','id');
	}
}
