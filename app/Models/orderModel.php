<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class orderModel extends Model
{
    protected $fillable = [

		'user_id','promotion_id','code_order','receiver_name','orderer_email','receiver_phone','ship_address','note','status','checkout'

	];

    protected $table = "order";

}
