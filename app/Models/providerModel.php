<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class providerModel extends Model
{
    protected $fillable = [
		'provider_name','provider_phone','provider_address','provider_email',
	];
	protected $table = "provider";

	public function imports()
	{
		return $this->hasMany('App\Models\importModel','provder_id','id');
	}
}
