<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class personModel extends Model
{
    protected $fillable = [
    	'user_id','name','avatar_person','phone','address',
    ];

    protected $table = 'persons';
}
