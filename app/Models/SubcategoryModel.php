<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubcategoryModel extends Model
{
	protected $fillable = [
		'category_id','subcategory_name','url_subcategory_name','status',
	];
	protected $table = "subcategorys";

	public function category()
	{
		return $this->belongsTo('App\Models\categoryModel','category_id','id');
	}

	public function products()
	{
		return $this->hasMany('App\Models\productsModel','subcategory_id','id');
	}
}
