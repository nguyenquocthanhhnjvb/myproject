<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionModel extends Model
{
	protected $fillable = [
		'promotion_name','promotion_code','promotion_percent','promotion_price','promotion_max','status','date_start','date_end',
	];
	protected $table = "promotion";
}
