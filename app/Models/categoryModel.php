<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class categoryModel extends Model
{
	protected $fillable = [
	'category_name','url_cetegory','status',
	];
    protected $table = "categorys";

    public function subcategorys(){
    	return $this->hasMany('App\Models\SubcategoryModel','category_id','id');
    }
}
