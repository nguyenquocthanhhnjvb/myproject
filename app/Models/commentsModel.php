<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class commentsModel extends Model
{
	protected $fillable = [
		'user_id','product_id','title','content','vote','status',	
	];
	protected $table = "comments";

	public function product(){
		return $this->belongsTo('App\Models\productsModel','product_id');
	}
}
