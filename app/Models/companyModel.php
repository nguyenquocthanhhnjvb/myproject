<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class companyModel extends Model
{
   	protected $fillable = [
	'company_name','url_company',
	];
    protected $table = "company";

    public function products()
	{
		return $this->hasMany('App\Models\productsModel','company_id','id');
	}
}
