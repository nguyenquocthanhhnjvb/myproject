<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class authorsModel extends Model
{
	protected $fillable = [
		'author_name','url_author','author_info','avatar_image'
	];
	protected $table = "authors";

	public function products()
	{
		return $this->hasMany('App\Models\productsModel','category_id','id');
	}
}
