<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class importModel extends Model
{
	protected $fillable = [
		'provider_id','user_id','status'
	];
	protected $table = "import";

	public function provider()
	{
		return $this->belongsTo('App\Models\providerModel','provider_id');
	}
}
