<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class enjoy extends Model
{
    protected $fillable = [
        'product_id', 'user_id'
    ];
    protected $table = 'enjoy';
}
