<?php
namespace App\myfunction;
use App\myfunction\seoname;
use Intervention\Image\Facades\Image;

trait uploadFile
{
	use seoname;

	public function getFileName($file)
	{
		return $file->getClientOriginalName();
	}

	public function getSize($file)
	{
		return $file->getSize();
	}

	public function getFileType($file)
	{
		return $file->getMimeType();
	}

	public function getExtendFile($file)
	{
		return $file->getClientOriginalExtension();
	}

	public function createFileName($file)
	{
		if(!is_file($file)) return false;
		$FullfileName = $this->getFileName($file);
		$fileExtend = $this->getExtendFile($file);
		$fileName = str_replace(".".$fileExtend,'',$FullfileName);
		$FullfileName = date('dmY').'-'.rand(0,1000).'-'.$this->cleartext($fileName).'.'.$fileExtend;
		return $FullfileName;
	}

	public function saveFile($file, $path)
	{
		if(!is_file($file)) return false;
		$filename = $this->createFileName($file);
		if($file->move($path,$filename))
			return $filename;
		else
			return false;
	}

	public function deleteFile($filename, $path)
	{
		if(!$filename) return false;
		if(!file_exists($path.$filename)) return true;
		return unlink($path.$filename);
	}

	public function resize($file,$path,$width,$height){
		if(!is_file($file)) return false;
		$filename 	= $this->createFileName($file);
		$image 		= Image::make($file);
		$image->fit($width, $height)->save($path.'/'.$filename);
		return $filename;
	}

		public function resize_old_name($file,$path,$name,$width,$height){
		if(!is_file($file)) return false;
		$image 		= Image::make($file);
		$image->fit($width, $height)->save($path.'/'.$name);
		return $name;
	}
}