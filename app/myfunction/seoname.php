<?php
namespace App\myfunction;
use App\Models\productsModel;
trait seoname{
	public function cleartext($str){
        if(!$str) return false;
        $unicode = array(
            'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),
            'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),
            'd'=>array('đ'),
            'D'=>array('Đ'),
            'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),
            'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),
            'i'=>array('í','ì','ỉ','ĩ','ị'),
            'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),
            'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ'),
            'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),
            'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),
            'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),
            'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),
            'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
            '-'=>array(' ','&quot;','.','-–-')
        );
        foreach($unicode as $nonUnicode=>$uni){
            foreach($uni as $value)
                $str = @str_replace($value,$nonUnicode,$str);
            $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);
            $str = preg_replace("/-+-/","-",$str);
            $str = preg_replace("/^\-+|\-+$/","",$str);
        }
        return strtolower($str);
    }

    // lấy ảnh avatar
    public function getAvatar($id){
        $thumbnail_image = productsModel::select('product_images.thumbnail_image')->where('products.id',$id)->join('product_images', 'products.id','=','product_images.product_id')->where('product_images.status','=','avatar')->get();

        if($thumbnail_image->isNotEmpty())
            return $thumbnail_image[0]['thumbnail_image'];
        else
            return $thumbnail_image;
    }

    //lấy trung bình cmt
    public function averageCmt(array $attributes)
    {
        $total = count($attributes);
        if($total == 0) return false;
        $nam = 0;
        $bon = 0;
        $ba  = 0;
        $hai = 0;
        foreach ($attributes as $key => $comment) {
            if($comment['vote'] == 5){
                $nam++;
            }
            elseif ($comment['vote'] == 4) {
                $bon++;

            }
            elseif ($comment['vote'] == 3) {
                $ba++;

            }
            elseif ($comment['vote'] == 2) {
                $hai++;
            }
        }
            //Tính % của các đánh giá!
        $averageNam = intval($nam/$total*100);
        $averageBon = intval($bon/$total*100);
        $averageBa  = intval($ba/$total*100);
        $averageHai = intval($hai/$total*100);
        $averagemot = 100 - ($averageNam + $averageBon + $averageBa + $averageHai);

        $averageCmt = [
            '5' => $averageNam,
            '4' => $averageBon,
            '3' => $averageBa,
            '2' => $averageHai,
            '1' => $averagemot,
        ];
        $a = array_keys($averageCmt,max($averageCmt));
        $averageCmt['topAverage']   = $a[0];
        $averageCmt['total']        = $total;

        return $averageCmt;
    }
}