<?php
namespace App\service;

use Illuminate\Http\Request;
use App\repositories\orderRepositoryInterface;
use DB;
class orderService
{	

	protected $orderRepository;

	public function __construct(
		orderRepositoryInterface $orderRepository ){
		$this->orderRepository = $orderRepository;
	}

	// public function addOrder($Request){
	// 	$checkout = $Request->checkout;
	// 	switch ($checkout) {
	// 		case 'offline':
	// 			$this->checkoutOf($Request); 
	// 		break;
			
	// 		case 'online':
	// 			$this->checkoutOn($Request); 
	// 		break;
	// 	}
	// }

    public function checkoutOf($Request){
    	$userId = auth()->user()->id;

    	$data = [
    		'user_id'			=> $userId,
    		'receiver_name' 	=> $Request->name,
    		'receiver_phone' 	=> $Request->phone,
    		'ship_address'		=> $Request->address,
    		'note'				=> $Request->note,
    		'checkout'			=> 'noactive',
    		'status'			=> 'noactive',
    	];

    	$listprd = DB::table('cart')
    	->select('product_id','cart.quatity as buyquatity','products.price','products.quatity as prdquatity')
    	->where('user_id',$userId)
    	->join('products','cart.product_id','=','products.id')->get();
		// dd($listprd);
    	DB::beginTransaction();

    	try {
    		$order = $this->orderRepository->create($data);

    		$count = 0;

    		foreach ($listprd as $key => $value) {

    			if ($value->prdquatity <= $value->buyquatity) {
					//Đếm số sp hết hàng
    				$count++;

    			}
				// nếu tất cả sp trong giỏ hết hàng ko thêm order
    			if($count == count($listprd))
    			{

    				DB::rollBack();

    			}
    			elseif($value->prdquatity >= $value->buyquatity){
    				$order_detail = [
    					'order_id' 		=> $order->id,
    					'product_id'	=> $value->product_id,
    					'price'			=> $value->price,
    					'quatity'		=> $value->buyquatity,
    					'created_at'	=> now(),
    					'updated_at'	=> now(),	
    				];
    				DB::table('products')
    				->where('id', $value->product_id)
    				->update(['quatity' => ($value->prdquatity - $value->buyquatity)]);

    				DB::table('cart')->where('product_id',$value->product_id)->delete();

    				DB::table('detail_order')->insert($order_detail);
    			}
    		}
    		DB::commit();

    		$notsuccess = DB::table('cart')->count();

    		return $notsuccess;

    	} catch (Exception $e) {
    		DB::rollBack();

    		return $e->getMessage();
    	}
    }

    public function checkoutOn_getID($Request){
    	$userId = auth()->user()->id;

    	$data = [
    		'user_id'			=> $userId,
    		'receiver_name' 	=> $Request->name,
    		'receiver_phone' 	=> $Request->phone,
    		'ship_address'		=> $Request->address,
    		'note'				=> $Request->note,
    		'checkout'			=> 'noactive',
    		'status'			=> 'noactive',
    	];

    	$listprd = DB::table('cart')
    	->select('product_id','cart.quatity as buyquatity','products.price','products.quatity as prdquatity')
    	->where('user_id',$userId)
    	->join('products','cart.product_id','=','products.id')->get();
		// dd($listprd);
    	DB::beginTransaction();

    	try {
    		$order = $this->orderRepository->create($data);

    		$count = 0;

    		foreach ($listprd as $key => $value) {

    			if ($value->prdquatity <= $value->buyquatity) {
					//Đếm số sp hết hàng
    				$count++;

    			}
				// nếu tất cả sp trong giỏ hết hàng ko thêm order
    			if($count == count($listprd))
    			{

    				DB::rollBack();

    			}
    			elseif($value->prdquatity >= $value->buyquatity){
    				$order_detail = [
    					'order_id' 		=> $order->id,
    					'product_id'	=> $value->product_id,
    					'price'			=> $value->price,
    					'quatity'		=> $value->buyquatity,
    					'created_at'	=> now(),
    					'updated_at'	=> now(),	
    				];
    				DB::table('products')
    				->where('id', $value->product_id)
    				->update(['quatity' => ($value->prdquatity - $value->buyquatity)]);

    				DB::table('cart')->where('product_id',$value->product_id)->delete();

    				DB::table('detail_order')->insert($order_detail);
    			}
    		}
    		DB::commit();

    		return $order->id;

    	} catch (Exception $e) {
    		DB::rollBack();

    		return $e->getMessage();
    	}
    }
    public function cancel($id){

    	DB::beginTransaction();

    	try {

    		$this->orderRepository->update($id,['status' => 'cancel']);

    		$listprd = DB::table('detail_order')
    		->select('detail_order.*')
    		->join('products','detail_order.product_id','=','products.id')
    		->where('detail_order.order_id',$id)->get();
				// dd($listprd);
    		if($listprd->isNotEmpty()){
    			foreach ($listprd as $key => $prd) {
    				$prdinfo = DB::table('products')->where('id',$prd->product_id)->get();

    				DB::table('products')
    				->where('id',$prd->product_id)
    				->update(['quatity' => ($prd->quatity + $prdinfo[0]->quatity)]);
    			}
    		}
    		DB::commit();

    		return true;

    	} catch (Exception $e) {
    		DB::rollBack();

    		return $e->getMessage();
    	}

    }

    public function changestatus($id,$status){
    	switch ($status) {
    		case 'active':
    		return $this->orderRepository->update($id,['status' => 'active']);
    		break;
    		case 'ship':
    		return $this->orderRepository->update($id,['status' => 'ship']);
    		break;
    		case 'done':
    		return $this->orderRepository->update($id,['status' => 'done','checkout' => 'active']);
    		break;
    	}
    }

    public function orderdetail($id){
    	return $this->orderRepository->orderdetail($id);
    }

    public function getListOrder($userId,$paginate){
    	return $this->orderRepository->getListOrder($userId,$paginate);
    }

    public function update($id,$data){
    	return $this->orderRepository->update($id,$data);
    }
    public function getAllOrder($paginate){
    	return $this->orderRepository->getAllOrder($paginate);
    }
}