<?php
namespace App\service;

use Illuminate\Http\Request;
use App\repositories\importRepositoryInterface;
use App\repositories\productRepositoryInterface;
use App\repositories\providerRepositoryInterface;
use DB;
/**
* 
*/
class importService
{   
	protected $importRepository;
	protected $productRepository;
	protected $providerRepository;

	public function __construct(

		importRepositoryInterface $importRepository,
		productRepositoryInterface $productRepository,
		providerRepositoryInterface $providerRepository ){

		$this->importRepository = $importRepository;
		$this->productRepository = $productRepository;
		$this->providerRepository = $providerRepository;

	}
	public function getProvider(){
		return $this->providerRepository->getAll();
	}

	public function getProduct(){
		return $this->productRepository->getAll();
	}

	public function getInfo(){
		return $this->importRepository->getInfo();
	}

	public function getInfoOne($id){
		return $this->importRepository->getInfoOne($id);
	}

	public function addimport($Request){

		$userId 		= auth()->user()->id;
		$numberRecord 	= count($Request->product);
		$products 		= $Request->product;
		$quatity 		= $Request->quatity;
		$price 			= $Request->price;
		$provider 		= $Request->provider;

		DB::beginTransaction();

		try {

			$import = $this->importRepository->create([
					'user_id'		=> $userId,
					'provider_id' 	=> $provider,
				]);

			for ($i=0; $i < $numberRecord; $i++) { 
				$import_detail = [
					'product_id' 	=> $products[$i],
					'import_id'		=> $import->id,
					'price'			=> $price[$i],
					'quatity'		=> $quatity[$i],
					'created_at'	=> now(),
					'updated_at'	=> now(),	
				];

				DB::table('detail_import')->insert($import_detail);

				$product = $this->productRepository->find($products[$i]);

				$this->productRepository->update($products[$i], ['quatity' => ($product->quatity + $quatity[$i])] );
			}
			DB::commit();

			return true;

		} catch (Exception $e) {
			DB::rollBack();

			return $e->getMessage();
		}
	}

	public function Changestatus($id,$status){
		if ($status == 'accept'){
			return $this->importRepository->update($id,['status' => 'accept']);
		}elseif($status == 'cancel'){
			return $this->importRepository->update($id,['status' => 'cancel']);
		}

	}
}