<?php
namespace App\service;

use Illuminate\Http\Request;
use App\myfunction\seoname;
use App\repositories\commentsRepositoryInterface;
/**
* 
*/
class commentsService
{
	protected $commentsRepository;

	public function __construct(commentsRepositoryInterface $commentsRepository){

		$this->commentsRepository = $commentsRepository;

	}

	public function addComments($Request,$userid,$prdid){
		$data = [
			'user_id' 		=> $userid,
			'product_id'	=> $prdid,
			'title'			=> $Request->rateTitle,
			'content'		=> $Request->rateContent,
			'vote'			=> $Request->rateStar,
		];
		return $this->commentsRepository->create($data);
	}
}