<?php
namespace App\service\api;

use Illuminate\Http\Request;
use App\service\productService;
// use App\repositories\api\productDetailRepositoryInterface;
/**
* 
*/
class productDetailService
{   
    // protected $productDetailRepository;
    protected $productService;
    public function __construct(productService $productService ){
        // $this->productService = $productService;
        $this->productService = $productService;
    }

    public function productDetail($id){
        return $this->productService->productDetail($id);
    }
}