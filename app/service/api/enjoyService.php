<?php
namespace App\service\api;

use Illuminate\Http\Request;
use App\repositories\api\enjoyRepositoryInterface;
/**
* 
*/
class enjoyService
{   
    protected $enjoyRepository;

    public function __construct(enjoyRepositoryInterface $enjoyRepository ){
        $this->enjoyRepository = $enjoyRepository;
    }

        public function like($request){
        $productId = $request->id;
        $userId = auth()->user()->id;
        try {
            $this->enjoyRepository->create([
                'user_id' => $userId,
                'product_id' => $productId
            ]);
            $notification = [
                'status'  => true,
                'message' => 'success'
            ];
        } catch (\Exception $e) {
            $notification = [
                'status'  => 'error',
                'message' => $e->getMessage()
            ];
        }
        return \response()->json($notification);
    }

    public function unLike(Request $request){
        $productId = $request->id;
        $userId = auth()->user()->id;
        try {
            $this->enjoyRepository->unLike($productId,$userId);
            $notification = [
                'status'  => true,
                'message' => 'success'
            ];
        } catch (\Exception $e) {
            $notification = [
                'status'  => 'error',
                'message' => $e->getMessage()
            ];
        }
        return \response()->json($notification);
    }

    public function listProductLike($userId){
        return $this->enjoyRepository->listProductLike($userId);
    }

    public function listProductLike_byUser($userId,$productId){
        return $this->enjoyRepository->listProductLike_byUser($userId,$productId);
    }
}