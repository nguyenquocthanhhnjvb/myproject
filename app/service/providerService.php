<?php
namespace App\service;

use Illuminate\Http\Request;
use App\repositories\providerRepositoryInterface;
class providerService
{
	protected $providerRepository;

	public function __construct(providerRepositoryInterface $providerRepository){
		$this->providerRepository = $providerRepository;
	}
	public function addprovider($Request){

		return $this->providerRepository->create([
			'provider_name'		=> $Request->provider_name,
			'provider_phone'	=> $Request->provider_phone,
			'provider_address'	=> $Request->provider_address,
			'provider_email'	=> $Request->provider_email, 
		]);
	}

	public function getAll(){
		return $this->providerRepository->getAll();
	}

	public function Dprovider($id){
		return $this->providerRepository->delete($id);
	}

	public function Fprovider($id){
		$result = $this->providerRepository->find($id);
		$result['action'] = route('Eprovider',['id'=>$id]);
		return response()->json($result);
	}

	public function Eprovider($id,$Request){
		$data = [
			'provider_name'		=> $Request->provider_name,
			'provider_phone'	=> $Request->provider_phone,
			'provider_address'	=> $Request->provider_address,
			'provider_email'	=> $Request->provider_email, 
		];
		return $this->providerRepository->update($id,$data);
	}
}