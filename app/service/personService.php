<?php
namespace App\service;

use Illuminate\Http\Request;
use App\repositories\personRepositoryInterface;
use App\myfunction\uploadFile;

class personService
{
	use uploadFile;
	protected $personRepository;

	public function __construct(personRepositoryInterface $personRepository){

		$this->personRepository = $personRepository;

	}
	public function addInfoPerson($Request){
		$data = [
			'user_id'			=> auth()->user()->id,
			'name' 				=> $Request->name,
			'phone' 			=> $Request->phone,
			'address'			=> $Request->address,
			'avatar_person'		=> 'default.jpg',
		];
		if($Request->hasFile('avatar')){
			$file = $Request->avatar;
			$filename = $this->saveFile($file, 'Image/authorImage');
			if($filename != false){
				$data['avatar_person']  = $filename;
			}
		}
		return $this->personRepository->create($data);
	}

	public function findInfoByUser_id($userId){
		return $this->personRepository->findInfoByUser_id($userId);
	}
}