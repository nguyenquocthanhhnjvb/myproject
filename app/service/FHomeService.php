<?php
namespace App\service;

use Illuminate\Http\Request;
use App\service\productService;
use App\service\authorService;
use App\service\categoryService;
use App\service\companyService;
use App\service\api\enjoyService;
use App\myfunction\seoname;
use App\Models\usersModel;
use App\Models\productsModel;
/**
* 
*/
class FHomeService
{	
	use seoname;
	protected $productService;
	protected $authorService;
	protected $categoryService;
	protected $companyService;
	protected $enjoyService;

	public function __construct(
		productService $productService,
		categoryService $categoryService,
		companyService $companyService,
		enjoyService $enjoyService,
		authorService $authorService ){

		$this->productService = $productService;
		$this->authorService = $authorService;
		$this->categoryService = $categoryService;
		$this->enjoyService = $enjoyService;
		$this->companyService = $companyService;
	}

	public function getProductWithInfo_random($limit){
		$products = $this->productService->getProductWithInfo_random($limit);
		$result =  [];
		//  lấy ảnh avtar trong list ảnh
		foreach ($products as $keyprd => $product) {
			foreach ($product['product_images'] as $keyimg => $image) {
				$product['averageCmt'] 		  = $this->averageCmt($product['comments']);
				if($image['status'] == 'avatar'){
					$product['thumbnailImage'] = $image['thumbnail_image'];
					array_push($result, $product);
				}
			}
		}
		return $result;
	}

	// public function getProductOffet($offset,$limit){
	// 	$products = productsModel::offset($offset)->limit($limit)
    //                     ->with('author','company','product_images','subcategorys','comments')->get();
	// 	$result =  [
	// 		'products' => [],
	// 		'total' => ceil(productsModel::count() / 10)
	// 	];
	// 	//  lấy ảnh avtar trong list ảnh
	// 	foreach ($products as $keyprd => $product) {
	// 		foreach ($product['product_images'] as $keyimg => $image) {
	// 			if($image['status'] == 'avatar'){
	// 				$product['thumbnailImage'] = $image['thumbnail_image'];
	// 				array_push($result['products'], $product);
	// 			}
	// 		}
	// 	}
	// 	return $result;	
	// }

	public function FindProductByID($id){
		$result = $this->productService->getProductDetal($id);
		if(empty($result))
			return abort(404);
		$products = $result[0];
		// Thêm mảng sản phẩm liên quan
		$products['prdCategory'] = $this->getProductByCategory($products['subcategory_id'],7);

		//Thêm đánh giá trung bình vào mảng sản phẩm liên quan
		foreach ($products['prdCategory'] as $keyCate => $prdCategory) {
			if ($this->averageCmt($prdCategory['comments'])) {

				$prdCategory['averageCmt'] 		  = $this->averageCmt($prdCategory['comments']);
				$products['prdCategory'][$keyCate] = $prdCategory;

			}
		}

		//lấy avt, img của người dùng đã cmt
		foreach ($products['comments'] as $key => $comment) {
			$user = usersModel::find($comment['user_id']);
			$comment['username'] = $user->name;
			$comment['useravatar'] = $user->avatar;
			$products['comments'][$key]= $comment;
		}

		//
		if ($this->averageCmt($products['comments'])) {
			$products['averageCmt'] = $this->averageCmt($products['comments']);
		}

		return $products;
	}

	public function getAuthorInfo_random($limit){
		return $this->authorService->getAuthorInfo_random($limit);
	}

	public function getNavBarInfo($where, $id,$name,$key){
		$comp 		= $this->companyService->getAll();
		$author 	= $this->authorService->getAll();
		$cate 		= $this->categoryService->getlist_sub_cate();

		switch ($where) {
			case null:
				$where = 'Nowhere';
				break;
			case 'cate':
				$where = 'subcategory_id';

				break;
			case 'author':
				$where = 'author_id';
				break;
			case 'comp':
				$where = 'company_id';
				break;
			
			default:
				$where = 'Nowhere';
				break;
		}
		$product 	= $this->productService->getPrdPaginate($where,$id);

		return $result = [
			'cate'		=> $cate,
			'author'	=> $author->toArray(),
			'comp' 		=> $comp->toArray(),
			'product'	=> $product,
			'name'		=> $name,
			'where'		=> $where,
			'key'		=> $key,
		];
	}

	public function getProductByCategory($id,$limit){
		$result = $this->productService->getProductByCategory($id,$limit);
		$resultWithImgAvatar =  [];
		//  lấy ảnh avtar trong list ảnh
		foreach ($result as $keyprd => $value) {
			foreach ($value['product_images'] as $keyimg => $image) {
				if($image['status'] == 'avatar'){
					$value['thumbnailImage'] = $image['thumbnail_image'];
					array_push($resultWithImgAvatar, $value);
				}
			}
		}
		return $resultWithImgAvatar;
	}

	public function search($Request){
		$searchText = $Request->searchText;

		return $this->productService->search($searchText);
	}

	public function getProductOffet($offset,$limit){
		$products = $this->productService->getProductOffet($offset,$limit);

        $result =  [
                'products' => [],
                'total' => ceil(productsModel::count() / 10)
            ];

        if(auth()->user())
        {
			$userId = auth()->user()->id;
			
			$listProductLike = $this->enjoyService->listProductLike($userId);

            //  lấy ảnh avtar trong list ảnh
            foreach ($products as $keyprd => $product) {
                $product['like'] = false;
                foreach ($product['product_images'] as $keyimg => $image) {
                    if($image['status'] == 'avatar'){
                        $product['thumbnailImage'] = $image['thumbnail_image'];
                    }
                }
                foreach($listProductLike as $key => $like){
                    if(!$product['like']){
                        if($like->product_id == $product['id']){
                            $product['like'] = true;
                        }
                    }
                }
                 array_push($result['products'], $product);
            }
		    return $result;	
        }
		//  lấy ảnh avtar trong list ảnh
		foreach ($products as $keyprd => $product) {
			foreach ($product['product_images'] as $keyimg => $image) {
				if($image['status'] == 'avatar'){
					$product['thumbnailImage'] = $image['thumbnail_image'];
					array_push($result['products'], $product);
				}
			}
		}
		return $result;	
    }

}