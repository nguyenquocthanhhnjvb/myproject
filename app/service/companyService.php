<?php
namespace App\service;

use Illuminate\Http\Request;
use App\repositories\companyRepositoryInterface;
use App\myfunction\seoname;
use App\Models\companyModel;
/**
* 
*/
class companyService
{	
	use seoname;
	protected $companyRepository;

	public function __construct(companyRepositoryInterface $companyRepository){

		$this->companyRepository = $companyRepository;

	}
	public function getAll()
	{
		return $this->companyRepository->getAll();
	}
	public function AddCompany($Request)
	{
		foreach ($Request->company_name as $key => $name) {
			if(!empty(trim($name))){
				$data[] = array('company_name' => $name,'url_company'=>$this->cleartext($name));
			}
		}
		return $this->companyRepository->insert($data);
	}
	public function DeleteCompany($id){
		return $this->companyRepository->delete($id);
	}
	public function EditCompany($Request,$id){
		$data = [
            'company_name'   => $Request->company_name,
            'url_company'    => $this->cleartext($Request->company_name),
        ];
		return $this->companyRepository->update($id,$data);
	}
	public function FindCompany($id){
		$result = $this->companyRepository->find($id);
		$result['action'] = route('EditCompany',['id'=>$id]);
		return response()->json($result);
	}
}