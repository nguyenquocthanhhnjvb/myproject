<?php
namespace App\service;

use Illuminate\Http\Request;
use App\repositories\cartRepositoryInterface;
use App\Models\productsModel;
use Cart;
use Auth;
class cartService
{
	protected $cartRepository;

	public function __construct(cartRepositoryInterface $cartRepository){

		$this->cartRepository = $cartRepository;

	}

	public function getAllBuyNow($userId){
		// dd($this->cartRepository->getAllBuyNow($userId));
		return $this->cartRepository->getAllBuyNow($userId);
	}

	public function findPrdInCart($userId, $prdId){
		return $this->cartRepository->findPrdInCart($userId, $prdId);
	}

	public function PrdInCartNoLogin()
	{
		$PrdInCartNoLogin = [];

		if(Cart::count() != 0){
			foreach (Cart::content() as $key => $value) {
				$product = productsModel::find($value->id);

				$product['rowID'] = $value->rowId;
				$product['buyQuatity'] = $value->qty;

				//tạo trường thumbnail_image lấy ảnh avt từ bảng ảnh vào $products
				$product['thumbnail_image'] = $this->cartRepository->getAvatar($value->id);

				array_push($PrdInCartNoLogin, $product);
			}
		}
		return $PrdInCartNoLogin;
	}

	public function addPrd($request,$product){

		$id = $request->id;

		$quatity = $request->quatity;

		if(!auth()->check()){
			if( $quatity > $product->quatity) return false;

			$prdInFlashCart  = Cart::content()->where('id', $id);

			if($prdInFlashCart){

				foreach ($prdInFlashCart as $key => $value) {
					if(($quatity + $value->qty) > $product->quatity) return false;
				}
			}
			
			Cart::add($id,$product->product_name, $quatity, $product->price, 0);
			return  Cart::content()->count();
		}
		else{

			$userId = auth()->user()->id;

			$prdInCart = $this->findPrdInCart($userId, $id);

			if($prdInCart)
				$newQuatity = $quatity + $prdInCart->quatity;
			else
				$newQuatity = 0;

			if ($quatity > $product->quatity || $newQuatity > $product->quatity) return false;

			if(!$prdInCart){

				$this->cartRepository->create([
					'user_id'	 => auth()->user()->id,
					'product_id' => $id,
					'quatity' 	 => $quatity,
					'bought' 	 => true,
				]);

			}else{

				$data = [
					'user_id'	 => $prdInCart->user_id,
					'product_id' => $prdInCart->product_id,
					'quatity' 	 => $newQuatity,
					'bought' 	 => true,
				];

				$this->cartRepository->update($prdInCart->id, $data);

			}

			$all = $this->cartRepository->getAllBuyNow($userId);
			return (count($all));
		}
	}

	public function UcartQuatity($request){

		$userId 	= auth()->user()->id;
		$id 		= $request->id;
		$quatity 	= $request->quatity;
		$maxQuatity = $request->maxQuatity;
		if ($quatity > $maxQuatity) return false;

		if (!auth()->check()) {

			Cart::update($id, $quatity);

			return Cart::total();
		}
		else{
			$this->cartRepository->update($id, ['quatity' => $quatity]);
			$data = $this->cartRepository->getAllBuyNow($userId);

			$total = $this->cartRepository->total($data->toArray());
			
			return $total;
		}
	}

	public function DcartPrd($request){
		$id		= $request->id;
		if (!auth()->check()) {

			Cart::remove($id);

			return Cart::total();
		}
		else{
			return $this->cartRepository->delete($id);
		}
	}

	public function loginModal($Request){
		$data = [
			'name' 		=> $Request->name,
			'password'	=> $Request->password,
		];
		if (Auth::guard()->attempt($data)) {
			if(auth()->user()->role == 'customer'){
				if(Cart::count() != 0){
					foreach (Cart::content() as $key => $value) {
						$this->cartRepository->create([
							'user_id'	 => auth()->user()->id,
							'product_id' => $value->id,
							'quatity' 	 => $value->qty,
							'bought' 	 => true,
						]);
						Cart::remove($value->rowId);
					}
				}
			}
			return redirect()->route('checkout');
		}else{
			$notification = array(
				'message'   => 'Tài khoản hoặc mật khẩu không chính xác!',
				'type'      => 'warning',
			);
			return back()->with($notification)->withErrors(['warning' => 'Tài khoản hoặc mật khẩu không chính xác!']);
		}
	}
}