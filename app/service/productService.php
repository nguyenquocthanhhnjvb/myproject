<?php
namespace App\service;

use Illuminate\Http\Request;
use App\repositories\productRepositoryInterface;
use App\myfunction\seoname;
use App\service\categoryService;
use App\service\authorService;
use App\service\companyService;
use App\myfunction\uploadFile;
use App\Models\product_imagesModel;
class productService
{	
	use seoname;
	use uploadFile;
	protected $productRepository;

	protected $authorService;

	protected $companyService;

	protected $categoryService;

	public function __construct(

		productRepositoryInterface $productRepository,
		categoryService $categoryService,
		authorService $authorService,
		companyService $companyService ){

		$this->productRepository 	= $productRepository;
		$this->categoryService 		= $categoryService;
		$this->companyService 		= $companyService;
		$this->authorService		= $authorService;

	}

	public function getAll(){
		return $this->productRepository->getProductWithInfo();
	}

	public function getProductWithInfo_random($limit){
		return $this->productRepository->getProductWithInfo_random($limit);
	}

	public function AddProduct($Request){
		$data = [
			'product_name' 		=> $Request->product_name,
			'url_product' 		=> $this->cleartext($Request->product_name),
			'subcategory_id'   	=> $Request->subcategory,
			'author_id'			=> $Request->author,
			'company_id' 		=> $Request->company,
			'description' 		=> $Request->description,
			'price'				=> $Request->price,
			'quatity'			=> $Request->quatity,
			'length_width'		=> $Request->length_width,
			'publication_date'	=> $Request->publication_date,
			'page_number'		=> $Request->page_number,
		];
		$pro = $this->productRepository->create($data);
		if($Request->hasFile('product_image')){
			$this->SaveProductImg($Request, $pro);
		}
		return $pro;
	}

	public function AddMoreProductImg($Request, $id){
		if($Request->hasFile('product_image')){
			$files = $Request->product_image;
			$priority = 0;
			foreach ($files as $key => $value) {
				$data = [];
				if($key <=10){
					$thumbnailName = $this->resize($value,'Image/productImage/thumbnailImage',150,200);
					$this->resize_old_name($value,'Image/productImage/thumbnailMedium',$thumbnailName,300,400);
					$filename = $this->saveFile($value, 'Image/productImage');
					
					$data = [
						'product_id' 		=> $id,
						'image'		 		=> $filename,
						'priority'	 		=> $priority,
						'thumbnail_image' 	=> $thumbnailName,
						'status'			=> 'show',
					];
					product_imagesModel::create($data);
					$priority++;
				}
				else break;
			}
		}
	}

	public function EditProduct($Request, $id){
		$data = [
			'product_name' 		=> $Request->product_name,
			'url_product' 		=> $this->cleartext($Request->product_name),
			'subcategory_id'   	=> $Request->subcategory,
			'author_id'			=> $Request->author,
			'company_id' 		=> $Request->company,
			'description' 		=> $Request->description,
			'price'				=> $Request->price,
			'quatity'			=> $Request->quatity,
			'length_width'		=> $Request->length_width,
			'publication_date'	=> $Request->publication_date,
			'page_number'		=> $Request->page_number,
		];
		return $this->productRepository->update($id, $data);
	}

	public function DeleteProduct($id){
		$result = $this->getProductDetal($id);
		if($result){
			foreach ($result[0]['product_images'] as $key => $value) {
				$this->deleteFile($value['thumbnail_image'], 'Image/productImage/thumbnailImage/');
				$this->deleteFile($value['image'], 'Image/productImage/');
			}
		}
		product_imagesModel::where('product_id', $id)->delete();
		return $this->productRepository->delete($id);
	}


	public function DeleteProductImg($id){
		$result = product_imagesModel::find($id);
		if($result){
			$this->deleteFile($result->thumbnail_image, 'Image/productImage/thumbnailImage/');
			$this->deleteFile($result->image, 'Image/productImage/');
		}
		return product_imagesModel::where('id',$id)->delete();
	}

	public function AvatarProductImg($id){

		$newAvatar = product_imagesModel::find($id);
		if($newAvatar){
			product_imagesModel::where('product_id',$newAvatar->product_id)->update(['status' => 'show']);
			$newAvatar->update(['status' => 'avatar']);
			return $newAvatar;
		}
		return false;
	}

	public function FindProductImgByID($id){
		$result = product_imagesModel::find($id);

		$result['action'] = route('EditProductImg',$id);

		return response()->json($result);
	}

	public function EditProductImg($Request, $id){

		if($Request->hasFile('productImg')){
			$file = $Request->productImg;
			$result = product_imagesModel::find($id);
			if ($result) {
				$this->deleteFile($result->thumbnail_image, 'Image/productImage/thumbnailImage/');
				$this->deleteFile($result->image, 'Image/productImage/');
				$thumbnailName = $this->resize($file,'Image/productImage/thumbnailImage',150,200);
				$this->resize_old_name($value,'Image/productImage/thumbnailMedium',$thumbnailName,300,400);
				$filename = $this->saveFile($file, 'Image/productImage');
				$data = [
					'image' 			=> $filename,
					'thumbnail_image'	=> $thumbnailName,
				];
				$result->update($data);
				return $result;
			}
		}
	}

	public function BlockProductImg($id){
		$ProImg = product_imagesModel::find($id);
		if (!$ProImg){
			return false;
		}
		else{

			if($ProImg->status == 'show'){
				$data = [
					'status' => 'block',
				];
			}
			elseif($ProImg->status == 'block'){
				$data = [
					'status' => 'show',
				];
			}
			$ProImg->update($data);
			return response()->json($ProImg);
		}
	}

	public function getProductDetal($id){
		return $this->productRepository->getProductDetal($id);
	}

	public function getProductByCategory($id,$limit){
		return $this->productRepository->getProductByCategory($id,$limit);
	}

	public  function SaveProductImg($Request, $pro){
		$files = $Request->product_image;
		$priority = 0;
		foreach ($files as $key => $value) {
			$data = [];
			if($key <=10){
				$thumbnailName = $this->resize($value,'Image/productImage/thumbnailImage',150,200);
				$this->resize_old_name($value,'Image/productImage/thumbnailMedium',$thumbnailName,300,400);
				$filename = $this->saveFile($value, 'Image/productImage');

				$data = [
					'product_id' 		=> $pro->id,
					'image'		 		=> $filename,
					'priority'	 		=> $priority,
					'thumbnail_image' 	=> $thumbnailName,
				];
				if ($priority == 0)
					$data['status'] = 'avatar';
				else
					$data['status'] = 'show';

				product_imagesModel::create($data);
				
				$priority++;
			}
			else break;
		}
	}


	public function get_Cate_Author_Company(){
		$data = [];
		$data['listCate'] 		= $this->getListCate();
		$data['listAuthor']		= $this->getListAuthor();
		$data['listCompany']	= $this->getListCompany();
		return $data;
	}

	public function getListCate(){
		return $listCate = $this->categoryService->getlist_sub_cate();
	}

	public function getListAuthor(){
		$listAuthor = $this->authorService->getAll();
		return $listAuthor->toArray();
	}

	public function getListCompany(){
		$listCompany = $this->companyService->getAll();
		return $listCompany->toArray();
	}

	public function getPrdPaginate($where,$id){
		// return $where;
		$data = $this->productRepository->getPrdPaginate($where,$id);
		foreach ($data as $key => $value) {
			$data[$key]['thumbnail_image'] = $this->getAvatar($value->id);
		}
		return $data;
	}

	public function search($searchText){
		return $this->productRepository->search($searchText);
	}

	public function getProductOffet($offset,$limit){
		return $this->productRepository->getProductOffet($offset,$limit);
	}

	public function productDetail($id){
		return $this->productRepository->productDetail($id);
	}
}