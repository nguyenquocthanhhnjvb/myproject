<?php
namespace App\service;

use Illuminate\Http\Request;
use App\repositories\categoryRepositoryInterface;
use App\repositories\subcategoryRepositoryInterface;
use App\myfunction\seoname;

/**
* 
*/
class categoryService
{	
	use seoname;
	protected $categoryRepository;

	protected $subcategoryRepository;

	public function __construct(
		categoryRepositoryInterface $categoryRepository,
		subcategoryRepositoryInterface $subcategoryRepository ){

		$this->categoryRepository = $categoryRepository;

		$this->subcategoryRepository = $subcategoryRepository;

	}

	public function getAll(){
		return $this->categoryRepository->getAll();
	}

	public function addCategory($Request){
		$data = [
			'category_name' => $Request->category_name,
			'url_cetegory' 	=> $this->cleartext($Request->category_name),
		];
		return $this->categoryRepository->create($data);
	}

	public function addSubCategory($Request){
		$data = [
			'category_id' 			=> $Request->category_id,
			'subcategory_name'		=> $Request->subcategory_name,
			'url_subcategory_name' 	=> $this->cleartext($Request->subcategory_name),
		];
		return $this->subcategoryRepository->create($data);
	}

	public function getlist_sub_cate(){
		return $this->categoryRepository->getlist_sub_cate();
	}

	public function findSubCategory($id){
		$result = $this->subcategoryRepository->find($id);
		$result['action'] = route('EditSub',['id'=>$id]);
		return response()->json($result);
	}

	public function findCategory($id){
		$result = $this->categoryRepository->find($id);
		$result['action'] = route('EditCate',['id'=>$id]);
		return response()->json($result);
	}

	public function EditCate($Request,$id){
		$data = [
			'category_name' 		=> $Request->category_name,
			'url_cetegory' 	=> $this->cleartext($Request->category_name),
		];
		return $this->categoryRepository->update($id,$data);
	}

	public function DeleteCate($id){
		return $this->categoryRepository->delete($id);
	}

	public function EditSub($Request,$id){
		$data = [
			'category_id' 			=> $Request->category_id,
			'subcategory_name' 		=> $Request->subcategory_name,
			'url_subcategory_name' 	=> $this->cleartext($Request->subcategory_name),
		];
		return $this->subcategoryRepository->update($id,$data);
	}

	public function Deletesub($id){
		return $this->subcategoryRepository->delete($id);
	}
}