<?php
namespace App\service;

use Illuminate\Http\Request;
use App\repositories\proRepositoryInterface;
use App\myfunction\seoname;
/**
* 
*/
class promotionService
{ 
	use seoname;

	protected $proRepository;

	public function __construct(proRepositoryInterface $proRepository ){

		$this->proRepository = $proRepository;
	}

	public function AddPromotion($Request){
		$data = [
			'promotion_name' 		=> $Request->pro_name,
			'promotion_code'		=> $this->makeCode(),
			'promotion_percent'		=> $Request->pro_percent,
			'promotion_price' 		=> $Request->pro_price,
			'promotion_max'			=> $Request->pro_max,
			'status'				=> 'apply',
			'date_start' 				=> $Request->pro_dateStart,
			'date_end' 				=> $Request->pro_dateEnd,
		];
		dd($data);
		return $this->proRepository->create($data);
	}

	public function getAll(){
		$result = $this->proRepository->getAllOrderByDESC();
		$today = date("Y-m-d");
		foreach ($result as $key => $value) {
			if (strtotime($today) > strtotime($value->date_end))
				$value->setAttribute('expiry', true);
			else
				$value->setAttribute('expiry', false);
		}
		return $result;
	}

	public function DeletePro($id){
		return $this->proRepository->delete($id);
	}

	public function FindPro($id){
		$result = $this->proRepository->find($id);
		$result['action'] = route('EditPro',$id);
		return response()->json($result);
	}

	public function blockPro($id){
		$get_pro = $this->proRepository->find($id);
		if($get_pro->status == 'apply'){
			$data = [
				'status' => 'block',
			];
		}
		else{
			$data = [
				'status' => 'apply',
			];
		}
		$result =  $this->proRepository->update($id,$data);
		return response()->json($result);
	}

	public function EditPro($Request, $id){
		$data = [
			'promotion_name' 		=> $Request->pro_name,
			'promotion_percent'		=> $Request->pro_percent,
			'promotion_price' 		=> $Request->pro_price,
			'promotion_max'			=> $Request->pro_max,
			'status'				=> 'apply',
			'date_start' 			=> $Request->pro_dateStart,
			'date_end' 				=> $Request->pro_dateEnd,
		];
		return $this->proRepository->update($id,$data);
	}

	public function makeCode(){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < 10; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString = 'KITE-'.$randomString.rand(0,1000);
	}
}