<?php
namespace App\service;

use Illuminate\Http\Request;
use App\repositories\authorRepositoryInterface;
use App\myfunction\seoname;
use App\myfunction\uploadFile;
/**
* 
*/
class authorService
{   
    use seoname;
    use uploadFile;
    protected $authorRepository;

    public function __construct(authorRepositoryInterface $authorRepository ){

        $this->authorRepository = $authorRepository;

    }
    public function getAll()
    {
        return $this->authorRepository->getAll();
    }
    public function addAuthor(Request $Request)
    {
        $data = [
            'author_name'   => $Request->author_name,
            'url_author'    => $this->cleartext($Request->author_name),
            'author_info'   => $Request->author_info,
            'avatar_image'  =>'default.jpg',
        ];
        return $this->authorRepository->create($data);

    }

    public function getAuthorInfo_random($limit){
        return $this->authorRepository->getAuthorInfo_random($limit);
    }

    public function DeleteAuthor($id)
    {
        $result = $this->authorRepository->find($id);
        if($result->avatar_image  != 'default.jpg'){
            $this->deleteFile($result->avatar_image, 'Image/authorImage/');
        }
        return $this->authorRepository->delete($id);
        
    }
    public function EditAuthor($Request, $id)
    {
        $data = [
            'author_name'   => $Request->author_name,
            'url_author'    => $this->cleartext($Request->author_name),
            'author_info'   => $Request->author_info,
        ];
        if($Request->hasFile('author_image')){
            $file = $Request->author_image;
            $filename = $this->saveFile($file, 'Image/authorImage');
            if($filename != false){
                $data['avatar_image']  = $filename;
            }
        }
        return $this->authorRepository->update($id,$data);
    }
}