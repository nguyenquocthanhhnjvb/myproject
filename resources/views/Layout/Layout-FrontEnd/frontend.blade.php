<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title')</title>

	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
	<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('css/plugins/slick/slick.css')}}" rel="stylesheet">
	<link href="{{asset('css/plugins/slick/slick-theme.css')}}" rel="stylesheet">
	<link href="{{asset('css/plugins/blueimp/css/blueimp-gallery.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/frontend/home.css')}}" rel="stylesheet">
</head>
<body id="backtotop" class="preloading">
	<div class="backtotop">
		<a href="#backtotop" class="btn btn-info" title="Về đầu trang"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
	</div>
	<div id="preload" class="text-center">
		<div class="row m-0 load-block">
			<div class="loading">Loading</div>
			<div class="circle"></div>
			<div class="circle"></div>
			<div class="circle"></div>
		</div>
	</div>
	<div class="container-fluid p-0">
		<div class="col-12 p-0" id="header">
			<div id="menu">
				<div id="header-menu" class=" container row">
					<a class="col-2" href="{{route('home')}}"><img class="logo" src="{{asset('Image/frontend/logo-nice.png')}}" alt=""></a>
					<div class="col-6" id="search">
						<form action="">
							{{csrf_field()}}
							<div class="input-group">
								<input id="search-input" type="text" class="form-control" placeholder="Search something here">
								<div class="input-group-append">
									<button class="btn btn-info" type="button">
										<i class="fa fa-search"></i>
									</button>
								</div>
							</div>
						</form>
						<div id="search-block" class="col-10 p-0">
							<ul class="list-group list-group-flush">
							</ul>
						</div>
					</div>
					<div id="right-header" class="col-4 p-0">
						<ul class="nav navbar-top-links navbar-right" style="line-height: 40px">
							@if(!auth()->check())
								<li class="pl-2">
									<a href="{{route('login')}}" class="btn btn-light btn-sm login-link">Đăng nhập</a>
								</li>
								<li class="pl-2">
									<a href="{{route('register')}}" class="btn btn-light btn-sm login-link">Đăng ký</a>
								</li>
							@else
								<li class="pl-2">
									<a href="{{route('userinfo')}}" title="Thông tin cá nhân" class="btn btn-light btn-sm login-link">{{auth()->user()->name}}</a>
								</li>
								<li class="pl-2">
									<a href="{{route('logout')}}" title="Đăng xuất" class="btn btn-light btn-sm login-link">
										Logout <i class="fa fa-sign-out" aria-hidden="true"></i>
									</a>
								</li>
							@endif
							<li class="pl-2">
								<a class="btn btn-light cart-link" href="{{route('cart')}}">
									@if(!auth()->check() || $getAllBuyNow->isEmpty())
										<i class="fa fa-shopping-cart" aria-hidden="true"></i>
										<span id="menuCart">({{count($getAllBuyNow)}})</span>
									@else
										<i class="fa fa-shopping-cart" aria-hidden="true"></i>
										<span id="menuCart">({{count($getAllBuyNow)}})</span>
									@endif
								</a>
							</li>
						</ul>
					</div>
				</div>
				<nav class="navbar navbar-expand-sm navbar-light" id="nav-footer-menu">
					<div class="collapse navbar-collapse container pl-5" id="footer-menu">
						<ul class="navbar-nav">
							<li class="nav-item"><a href="{{route('home')}}" class="nav-link">Trang chủ</a>
							</li>
							<li class="nav-item"><a href="{{route('proList')}}" class="nav-link">Sản phẩm</a></li>
							{{-- <li class="nav-item"><a href="#" class="nav-link">Tác giả</a></li> --}}
							<li class="nav-item"><a href="#footer" class="nav-link">Liên hệ</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>

		@yield('header')
		
		<div class="container" id="content-web">
			<div class="col-12 p-0 m-0">

				@yield('content')
				
			</div>
		</div>
		<div class="col-12 p-0 " id="footer">
			<div class="overlay"></div>
			<div class="infomation row m-0">
				<div data-wow-delay="0.5s" class="col-12 wow fadeInLeft"><h3>the kite shop</h3></div>
				<div data-wow-delay="0.7s" class="col-3 wow fadeInUp">
					<h5 class="text-center">Liên hệ với chúng tôi</h5>
					<div class="p-2"><i class="fa fa-map-marker" aria-hidden="true"></i>  58 Tố Hữu, Hà Nội - Việt Nam</div>
					<div class="p-2"><i class="fa fa-phone" aria-hidden="true"></i>  0987654321</div>
					<div class="p-2"><i class="fa fa-envelope" aria-hidden="true"></i>  motconvit@gmail.com</div>
				</div>
				<div data-wow-delay="0.7s" class="col-5 slogan wow fadeInDown">
					<h5 class="text-center">Thay thói quen, đổi cuộc đời</h5>
					<div><p>slogan các thứ các thứ. slogan các thứ các thứ. slogan các thứ các thứ. slogan các thứ các thứ. slogan các thứ các thứ.slogan các thứ các thứ.</p></div>
				</div>
				<div data-wow-delay="0.7s" class="col-3 wow fadeInUp">
					<h5 class="text-center">Theo dõi chúng tôi</h5>
					<div class="p-2 text-center"><i class="fa fa-facebook-square" aria-hidden="true"></i> Facebook</div>
					<div class="p-2 text-center"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</div>
					<div class="p-2 text-center"><i class="fa fa-youtube-play" aria-hidden="true"></i> Youtube</div>
				</div>
				<div class="col-12 coppy-right">
					<div class="float-right">
						10M - <strong>15M</strong> One Year.
					</div>
					<div>
						<strong>Copyright</strong> By NT &copy; 2020
					</div>
				</div>
			</div>
		</div>
		<div id="blueimp-gallery" class="blueimp-gallery" aria-label="image gallery" aria-modal="true" role="dialog">
			<div class="slides" aria-live="polite"></div>
			<a class="prev" aria-controls="blueimp-gallery" aria-label="previous slide" aria-keyshortcuts="ArrowLeft"></a>
			<a class="next" aria-controls="blueimp-gallery" aria-label="next slide" aria-keyshortcuts="ArrowRight" ></a>
			<ol class="indicator"></ol>
		</div>
	</div>
	<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	<script src="{{asset('js/popper.min.js')}}"></script>
	<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
	<script src="{{asset('js/bootstrap.js')}}"></script>
	<script src="{{asset('js/plugins/slick/slick.min.js')}}"></script>
	<script src="{{asset('js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>
	<script src="{{asset('js/plugins/wow/wow.min.js')}}"></script>
	<script src="{{asset('js/myfunction/frontend.js')}}"></script>
	@yield('script')
	<script type="text/javascript">
		$(document).ready(function(){
			toastr.options = {
				closeButton: true,
				progressBar: true,
				preventDuplicates: true,
				showMethod: 'slideDown',
				positionClass: "toast-bottom-right",
				timeOut: 5000
			};
			@if(Session::has('message'))
				setTimeout(function() {
					toastr.{{Session::get('type')}}('', '{{Session::get('message')}}')
				}, 200);
			@endif
			@if(Session::has('error'))
				console.log('{{Session::get('error')}}');
			@endif

		});
	</script>
</body>
</html>