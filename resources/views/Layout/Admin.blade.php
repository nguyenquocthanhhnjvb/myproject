<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <!-- Toastr style -->
    <link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
    
    <link href="{{asset('css/plugins/select2/select2.min.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/blueimp/css/blueimp-gallery.min.css')}}" rel="stylesheet">

    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">


</head>

<body  class="no-skin-config">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <img alt="image" style="max-width: 50%" class="rounded-circle" src="{{asset('Image/authorImage/default.jpg')}}"/>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="block m-t-xs font-bold">{{Auth::user()->name}}</span>
                            </a>
                        </div>
                        <div class="logo-element">
                            KITE
                        </div>
                    </li>
                    <li class="active">
                        <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Sản phẩm</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{route('category')}}">Quản lý danh mục</a></li>
                            <li><a href="{{route('listProducts')}}">Quản lý sản phẩm</a></li>
                           {{--  <li><a href="{{route('listPro')}}">Quản lý khuyến mãi</a></li> --}}
                            <li><a href="{{route('author')}}">Quản lý tác giả</a></li>
                            <li><a href="{{route('company')}}">Quản lý nhà xuất bản</a></li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Mua bán</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{route('Alistorder')}}">Quản lý đơn hàng</a></li>
                            <li><a href="{{route('listimport')}}">Quản lý nhập hàng</a></li>
                            <li><a href="{{route('listprovider')}}">Quản lý nhà cung cấp</a></li>
                            {{-- <li><a href="dashboard_3.html">Báo cáo thống kê</a></li> --}}
                        </ul>
                    </li>
                    <li class="">
                        <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Social</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="dashboard_2.html">Quản lý bình luận</a></li>
                            <li class="active"><a href="index.html">Quản lý chat</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" action="search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                            </div>
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li style="padding: 20px">
                            <span class="m-r-sm text-muted welcome-message">Welcome to KITE Admin Page.</span>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts">
                                <li>
                                    <a href="mailbox.html" class="dropdown-item">
                                        <div>
                                            <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                            <span class="float-right text-muted small">4 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <a href="profile.html" class="dropdown-item">
                                        <div>
                                            <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                            <span class="float-right text-muted small">12 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <a href="grid_options.html" class="dropdown-item">
                                        <div>
                                            <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                            <span class="float-right text-muted small">4 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a href="notifications.html" class="dropdown-item">
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>


                        <li>
                            <a href="/logout">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>

                </nav>
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">

                @yield('contentTitle')

            </div>

            <div class="wrapper wrapper-content animated fadeIn">

                @yield('content')
                
            </div>

            <div class="footer">
                <div class="float-right">
                    10M - <strong>15M</strong> One Year.
                </div>
                <div>
                    <strong>Copyright</strong> By NT &copy; 2020
                </div>
            </div>
        </div>
        <div class="small-chat-box fadeInRight animated">

            <div class="heading" draggable="true">
                <small class="chat-date float-right">
                    02.19.2015
                </small>
                Small chat
            </div>

            <div class="content">

                <div class="left">
                    <div class="author-name">
                        Monica Jackson <small class="chat-date">
                            10:02 am
                        </small>
                    </div>
                    <div class="chat-message active">
                        Lorem Ipsum is simply dummy text input.
                    </div>

                </div>
                <div class="right">
                    <div class="author-name">
                        Mick Smith
                        <small class="chat-date">
                            11:24 am
                        </small>
                    </div>
                    <div class="chat-message">
                        Lorem Ipsum is simpl.
                    </div>
                </div>
                <div class="left">
                    <div class="author-name">
                        Alice Novak
                        <small class="chat-date">
                            08:45 pm
                        </small>
                    </div>
                    <div class="chat-message active">
                        Check this stock char.
                    </div>
                </div>
                <div class="right">
                    <div class="author-name">
                        Anna Lamson
                        <small class="chat-date">
                            11:24 am
                        </small>
                    </div>
                    <div class="chat-message">
                        The standard chunk of Lorem Ipsum
                    </div>
                </div>
                <div class="left">
                    <div class="author-name">
                        Mick Lane
                        <small class="chat-date">
                            08:45 pm
                        </small>
                    </div>
                    <div class="chat-message active">
                        I belive that. Lorem Ipsum is simply dummy text.
                    </div>
                </div>


            </div>
            <div class="form-chat">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control">
                    <span class="input-group-btn"> <button
                        class="btn btn-primary" type="button">Send
                    </button> </span></div>
                </div>

            </div>
            <div id="small-chat">

                <span class="badge badge-warning float-right">5</span>
                <a class="open-small-chat" href="">
                    <i class="fa fa-comments"></i>
                </a>
            </div>
            <div id="blueimp-gallery" class="blueimp-gallery">
                <div class="slides"></div>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
        <script src="{{asset('js/popper.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.js')}}"></script>
        <script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
        <script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

        <!-- Custom and plugin javascript -->
        <script src="{{asset('js/inspinia.js')}}"></script>
        
        <script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>
        <!--select2-->
        <script src="{{asset('js/plugins/select2/select2.full.min.js')}}"></script>

        <!-- Toastr -->
        <script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>

        <!--Datatable-->
        <script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
        <script src="{{asset('js/plugins/dataTables/dataTables.bootstrap4.min.js')}}"></script>
        
        <!--ckeditor-->
        <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

        <script src="{{asset('js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>

        @yield('script')
        <script type="text/javascript">
            $(document).ready(function(){
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    preventDuplicates: true,
                    showMethod: 'slideDown',
                    positionClass: "toast-bottom-right",
                    timeOut: 4000
                };
                @if(Session::has('message'))
                setTimeout(function() {
                    toastr.{{Session::get('type')}}('', '{{Session::get('message')}}')
                }, 200);
                @endif
                @if(Session::has('error'))
                console.log('{{Session::get('error')}}');
                @endif
            });
        </script>
        <script src="{{asset('js/myfunction/backend.js')}}"></script>
    </body>
    </html>

