@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Thêm nhà cung cấp.</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE / Mua bán</a>
		</li>
        <li class="breadcrumb-item active"><strong>Quản lý nhà cung cấp.</strong></li>
    </ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox-content">
    <form method="POST" action="{{route('addprovider')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group row md-form"><label class="col-sm-2 col-form-label">Tên nhà cung cấp</label>

            <div class="col-sm-10">
                <input type="text" name="provider_name" class="form-control @error('provider_name') is-invalid @enderror" placeholder="Ví dụ: Nguyễn Văn A" 
                value="{{ old('provider_name') }}"  required="">
                @error('provider_name')
                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                    <div>{{$message}}</div>
                </span>
                @enderror
            </div>

        </div>
        <div class="hr-line-dashed"></div>
        <div class="form-group row md-form"><label class="col-sm-2 col-form-label">Số điện thoại</label>

            <div class="col-sm-10">
                <input type="text" name="provider_phone" class="form-control @error('provider_phone') is-invalid @enderror" placeholder="Ví dụ: 0123456789" 
                value="{{ old('provider_phone') }}"  required="">
                @error('provider_phone')
                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                    <div>{{$message}}</div>
                </span>
                @enderror
            </div>

        </div>
        <div class="hr-line-dashed"></div>
        <div class="form-group row md-form"><label class="col-sm-2 col-form-label">Email</label>

            <div class="col-sm-10">
                <input type="email" name="provider_email" class="form-control @error('provider_email') is-invalid @enderror" placeholder="Ví dụ: thekite@gmail.com" 
                value="{{ old('provider_email') }}" required="">
                @error('provider_email')
                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                    <div>{{$message}}</div>
                </span>
                @enderror
            </div>

        </div>
        <div class="hr-line-dashed"></div>
        <div class="form-group row md-form"><label class="col-sm-2 col-form-label">Địa chỉ</label>

            <div class="col-sm-10">
                <input type="text" name="provider_address" class="form-control @error('provider_address') is-invalid @enderror" placeholder="Ví dụ: 58 Tố Hữu" 
                value="{{ old('provider_address') }}"  required="">
                @error('provider_address')
                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                    <div>{{$message}}</div>
                </span>
                @enderror
            </div>

        </div>
        <div class="hr-line-dashed"></div>
        <div class="form-group">
            <button type="submit" class=" btn btn-info">Thêm</button>
        </div>
    </form>
</div>
@endsection