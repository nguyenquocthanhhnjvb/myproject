@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Danh sách nhập hàng</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE</a>
		</li>

        <li class="breadcrumb-item active"><strong>Quản lý nhập hàng</strong></li>
    </ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
   <div class="ibox-content">
   		@if($imports->isNotEmpty())
   		<a class="btn btn-xs btn-success m-2" href="{{route('import')}}">Thêm đơn nhập hàng</a>
			<div class="col-12 p-0">
				<form action="">
					@csrf
					<table class="dataTables-example table table-striped table-bordered table-hover mb-0">
				        <thead>
				            <tr>
				                <th class="text-center" style="width: 70px">STT</th>
				                <th class="text-center">Ngày lập</th>
				                <th class="text-center">Tên nhà cung cấp</th>
				                <th class="text-center">Số sản phẩm</th>
				                <th class="text-center">Tổng tiền</th>
				                <th class="text-center">Trạng thái</th>
				                <th class="text-center">Tác vụ</th>
				            </tr>
				        </thead>
				        <tbody>
				        	@foreach($imports as $key => $import)
					        	<tr>
					            	<td class="text-center">{{$key+1}}</td>
					            	<td class="text-center">{{$import->created_at}}</td>
					            	<td class="text-center">{{$import->provider->provider_name}}</td>
					            	<td class="text-center">{{count($import->detail)}}</td>
					            	<td class="text-center">
					            		<strong class="change">{{$import->total}}</strong>
					            	</td>
					            	<td class="text-center">
					            		@if($import->status == 'accept')
					            			<span class="label label-info">Đã xác nhận</span>
					            		@elseif($import->status == 'cancel')
					            			<span class="label label-danger">Đã huỷ</span>
					            		@endif
					            	</td>
					            	<td class="text-center">
					          			<a href="{{route('importdetail',['id' => $import->id])}}" class="btn btn-xs btn-info " title="Xem chi tiết">
					          				<i class="fa fa-eye" aria-hidden="true"></i>
					          			</a>
					          			@if(empty($import->status))
											<button class="btn btn-xs btn-success button" type="submit" 
												formaction="{{route('Changestatus',['id' => $import->id,'status' => 'accept'])}}" 
												formmethod="post" name="accept" data-toggle="tooltip" title="Xác nhận đơn">
												<i class="fa fa-check" aria-hidden="true"></i>
											</button>
											<button class="btn btn-xs btn-warning button" type="submit" 
												formaction="{{route('Changestatus',['id' => $import->id,'status' => 'cancel'])}}" 
												formmethod="post" name="cancel" data-toggle="tooltip" title="Huỷ đơn">
												<i class="fa fa-times" aria-hidden="true"></i>
											</button>
										@endif
					            	</td>
					        	</tr>
							@endforeach
				    	</tbody>
			    	</table>
		    	</form>
			</div>
		@endif
   </div>
</div>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function(){

			$('.change').each(function(index){
				let money = $(this).html();
				$(this).html(money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +' ₫');
			});

		});
	</script>
@endsection
