
@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
    <h2>Chi tiết sản phẩm.</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">KITE</a></li>
        <li class="breadcrumb-item active"><strong>Quản lý sản phẩm</strong></li>
    </ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
    <div class="ibox-content">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li><a class="nav-link active" data-toggle="tab" href="#tab-1">Thông tin sản phẩm</a></li>
                <li><a class="nav-link" data-toggle="tab" href="#tab-2">Ảnh sản phẩm</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">
                        <fieldset>
                            <form method="POST" action="{{route('EditProduct',['id' => $ProductDetal['id']])}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group row md-form">
                                    <label class="col-2 col-form-label">Tên sản phẩm</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="product_name" class="form-control @error('product_name') is-invalid @enderror" placeholder="Tên sản phẩm" 
                                        value="{{empty(old('product_name')) ? $ProductDetal['product_name'] : old('product_name')}}" required="">
                                        @error('product_name')
                                        <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                                            <div>{{$message}}</div>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row md-form">
                                    <div class="col-4 row">
                                        <label class="col-4 col-form-label">Danh mục</label>
                                        <div class="col-8">
                                            <select class="select2 form-control" required="" name="subcategory">
                                                @foreach($ProductDetal['C_A_C']['listCate'] as $CateKey => $listCate)
                                                @if(!empty($listCate['subcategorys']))
                                                <optgroup label="{{$listCate['category_name']}}">
                                                    @foreach($listCate['subcategorys'] as $SubKey => $listSub)
                                                    <option {{ $ProductDetal['subcategorys']['id'] == $listSub['id'] ? "selected" : "" }} value="{{$listSub['id']}}">{{$listSub['subcategory_name']}}</option>
                                                    @endforeach
                                                </optgroup>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4 row">
                                        <label class="col-4 col-form-label">Tác giả</label>
                                        <div class="col-8">
                                            <select class="select2 form-control" required="" name="author">
                                                @foreach($ProductDetal['C_A_C']['listAuthor'] as $AuthorKey  => $listAuthor)
                                                <option {{ $ProductDetal['author']['id'] == $listAuthor['id'] ? "selected" : "" }} value="{{$listAuthor['id']}}">{{$listAuthor['author_name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4 row">
                                        <label class="col-4 col-form-label">NXB</label>
                                        <div class="col-8">
                                            <select class="select2 form-control" required="" name="company">
                                                @foreach($ProductDetal['C_A_C']['listCompany'] as $ComKey  => $listCompany)
                                                <option {{ $ProductDetal['company']['id'] == $listCompany['id'] ? "selected" : "" }} value="{{$listCompany['id']}}">{{$listCompany['company_name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row md-form">
                                    <div class="col-12">         
                                        <label class="mb-3" for="tt">Mô tả sản phẩm</label>
                                        <textarea class="form-control @error('description') is-invalid @enderror" placeholder="Mô tả sản phẩm..." name="description" id="tt" rows="3" >{{empty(old('description')) ? $ProductDetal['description'] : old('description')}}</textarea>
                                        @error('description')
                                        <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                                            <div>{{$message}}</div>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row md-form">
                                    <label class="col-3 col-form-label">Giá sản phẩm</label>
                                    <div class="col-7">
                                        <input type="text" name="price" class="form-control @error('price') is-invalid @enderror" placeholder="Giá sản phẩm" 
                                        value="{{empty(old('price')) ? $ProductDetal['price'] : old('price')}}"  required="">
                                        @error('price')
                                        <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                                            <div>{{$message}}</div>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row md-form">
                                    <label class="col-3 col-form-label">Số lượng</label>
                                    <div class="col-7">
                                        <input type="number" name="quatity" class="form-control @error('quatity') is-invalid @enderror" placeholder="Số lượng sản phẩm" 
                                        value="{{empty(old('quatity')) ? $ProductDetal['quatity'] : old('quatity')}}"  required="">
                                        @error('quatity')
                                        <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                                            <div>{{$message}}</div>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row md-form">
                                    <label class="col-3 col-form-label">Kích thước: dài x rộng(cm)</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="length_width" class="form-control @error('length_width') is-invalid @enderror" placeholder="Kích thước" 
                                        value="{{empty(old('length_width')) ? $ProductDetal['length_width'] : old('length_width')}}"  required="">
                                        @error('length_width')
                                        <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                                            <div>{{$message}}</div>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row md-form">
                                    <label class="col-3 col-form-label">Ngày xuất bản</label>
                                    <div class="col-7">
                                        <input type="date" name="publication_date" class="form-control @error('publication_date') is-invalid @enderror" placeholder="Ngày xuất bản" 
                                        value="{{empty(old('publication_date')) ? $ProductDetal['publication_date'] : old('publication_date')}}"  required="">
                                        @error('publication_date')
                                        <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                                            <div>{{$message}}</div>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row md-form">
                                    <label class="col-3 col-form-label">Số trang</label>
                                    <div class="col-7">
                                        <input type="number" name="page_number" class="form-control @error('page_number') is-invalid @enderror" placeholder="Số trang" 
                                        value="{{empty(old('page_number')) ? $ProductDetal['page_number'] : old('page_number')}}"  required="">
                                        @error('page_number')
                                        <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                                            <div>{{$message}}</div>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="col-md-12 text-center m-t">
                                    <button type="submit" class="btn btn-primary">Sửa</button>
                                </div>
                            </form>
                        </fieldset>

                    </div>
                </div>
                <div id="tab-2" class="tab-pane">
                    <div class="panel-body">
                        <form method="POST" action="" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <table class="table table-bordered table-stripped">
                                <thead>
                                    <tr>
                                        <th class="text-center">STT</th>
                                        <th class="text-center">Ảnh</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th class="text-center">Tác vụ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($ProductDetal['product_images'] as $imgKey => $img)
                                    <tr>
                                        <td class="text-center">{{$imgKey+1}}</td>
                                        <td class="text-center" style="width: 50%">
                                            <div class="lightBoxGallery">
                                                <a href="{{asset('Image/productImage/'.$img['image'])}}" title="Ảnh sản phẩm." data-gallery="">
                                                    <img style="max-width: 150px;height: auto;" src="{{asset('image/productImage/thumbnailImage/'.$img['thumbnail_image'])}}">
                                                </a>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            @if($img['status'] =='avatar')
                                            <span data-id="" class="label label-info">Ảnh bìa sản phẩm</span>
                                            @elseif($img['status'] =='show')
                                            <span data-id="{{$img['id']}}" class="label label-success">Đang hiển thị</span>
                                            @else
                                            <span data-id="{{$img['id']}}" class="label label-danger">Tạm ẩn</span>
                                            @endif
                                        </td>
                                        <td  class="text-center">
                                            @if($img['status'] =='avatar')

                                            <button type="button" name="editImg" value="{{route('FindProductImg',['id'=> $img['id']])}}" class="btn btn-xs btn btn-success" title="Đổi ảnh khác" data-toggle="modal" data-target="#parentModal">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </button>

                                            @else

                                            <button type="submit" formaction="{{route('AvatarProductImg',['id' => $img['id']])}}" class="btn btn-xs btn-primary" formmethod="post" title="Đặt làm ảnh bìa">
                                                <i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </button>
                                            <button type="button" name="editImg" value="{{route('FindProductImg',['id'=> $img['id']])}}" class="btn btn-xs btn btn-success" title="Đổi ảnh khác" data-toggle="modal" data-target="#parentModal">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </button>
                                            <button type="button" data-id="{{$img['id']}}" name="blockimg" value="{{route('BlockProductImg',['id' => $img['id']])}}" class="btn btn-xs btn-warning" title="{{$img['status'] == 'show' ? "Ẩn hiện thị ảnh" : "Hiển thị ảnh"}}">
                                                <i class="fa fa-lock" aria-hidden="true"></i>
                                            </button>
                                            <button class="btn btn-xs btn-danger button" type="submit" formaction="{{route('DeleteProductImg',['id' => $img['id']])}}" formmethod="post" name="delete" onclick="return confirm('Xóa ảnh này không?')" data-toggle="tooltip" title="Xóa nội dung">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>

                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </form>
                        <form method="POST" action="{{route('AddMoreProductImg',['id' => $ProductDetal['id']])}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="hr-line-dashed"></div>
                            <div class="col-12 row">        
                                <label class="mb-3 col-3" for="tt">Thêm hình ảnh sản phẩm</label>
                                <div class="col-7">
                                    <input type="file" class="custom-file-input" accept="image/*" id="product_image" name="product_image[]" onchange="preview_image();" multiple>
                                    <label class="custom-file-label" for="product_image">Chọn ảnh</label>
                                </div>
                                <button type="button" onclick="removeall();" class="btn btn-primary">Làm mới</button>
                                @error('product_image')
                                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                                    <div class="text-danger mt-3" role="alert">{{$message}}</div>
                                </span>
                                @enderror
                                <div id="image_preview"></div>
                            </div>
                            <div id="Addbutton" class="col-md-12 text-center m-t">
                                <button type="submit" class="btn btn-primary">Thêm ảnh</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- modal --}}
<div class="modal fade bd-example-modal-lg" id="parentModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content p-4">
            <form method="POST" action="" id="changeImgForm">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="col-sm-6 text-center">
                        <img id="productImg" style="max-width: 300px;height: auto;" class="avatar" src="{{asset('image/productImage/thumbnailImage/')}}" title="Click để thay đổi ảnh!">
                    </div>
                    <div class="col-sm-6">
                        <input type="file" accept="image/*" hidden="true" class="sr-onl fileupload" name="productImg">
                    </div>

                    <span>Click vào ảnh để thay đổi!</span>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Xác nhận</button>
                    <button type="button" class="btn btn-default close_modal"  data-dismiss="modal">Đóng</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    function removeall(){
        document.getElementById("product_image").value = "";
        $('#image_preview').slideUp("slow", function() {
            $('#image_preview').empty();
        });
        $('#Addbutton').slideUp();
    }
    function preview_image() 
    {
        $('#image_preview').show();
        $('#Addbutton').show();
        let total_file = document.getElementById("product_image").files.length;
        let img_has = $('tbody tr').length;
        if((total_file + img_has) <= 10){
            for(let i = 0 ; i < total_file ; i++)
            {
                $('#image_preview').append("<img class='p-2' style='max-width: 120px;height: auto;' src='"+URL.createObjectURL(event.target.files[i])+"'>");
            }
        }
        else{
            toastr.warning('', 'Tối đa 10 ảnh mỗi sản phẩm, sản phẩm này đã có: '+img_has+' ảnh!')
            $('#Addbutton').hide();
        }
    }
    $(document).ready(function() {
       $('#Addbutton').hide();
       $('.select2').select2({
        width:"100%",
        allowClear: true,
    });

       var editorx = CKEDITOR.replace("tt");
       editorx.on( 'instanceReady', function( ev ) {
        ev.editor.dataProcessor.writer.selfClosingEnd = '>';
    });
   });
</script>
@endsection