@extends('layout.masterlayout')
@section('title', 'Register')
@section('content')

<div class="middle-box text-center loginscreen animated fadeInDown pt-0">
    <div>
        <div>
            <h1 class="logo-name m-0">KITE</h1>
        </div>
        <h3>Welcome to the KITE!</h3>
        <form class="m-t" role="form" method="POST" action="{{route('register')}}">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Username" required="" 
                value="{{ old('name') }}" autocomplete="name">
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" required=""
                value="{{ old('email') }}" autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required="" autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="password" name="password_confirmation" class="form-control" placeholder="Comfirm Password" required="" autocomplete="new-password">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

            <p class="text-muted text-center"><small>Already have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="login">Login</a>
        </form>
    </div>
    @endsection
