@extends('layout.Admin')
@section('title', 'Admin Page')
@section('content')
@section('contentTitle')

<div class="col-lg-10">
	<h2>KITE</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="">KITE</a>
		</li>
            <!-- <li>
                <a>Home</a>
            </li> -->
            <li class="breadcrumb-item active"><strong>Home</strong></li>
        </ol>
    </div>
    <div class="col-lg-2"></div>

    @endsection
    <div class="wrapper wrapper-content animated fadeInRight">
    	<div class="row">
    		<div class="col-lg-12">
    			<div class="block-content block-content-full bg-primary-light" style="color: white; height: 300px; background-color: #98b9e3;text-align: center;">
    				<br/>
    				<i class="fa fa-newspaper-o fa-5x text-white" style="margin-top: 10px; font-size: 7em;"></i>
    				<div class="text-white-op push-15-t" style="font-size:40px; margin-top: 10px;">WEBSITE QUẢN LÝ BÁN SÁCH
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    @endsection