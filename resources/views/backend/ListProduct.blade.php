@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Danh sách sản phẩm.</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE</a>
		</li>
		<li class="breadcrumb-item active"><strong>Quản lý sản phẩm.</strong></li>
	</ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
	<div class="ibox-content">
		<div class="table-responsive">
			<form method="POST" action="">
				{{csrf_field()}}
				@if(!empty($Products))
				<a class="btn btn-xs btn-success m-2" href="{{route('formAddProduct')}}">Thêm sản phẩm</a>
				<table class="table table-striped table-bordered table-hover dataTables-example" >
					<thead>
						<tr>
							<th class="text-center">STT</th>
							<th class="text-center">Tên sản phẩm</th>
							<th class="text-center">Danh mục</th>
							<th class="text-center">Giá sản phẩm</th>
							<th class="text-center">Số lượng</th>
							<th class="text-center">Ảnh sản phẩm</th>
							<th class="text-center">Tác vụ</th>
						</tr>
					</thead>
					<tbody>
						@foreach($Products as $key => $Product)
						<tr>
							<td class="text-center">{{$key+1}}</td>
							<td class="text-center">{{$Product['product_name']}}</td>
							<td class="text-center">{{$Product['subcategorys']['subcategory_name']}}</td>
							<td class="text-center change">{{$Product['price']}}</td>
							<td class="text-center">{{$Product['quatity']}}</td>
							<td class="text-center">
								@if(!empty($Product['product_images']))
								@foreach($Product['product_images'] as $imgKey => $img)
								@if($img['status'] == 'avatar')
								<div class="lightBoxGallery">
									<a href="{{asset('Image/productImage/'.$img['image'])}}" title="Ảnh sản phẩm." data-gallery="">
										<img style="max-width: 120px;height: auto;" src="{{asset('Image/productImage/thumbnailImage/'.$img['thumbnail_image'])}}">
									</a>
								</div>
								@endif
								@endforeach
								@endif
							</td>
							<td class="text-center">
								<a href="{{route('productdetal',['id' => $Product['id']])}}" class="btn btn-xs btn-info " title="Xem chi tiết"><i class="fa fa-eye" aria-hidden="true"></i></a>
								<button class="btn btn-xs btn-danger button" type="submit" formaction="{{route('DeleteProduct',['id'=>$Product['id']])}}" formmethod="post" name="delete" onclick="return confirm('Xóa sản phẩm này không?')" data-toggle="tooltip" title="Xóa nội dung"><i class="fa fa-trash" aria-hidden="true"></i></button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				@else
				<strong>Chưa có sản phẩm nào</strong><a class="btn btn-xs btn-danger" href="{{route('formAddProduct')}}">Thêm sản phẩm.</a>
				@endif
			</form>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
			$('.change').each(function(index){
				let money = $(this).html();
				$(this).html(money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
			});
    	});
    </script>
@endsection