@extends('layout.masterlayout')
@section('title', 'Login')
@section('content')

<div class="middle-box text-center loginscreen animated fadeInDown pt-2">
    <div>
        <div>
            <h1 class="logo-name">KITE</h1>

        </div>
        <h3>Welcome to the KITE!</h3>
        <form class="m-t" role="form" method="POST" action="{{route('login')}}">
            {{ csrf_field()}}
            <div class="form-group">
                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Username" required="" 
                value="{{ old('name') }}" autocomplete="name">
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required="" autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
            @error('warning')
            <div class="alert alert-danger p-2 mt-3 mb-2" role="alert">{{$message}}</div>
            @enderror
            <a class="btn btn-link" href="/forgot"><small>Forgot password?</small></a>
            <p class="text-muted text-center mb-1"><small>Do not have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="/register">Create an account</a>
        </form>
    </div>
    @endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            toastr.options = {
                closeButton: true,
                progressBar: true,
                preventDuplicates: true,
                showMethod: 'slideDown',
                positionClass: "toast-bottom-right",
                timeOut: 5000
            };
            @if(Session::has('message'))
            setTimeout(function() {
                toastr.{{Session::get('type')}}('', '{{Session::get('message')}}')
            }, 200);
            @endif
            @if(Session::has('error'))
            console.log('{{Session::get('error')}}');
            @endif

        });
    </script>
@endsection
