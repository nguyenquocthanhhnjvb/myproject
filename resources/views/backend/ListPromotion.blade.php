@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Danh sách khuyến mãi.</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE</a>
		</li>
        <li class="breadcrumb-item active"><strong>Quản lý khuyến mãi.</strong></li>
    </ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
 <div class="ibox-content">
  <a class="btn btn-xs btn-success mb-2" href="{{route('addProform')}}" name="addCom">Thêm khuyến mãi</a>
  <form method="POST" action="">
    {{csrf_field()}}
    @if($result->isNotEmpty())
    <table class="dataTables-example table table-striped table-bordered table-hover mb-0">
      <thead>
       <tr>
        <th class="text-center">STT</th>
        <th class="text-center">Tên Khuyến mãi</th>
        <th class="text-center">Mã giảm giá</th>
        <th class="text-center">% khuyến mãi</th>
        <th class="text-center">Áp dụng cho hoá đơn từ</th>
        <th class="text-center">Số tiền giảm tối đa</th>
        <th class="text-center">Trạng thái</th>
        <th class="text-center">Ngày bắt đầu</th>
        <th class="text-center">Ngày hết hạn</th>
        <th class="text-center">Tác vụ</th>
    </tr>
</thead>
<tbody>
  @foreach($result as $key => $pro)
  <tr>
    <td class="text-center">{{$key+1}}</td>
    <td class="text-center">{{$pro->promotion_name}}</td>
    <td class="text-center">{{$pro->promotion_code}}</td>
    <td class="text-center">{{$pro->promotion_percent}}</td>
    <td class="text-center">{{$pro->promotion_price}}</td>
    <td class="text-center">{{$pro->promotion_max}}</td>
    <td class="text-center">
        @if($pro->status == 'apply' && $pro->expiry == false)
        <span data-id="{{$pro->id}}" class="label label-info">Đang áp dụng</span>
        @elseif($pro->status=='block' && $pro->expiry == false)
        <span data-id="{{$pro->id}}" class="label label-danger">Tạm ngừng</span>
        @else
        <span class="label label-warning">Đã hết hạn</span>
        @endif
    </td>
    <td class="text-center">{{$pro->date_start}}</td>
    <td class="text-center">{{$pro->date_end}}</td>
    <td class="text-center" style="width: 120px">
        @if($pro->expiry == false)
        <button type="button" name="block" data-id="{{$pro->id}}" value="{{route('blockPro',['id'=>$pro->id])}}" 
            class="btn btn-xs btn btn-warning" 
            title="{{$pro->status=='apply'? "Tạm ngừng Khuyến mãi" : "Tiếp tục khuyến mãi"}}" 
            data-toggle="tooltip">
            @if($pro->status=='apply')
            <i class="fa fa-lock" aria-hidden="true"></i>
            @else
            <i class="fa fa-unlock" aria-hidden="true"></i>
            @endif
        </button>
        @endif
        <button type="button" name="editProm" value="{{route('FindPro',['id'=>$pro->id])}}" class="btn btn-xs btn btn-primary" title="Sửa nội dung" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>

        <button class="btn btn-xs btn-danger button" type="submit" formaction="{{route('DeletePro',['id'=>$pro->id])}}" formmethod="post" name="delete" onclick="return confirm('Xóa Mã khuyến mãi này không?')" data-toggle="tooltip" title="Xóa nội dung"><i class="fa fa-trash" aria-hidden="true"></i></button>
    </td>
</tr>
@endforeach
</tbody>
</table>
@endif
</form>
</div>
</div>
{{-- modal --}}

<div class="modal fade bd-example-modal-lg" id="myModal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content p-3">
        <form method="POST" id="formModal">
            {{csrf_field()}}
            <h3 class="col-form-label">Sửa thông tin khuyến mãi</h3>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form mb-0">
                <label class="col-3 col-form-label">Tên khuyến mãi</label>
                <input type="text" id="tenkm" name="pro_name" class="col-8 form-control @error('pro_name') is-invalid @enderror" placeholder="Tên khuyến mãi" value="{{old('pro_name')}}" autofocus="" required="">
                @error('pro_name')
                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert"><div class="text-center">{{$message}}</div></span>
                @enderror
            </div>
            <div class="hr-line-dashed mt-3 mb-3"></div>
            <div class="form-group row md-form mb-2">
                <label class="col-3 col-form-label">% khuyến mãi</label>
                <input type="text" id="phantramkm" name="pro_percent" class="col-8 form-control @error('pro_percent') is-invalid @enderror" placeholder="% khuyến mãi" value="{{old('pro_percent')}}" autofocus="" required="">
                @error('pro_percent')
                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert"><div class="text-center">{{$message}}</div></span>
                @enderror
            </div>
            <div class="hr-line-dashed mt-3 mb-3"></div>
            <div class="form-group row md-form mb-2">
                <label class="col-3 col-form-label">Áp dụng cho hoá đơn từ</label>
                <input type="text" id="hoadonkm" name="pro_price" class="col-8 form-control @error('pro_price') is-invalid @enderror" placeholder="Áp dụng cho hoá đơn từ" value="{{old('pro_price')}}" autofocus="" required="">
                @error('pro_price')
                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert"><div class="text-center">{{$message}}</div></span>
                @enderror
            </div>
            <div class="hr-line-dashed mt-3 mb-3"></div>
            <div class="form-group row md-form mb-2">
                <label class="col-3 col-form-label">Số tiền giảm tối đa</label>
                <input type="text" id="maxkm" name="pro_max" class="col-8 form-control @error('pro_max') is-invalid @enderror" placeholder="Số tiền giảm tối đa" value="{{old('pro_max')}}" autofocus="" required="">
                @error('pro_max')
                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert"><div class="text-center">{{$message}}</div></span>
                @enderror
            </div>
            <div class="hr-line-dashed mt-3 mb-3"></div>
            <div class="form-group row md-form mb-2">
                <label class="col-3 col-form-label">Ngày bắt đầu</label>
                <input type="date" id="batdaukm" name="pro_dateStart" class="col-8 form-control @error('pro_dateStart') is-invalid @enderror" placeholder="Số tiền giảm tối đa" 
                value="{{ old('pro_dateStart')}}" autofocus="" required="">
                @error('pro_dateStart')
                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert"><div class="text-center">{{$message}}</div></span>
                @enderror
            </div>
            <div class="hr-line-dashed mt-3 mb-3"></div>
            <div class="form-group row md-form mb-2">
                <label class="col-3 col-form-label">Ngày hết hạn</label>
                <input type="date" id="hethankm" name="pro_dateEnd" class="col-8 form-control @error('pro_dateEnd') is-invalid @enderror" placeholder="Ngày hết hạn" value="{{old('pro_dateEnd')}}" autofocus="" required="">
                @error('pro_dateEnd')
                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert"><div class="text-center">{{$message}}</div></span>
                @enderror
            </div>
            <div class="modal-footer">
                <button type="submit" name="edit" class="btn btn-danger">Xác nhận</button>
                <button type="button" class="btn btn-default close_modal"  data-dismiss="modal">Đóng</button>
            </div>
        </form>
    </div>
</div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            @if($errors->any())
            $('button[name=editProm]').click();
            @endif
        });
    </script>
@endsection