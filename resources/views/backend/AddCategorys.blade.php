@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Thêm danh mục.</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE</a>
		</li>
		<li class="breadcrumb-item active"><strong>Quản lý danh mục.</strong></li>
	</ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="tabs-container">
	<ul class="nav nav-tabs" role="tablist">
		<li><a class="nav-link active" data-toggle="tab" href="#tab-1"> Thêm danh mục gốc.</a></li>
		<li><a class="nav-link" data-toggle="tab" href="#tab-2">Thêm danh mục con.</a></li>
	</ul>
	<div class="tab-content">
		<div role="tabpanel" id="tab-1" class="tab-pane active">
			<div class="panel-body">

				<div class="ibox-content">
					<form method="POST" action="{{route('addcategory')}}">
						{{csrf_field()}}
						<div class="form-group  row"><label class="col-sm-2 col-form-label">Tên danh mục</label>

							<div class="col-sm-10">
								<input type="text" name="category_name" class="catename form-control  @error('category_name') is-invalid @enderror" placeholder="Tên danh mục" required="" 
								value="{{ old('category_name') }}" autofocus="">
								@error('category_name')
								<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>

						</div>
						<div class="hr-line-dashed"></div>
						<div class="form-group row">
							<button type="submit" id="addCate" class=" btn btn-info">Thêm</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div role="tabpanel" id="tab-2" class="tab-pane">
			<div class="panel-body">
				<div class="ibox-content">
					<form method="POST" action="{{route('addsubcategory')}}">
						{{csrf_field()}}
						<div class="form-group  row">
							@if(!empty($categorys)&&count($categorys)!==0)
							<label class="col-sm-3 col-form-label">Chọn danh mục gốc</label>
							<div class="col-sm-7">
								<select class="select2 form-control" required="" name="category_id">
									<option></option>
									@foreach ($categorys as $category)
									<option {{ old('category_id') == $category->id ? "selected" : "" }} value="{{$category->id}}">{{$category->category_name}}</option>
									@endforeach
								</select>

							</div>
							@else
							<label class="col-sm-10 col-form-label">Chưa có danh mục gốc hãy tạo mới.</label>
							@endif
						</div>
						<div class="hr-line-dashed"></div>
						<div class="form-group  row">
							<label class="col-sm-3 col-form-label">Tên danh mục con</label>

							<div class="col-sm-7">
								<input type="text" name="subcategory_name" class="subname form-control  @error('subcategory_name') is-invalid @enderror" placeholder="Tên danh mục con" required="" 
								value="{{ old('subcategory_name') }}" autofocus="">
								@error('subcategory_name')
								<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>

						</div>
						<div class="hr-line-dashed"></div>
						<div class="form-group row text-center">
							<button type="submit" class="btn btn-info">Thêm</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		$(".select2").select2({
			width:"100%",
			placeholder: "Chọn danh mục gốc.",
			allowClear: true
		});
	});
</script>
@endsection