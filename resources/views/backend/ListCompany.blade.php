@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Nhà sản xuất.</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="#">KITE</a></li>
        <li class="breadcrumb-item active"><strong>Quản lý nhà sản xuất</strong></li>
    </ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
    <div class="ibox-content">
        <div class="table-responsive col-10 m-auto">
            <form method="POST" action="{{route('AddCompany')}}">
                {{csrf_field()}}
                <table class="table table-striped table-bordered table-hover mb-0">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 70px">STT</th>
                            <th class="w-50 text-center">Tên NSX</th>
                            <th class="text-center">Tác vụ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($list->isNotEmpty())
                        @foreach($list as $key => $company)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-center">{{$company->company_name}}</td>
                            <td class="text-center" style="width: 120px">
                                <button type="button" name="editCom" value="{{route('FindCompany',['id'=>$company->id])}}" class="btn btn-xs btn btn-primary" title="Sửa nội dung" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>

                                <button class="btn btn-xs btn-danger button" type="submit" formaction="{{route('DeleteCompany',['id'=>$company->id])}}" formmethod="post" name="delete" onclick="return confirm('Xóa nhà xuất bản này không?')" data-toggle="tooltip" title="Xóa nội dung"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                <button class="btn btn-xs btn-success pull-right" name="addCom" title="Thêm NSX"><i class="fa fa-plus-square"></i></button>
                <div class="col-md-12 text-center m-t">
                  <button type="submit" class="btn btn-primary">Lưu</button>
              </div>
          </form>
      </div>
  </div>
</div>

{{-- modal --}}

<div class="modal fade bd-example-modal-lg" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content p-4">
            <form method="POST" id="formModal">
                {{csrf_field()}}
                <h3 class="col-form-label">Sửa nhà xuất bản</h3>
                <div class="hr-line-dashed"></div>
                <div class="form-group row md-form">
                    <label class="col-3 col-form-label">Tên NXB</label>
                    <input type="text" id="tennxb" name="company_name" class="col-8 form-control" placeholder="Tên nhà xuất bản" 
                    value="" autofocus="" required="">
                </div>
                <div class="modal-footer">
                    <button type="submit" name="edit" class="btn btn-danger">Xác nhận</button>
                    <button type="button" class="btn btn-default close_modal"  data-dismiss="modal">Đóng</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        // if($('tbody tr').length <=0){
            $('button[type=submit]').attr('disabled','true').css('cursor','not-allowed');
        // }
    });
</script>
@endsection