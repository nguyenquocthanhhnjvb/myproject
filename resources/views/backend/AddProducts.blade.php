@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Thêm sản phẩm.</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE</a>
		</li>
        <li class="breadcrumb-item active"><strong>Quản lý sản phẩm.</strong></li>
    </ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
    <div class="ibox-content">
        <form method="POST" action="{{route('AddProduct')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group row md-form">
                <label class="col-2 col-form-label">Tên sản phẩm</label>
                <div class="col-sm-7">
                    <input type="text" name="product_name" class="form-control @error('product_name') is-invalid @enderror" placeholder="Ví dụ: Tiếng việt lớp 1" 
                    value="{{ old('product_name') }}" autofocus="" required="">
                    @error('product_name')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form">
                <div class="col-4 row">
                    <label class="col-4 col-form-label">Danh mục</label>
                    <div class="col-8">
                        <select class="select2 form-control" required="" name="subcategory">
                            <option></option>
                            @foreach($data['listCate'] as $CateKey => $listCate)
                            @if(!empty($listCate['subcategorys']))
                            <optgroup label="{{$listCate['category_name']}}">
                                @foreach($listCate['subcategorys'] as $SubKey => $listSub)
                                <option {{ old('subcategory') == $listSub['id'] ? "selected" : "" }} value="{{$listSub['id']}}">{{$listSub['subcategory_name']}}</option>
                                @endforeach
                            </optgroup>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-4 row">
                    <label class="col-4 col-form-label">Tác giả</label>
                    <div class="col-8">
                        <select class="select2 form-control" required="" name="author">
                            <option></option>
                            @foreach($data['listAuthor'] as $AuthorKey  => $listAuthor)
                            <option {{ old('author') == $listAuthor['id'] ? "selected" : "" }} value="{{$listAuthor['id']}}">{{$listAuthor['author_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-4 row">
                    <label class="col-4 col-form-label">NXB</label>
                    <div class="col-8">
                        <select class="select2 form-control" required="" name="company">
                            <option></option>
                            @foreach($data['listCompany'] as $CompanyKey => $listCompany)
                            <option {{ old('company') == $listCompany['id'] ? "selected" : "" }} value="{{$listCompany['id']}}">{{$listCompany['company_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form">
                <div class="col-12">         
                    <label class="mb-3" for="tt">Mô tả sản phẩm</label>
                    <textarea class="form-control @error('description') is-invalid @enderror" placeholder="Mô tả sản phẩm..." name="description" id="tt" rows="3" >{{ old('description') }}</textarea>
                    @error('description')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form">
                <label class="col-3 col-form-label">Giá sản phẩm</label>
                <div class="col-7">
                    <input type="text" name="price" class="form-control @error('price') is-invalid @enderror" placeholder="Ví dụ: 100000" 
                    value="{{ old('price') }}" autofocus="" required="">
                    @error('price')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form">
                <label class="col-3 col-form-label">Số lượng</label>
                <div class="col-7">
                    <input type="number" name="quatity" class="form-control @error('quatity') is-invalid @enderror" placeholder="Ví dụ: 100" 
                    value="{{ old('quatity') }}" autofocus="" required="">
                    @error('quatity')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form">
                <label class="col-3 col-form-label">Kích thước: dài x rộng(cm)</label>
                <div class="col-sm-7">
                    <input type="text" name="length_width" class="form-control @error('length_width') is-invalid @enderror" placeholder="Ví dụ: 17 x 23 cm" 
                    value="{{ old('length_width') }}" autofocus="" required="">
                    @error('length_width')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form">
                <label class="col-3 col-form-label">Ngày xuất bản</label>
                <div class="col-7">
                    <input type="date" name="publication_date" class="form-control @error('publication_date') is-invalid @enderror" placeholder="Ngày xuất bản" 
                    value="{{ old('publication_date')}}" autofocus="" required="">
                    @error('publication_date')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form">
                <label class="col-3 col-form-label">Số trang</label>
                <div class="col-7">
                    <input type="number" name="page_number" class="form-control @error('page_number') is-invalid @enderror" placeholder="Ví dụ: 100" 
                    value="{{ old('page_number') }}" autofocus="" required="">
                    @error('page_number')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form">        
                <label class="mb-3 col-3" for="tt">Hình ảnh sản phẩm</label>
                <div class="col-7">
                    <input type="file" class="custom-file-input" accept="image/*" id="product_image" name="product_image[]" onchange="preview_image();" multiple>
                    <label class="custom-file-label" for="product_image">Chọn ảnh</label>
                </div>
                <button type="button" onclick="removeall();" class="btn btn-primary">Làm mới</button>
                @error('product_image')
                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                    <div class="text-danger mt-3" role="alert">{{$message}}</div>
                </span>
                @enderror
                <div id="image_preview"></div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="col-md-12 text-center m-t">
                <button type="submit" class="btn btn-primary">Thêm</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    function removeall(){
        document.getElementById("product_image").value = "";
        $('#image_preview').slideUp("slow", function() {
            $('#image_preview').empty();
        });
    }
    function preview_image() 
    {
        $('#image_preview').show();
        let total_file = document.getElementById("product_image").files.length;
        if(total_file <= 10){
            for(let i = 0 ; i < total_file ; i++)
            {
                $('#image_preview').append("<img class='p-2' style='max-width: 120px;height: auto;' src='"+URL.createObjectURL(event.target.files[i])+"'>");
            }
        }
        else{
            toastr.warning('', 'Tối đa 10 ảnh mỗi sản phẩm!')
        }
    }
    $(document).ready(function() {
        $('.select2').select2({
            width:"100%",
            allowClear: true
        });

        $('select[name=company]').select2({
            placeholder: "Chọn nhà xuất bản",
        });

        $('select[name=author]').select2({
            placeholder: "Chọn tác giả",
        });

        $('select[name=subcategory]').select2({
            placeholder: "Chọn danh mục",
        });

        CKEDITOR.replace("tt");
        CKEDITOR.on( 'instanceReady', function( ev ) {
            ev.editor.dataProcessor.writer.selfClosingEnd = '>';
        });

        $("input[name=price]").on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    });
</script>
@endsection