@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Danh sách hoá đơn</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE</a>
		</li>

		<li class="breadcrumb-item active"><strong>Quản lý đơn hàng</strong></li>
	</ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
	<div class="ibox-content">
		<div class="col-12 p-0">
			<form action="">
				@csrf
				<table id="ordertable" class="table table-striped table-bordered table-hover mb-0">
			        <thead>
			            <tr>
			                <th class="text-center" style="width: 70px">STT</th>
			                <th class="text-center">Ngày lập</th>
			                <th class="text-center">Thông tin giao hàng</th>
			                <th class="text-center" style="width: 70px">Số sản phẩm</th>
			                <th class="text-center">Tổng tiền</th>
			                <th class="text-center">Thanh toán</th>
			                <th class="text-center">Trạng thái</th>
			                <th class="text-center">Tác vụ</th>
			            </tr>
			        </thead>
			        <tbody>
			        	@foreach($orders as $key => $order)
				        	<tr>
				            	<td class="text-center">{{$key+1}}</td>
				            	<td class="text-center">{{$order->created_at}}</td>
								@if(empty($order->note))
					            	<td class="text-center">
					            		{{$order->receiver_name}} - {{$order->receiver_phone}} - {{$order->ship_address}}
					            	</td>
								@else
									<td class="text-center">
					            		{{$order->receiver_name}} - {{$order->receiver_phone}} - {{$order->ship_address}}<br>
					            		(<b>Ghi chú: {{$order->note}}</b>)
					            	</td>
								@endif
				            	<td class="text-center">{{count($order->detail)}}</td>
				            	<td class="text-center">
				            		<strong class="change">{{$order->total}}</strong>
				            	</td>
				            	<td class="text-center">
				          			@if($order->checkout == 'noactive')
										Chưa thanh toán
				          			@else
				          				Đã thanh toán
				          			@endif
				            	</td>
				            	<td class="text-center">
				            		@if($order->status == 'noactive')
										<span class="badge badge-warning">Chờ xác nhận</span>
				            		@elseif($order->status == 'cancel')
										<span class="badge badge-danger">Đã huỷ</span>
									@elseif($order->status == 'active')
										<span class="badge badge-info">Đã xác nhận</span>
									@elseif($order->status == 'ship')
										<span class="badge badge-primary">Chờ nhận hàng</span>
									@else
										<span class="badge badge-primary">Đã hoàn thành</span>
				            		@endif
				            	</td>
				            	<td class="text-center">
				          			<a href="{{route('Aorderdetail',['id' => $order->id])}}" class="btn btn-xs btn-info " title="Xem chi tiết">
				          				<i class="fa fa-eye" aria-hidden="true"></i>
				          			</a>
									@if($order->status == 'noactive')
										<button class="btn btn-xs btn-success button" type="submit" 
											formaction="{{route('Changestatus',['id' => $order->id, 'status' => 'active'])}}" 
											formmethod="post" name="active" 
											title="Xác nhận đơn hàng">
											<i class="fa fa-share" aria-hidden="true"></i>
										</button>
										<button class="btn btn-xs btn-warning button" type="submit" 
											formaction="{{route('cancel',['id' => $order->id])}}" 
											formmethod="post" name="cancel"
											onclick="return confirm('Bạn muốn huỷ đơn hàng này?');" 
											title="Huỷ đơn">
											<i class="fa fa-times" aria-hidden="true"></i>
										</button>
									@elseif($order->status == 'active')
										<button class="btn btn-xs btn-success button" type="submit" 
											formaction="{{route('Changestatus',['id' => $order->id, 'status' => 'ship'])}}" 
											formmethod="post" name="ship" 
											title="Bắt đầu giao đơn hàng">
											<i class="fa fa-share" aria-hidden="true"></i>
										</button>
										<button class="btn btn-xs btn-warning button" type="submit" 
											formaction="{{route('cancel',['id' => $order->id])}}" 
											formmethod="post" name="cancel"
											onclick="return confirm('Bạn muốn huỷ đơn hàng này?');" 
											title="Huỷ đơn">
											<i class="fa fa-times" aria-hidden="true"></i>
										</button>
									@elseif($order->status == 'ship')
										<button class="btn btn-xs btn-success button" type="submit" 
											formaction="{{route('Changestatus',['id' => $order->id, 'status' => 'done'])}}" 
											formmethod="post" name="done" 
											title="Hoàn thành đơn hàng">
											<i class="fa fa-share" aria-hidden="true"></i>
										</button>
										<button class="btn btn-xs btn-warning button" type="submit" 
											formaction="{{route('cancel',['id' => $order->id])}}" 
											formmethod="post" name="cancel"
											onclick="return confirm('Bạn muốn huỷ đơn hàng này?');" 
											title="Huỷ đơn">
											<i class="fa fa-times" aria-hidden="true"></i>
										</button>
									@endif
				            	</td>
				        	</tr>
				        @endforeach
			    	</tbody>
		    	</table>
	    	</form>
		</div>
		<div class="box-footer pt-3 clearfix">
			<div class="row">
				<div class="col-md-7">
					<!-- Pagination -->
					<div class="float-right">
						<div class="no-margin text-center">
							{{$orders->links()}}
						</div>
					</div>
					<!-- / End Pagination -->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function(){

		$('.change').each(function(index){
			let money = $(this).html();
			$(this).html(money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +' ₫');
		});

	});
</script>
@endsection
