@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Thêm mã khuyến mãi.</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE</a>
		</li>
        <li class="breadcrumb-item active"><strong>Quản lý khuyến mãi.</strong></li>
    </ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
    <div class="ibox-content">
        <form method="POST" action="{{route('AddPromotion')}}">
            {{csrf_field()}}
            <div class="form-group row md-form"><label class="col-2 col-form-label">Tên khuyến mãi</label>
                <div class="col-sm-7">
                    <input type="text" name="pro_name" class="form-control @error('pro_name') is-invalid @enderror" placeholder="Tên khuyến mãi" 
                    value="{{ old('pro_name') }}" autofocus="" required="">
                    @error('pro_name')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form"><label class="col-2 col-form-label">% khuyến mãi</label>
                <div class="col-sm-7">
                    <input type="number" name="pro_percent" class="form-control @error('pro_percent') is-invalid @enderror" placeholder="% khuyến mãi" 
                    value="{{ old('pro_percent') }}" autofocus="" required="">
                    @error('pro_percent')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form"><label class="col-2 col-form-label">Hoá đơn có giá từ</label>
                <div class="col-sm-7">
                    <input type="number" name="pro_price" class="form-control @error('pro_price') is-invalid @enderror" placeholder="Áp dụng cho hoá đơn có giá từ" 
                    value="{{ old('pro_price') }}" autofocus="" required="">
                    @error('pro_price')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form"><label class="col-2 col-form-label">Số tiền giảm tối đa</label>
                <div class="col-sm-7">
                    <input type="number" name="pro_max" class="form-control @error('pro_max') is-invalid @enderror" placeholder="Số tiền giảm tối đa" 
                    value="{{ old('pro_max') }}" autofocus="" required="">
                    @error('pro_max')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form"><label class="col-2 col-form-label">Ngày bắt đầu</label>
                <div class="col-sm-5">
                    <input type="date" name="pro_dateStart" class="form-control @error('pro_dateStart') is-invalid @enderror" placeholder="Số tiền giảm tối đa" 
                    value="{{ old('pro_dateStart')}}" autofocus="" required="">
                    @error('pro_dateStart')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group row md-form"><label class="col-2 col-form-label">Ngày kết thúc</label>
                <div class="col-sm-5">
                    <input type="date" name="pro_dateEnd" class="form-control @error('pro_dateEnd') is-invalid @enderror" placeholder="Số tiền giảm tối đa" 
                    value="{{ old('pro_dateEnd') }}" autofocus="" required="">
                    @error('pro_dateEnd')
                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
                        <div>{{$message}}</div>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="col-md-12 text-center m-t">
                <button type="submit" class="btn btn-primary">Thêm</button>
            </div>
        </form>
    </div>
</div>
@endsection