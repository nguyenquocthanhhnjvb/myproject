@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Danh sách nhà cung cấp.</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE</a>
		</li>
		<li class="breadcrumb-item active"><strong>Quản lý nhà cung cấp.</strong></li>
	</ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
	<div class="ibox-content">
		<a class="btn btn-xs btn-success mb-2" href="{{route('provider')}}" name="addCom">Thêm nhà cung cấp</a>
		@if($providers->isNotEmpty())
			<form method="POST" action="">
				{{csrf_field()}}
				<table class="table table-striped table-bordered table-hover mb-0">
					<thead>
						<tr>
							<th class="text-center">STT</th>
							<th class="text-center">Tên nhà cung cấp</th>
							<th class="text-center">Email</th>
							<th class="text-center">Số điện thoại</th>
							<th class="text-center">Địa chỉ</th>
							<th class="text-center">Tác vụ</th>
						</tr>
					</thead>
					<tbody>
						@foreach($providers as $key => $provider)
							<tr>
								<td class="text-center">{{$key+1}}</td>
								<td class="text-center">{{$provider->provider_name}}</td>
								<td class="text-center">{{$provider->provider_email}}</td>
								<td class="text-center">{{$provider->provider_phone}}</td>
								<td class="text-center">{{$provider->provider_address}}</td>
								<td class="text-center">
                					<button type="button" name="editPRV" value="{{route('Fprovider',['id'=>$provider->id])}}" 
                						class="btn btn-xs btn btn-primary" title="Sửa nội dung" data-toggle="modal" data-target="#myModal">
                						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                					</button>
					                <button class="btn btn-xs btn-danger button" type="submit" 
					                	formaction="{{route('Dprovider',['id' => $provider->id])}}" formmethod="post" name="delete" onclick="return confirm('Xóa NCC này không?')" data-toggle="tooltip" title="Xóa nội dung">
					                	<i class="fa fa-trash" aria-hidden="true"></i>
					                </button>	
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</form>
		@else
			<strong>Chưa có nhà cung cấp nào </strong>
			<a class="btn btn-xs btn-success mb-2" href="{{route('provider')}}" name="addCom">Thêm nhà cung cấp</a>
		@endif
	</div>
</div>
{{-- modal --}}
<div class="modal fade bd-example-modal-lg" id="myModal" role="dialog">
   <div class="modal-dialog">
      	<div class="modal-content p-4">
         	<form method="POST" id="formModal">
            	{{csrf_field()}}
            	<h3 class="text-center m-0">Sửa nhà cung cấp</h3>
         		<div class="hr-line-dashed"></div>
		        <div class="form-group row">
		        	<label class="col-3 col-form-label">Tên NCC</label>
		            <div class="col-9">
		                <input type="text" id="ncc" name="provider_name" class="form-control @error('provider_name') is-invalid @enderror" placeholder="Ví dụ: Nguyễn Văn A" 
		                value="{{ old('provider_name') }}" required="">
		                @error('provider_name')
		                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
		                    <div>{{$message}}</div>
		                </span>
		                @enderror
		            </div>
		        </div>
      			<div class="hr-line-dashed"></div>
		        <div class="form-group row">
		        	<label class="col-3 col-form-label">Số điện thoại</label>
		            <div class="col-9">
		                <input type="text" id="sdt" name="provider_phone" class="form-control @error('provider_phone') is-invalid @enderror" placeholder="Ví dụ: 0123456789" 
		                value="{{ old('provider_phone')}}" required="">
		                @error('provider_phone')
		                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
		                    <div>{{$message}}</div>
		                </span>
		                @enderror
		            </div>
		        </div>
      			<div class="hr-line-dashed"></div>
		        <div class="form-group row">
		        	<label class="col-3 col-form-label">Email</label>
		            <div class="col-9">
		                <input type="email" id="email" name="provider_email" class="form-control @error('provider_email') is-invalid @enderror" placeholder="Ví dụ: thekite@gmail.com" 
		                value="{{ old('provider_email') }}" required="">
		                @error('provider_email')
		                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
		                    <div>{{$message}}</div>
		                </span>
		                @enderror
		            </div>
		        </div>
      			<div class="hr-line-dashed"></div>
		        <div class="form-group row">
		        	<label class="col-3 col-form-label">Địa chỉ</label>
		            <div class="col-9">
		                <input type="text" id="address" name="provider_address" class="form-control @error('provider_address') is-invalid @enderror" placeholder="Ví dụ: 58 Tố Hữu" 
		                value="{{ old('provider_address	') }}" required="">
		                @error('provider_address')
		                <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
		                    <div>{{$message}}</div>
		                </span>
		                @enderror
		            </div>
		        </div>
      			<div class="modal-footer">
			       	<button type="submit" name="edit" class="btn btn-danger">Xác nhận</button>
			       	<button type="button" class="btn btn-default close_modal"  data-dismiss="modal">Đóng</button>
   				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            @if($errors->any())
         		$('button[name=editPRV]').click();
        	@endif
        });
    </script>
@endsection