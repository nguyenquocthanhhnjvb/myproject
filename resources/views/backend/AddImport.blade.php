@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Đơn nhập hàng</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="#">KITE</a></li>
        <li class="breadcrumb-item active"><strong>Quản lý nhập hàng</strong></li>
    </ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
    <div class="ibox-content">
        <form method="POST" action="{{route('addimport')}}">
            {{csrf_field()}}
            <div class="form-group m-0 mb-3 row">
            <label class="col-3 text-right" for="provider"><strong>Nhà cung cấp:</strong></label>
            <div class="col-5">
                <select class="form-control" name="provider" id="provider" required="">
                    <option disabled selected hidden>Chọn nhà cung cấp</option>
                        @foreach($providers as $providerKey => $provider)
                            <option value="{{$provider->id}}">{{$provider->provider_name}}</option>
                        @endforeach
                </select>
            </div>
            </div>
            <table class="table table-striped table-bordered table-hover mb-0">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 70px">STT</th>
                        <th class="text-center">Tên sản phẩm</th>
                        <th class="text-center">Số lượng nhập</th>
                        <th class="text-center">Đơn giá nhập</th>
                        <th class="text-center">Thành tiền</th>
                        <th class="text-center">Tác vụ</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <button class="btn btn-xs btn-success pull-right" name="addimport" title="Thêm NSX"><i class="fa fa-plus-square"></i></button>
            <div class="col-md-12 text-center m-t">
              <button type="submit" id="addimport" class="btn btn-primary">Lưu</button>
            </div>
        </form>
    </div>
    <div class="d-none" id="prd">
        <select class="form-control" name="product[]" required="">
            @foreach($products as $prdKey => $product)
                <option></option>
                <option value="{{$product->id}}">{{$product->product_name}}</option>
            @endforeach
        </select>
    </div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('#addimport').hide();
    });
</script>
@endsection