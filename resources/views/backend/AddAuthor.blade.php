@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Thêm tác giả.</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE</a>
		</li>
            <!-- <li>
                <a>Home</a>
            </li> -->
            <li class="breadcrumb-item active"><strong>Quản lý tác giả.</strong></li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox-content">
	<form method="POST" action="{{route('addAuthor')}}" enctype="multipart/form-data">
		{{csrf_field()}}
		<div class="form-group row md-form"><label class="col-sm-2 col-form-label">Tên tác giả</label>

			<div class="col-sm-10">
				<input type="text" name="author_name" class="form-control @error('author_name') is-invalid @enderror" placeholder="Tên tác giả" 
				value="{{ old('author_name') }}" autofocus="" required="">
				@error('author_name')
				<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
					<div>{{$message}}</div>
				</span>
				@enderror
			</div>

		</div>
		<div class="hr-line-dashed"></div>
		<div class="form-group">
			<label for="tt">Thông tin tác giả</label>
			<textarea class="form-control @error('author_info') is-invalid @enderror" name="author_info" id="tt" rows="3" >{{ old('author_info') }}</textarea>
			@error('author_info')
			<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
				<div>{{$message}}</div>
			</span>
			@enderror
		</div>
		<div class="hr-line-dashed"></div>
		<div class="form-group">
			<label for="avt">Click vào ảnh để thay đổi ảnh đại diện</label>

			<div class="col-sm-6 text-center">
				<img style="max-width: 300px;height: auto;" class="avatar" src="{{asset('image/authorImage/default.jpg')}}" title="Click để thay đổi ảnh!">
			</div>
			<div class="col-sm-6">
				<input type="file" id="avt" accept="image/*" hidden="true" class="sr-onl fileupload" name="author_image">
			</div>
			@error('author_image')
			<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
				<div class="text-danger mt-3" role="alert">{{$message}}</div>
			</span>
			@enderror
		</div>

		<div class="hr-line-dashed"></div>
		<div class="form-group row">
			<button type="submit" class=" btn btn-info">Thêm</button>
		</div>
	</form>
</div>
@endsection