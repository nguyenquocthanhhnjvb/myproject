@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Danh sách danh mục.</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE</a>
		</li>

        <li class="breadcrumb-item active"><strong>Quản lý danh mục.</strong></li>
    </ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
   <div class="ibox-content">
      <div class="table-responsive">
         <form action="" method="POST">
            {{csrf_field()}}
            @if(!empty($list)&&count($list)!==0)
            <a class="btn btn-xs btn-success mb-2" href="{{route('add')}}" name="addCom">Thêm danh mục, danh mục con</a>
            <table class="table table-striped table-bordered table-hover" >
               <thead>
                  <tr>
                     <th>STT</th>
                     <th>Tên danh mục</th>
                     <th>Tên danh mục con</th>
                     <th>Trạng thái</th>
                     <th>Tác vụ</th>
                 </tr>
             </thead>
             <tbody>
              @foreach($list as $key =>$value)
              <tr>
                 <td class="text-center" rowspan="{{count($value['subcategorys'])+1}}">{{$key+1}}</td>
                 @if(!empty($value['subcategorys']))
                 <td rowspan="{{count($value['subcategorys'])+1}} ">
                    {{$value['category_name']}}

                    <button type="button" name="editCate" value="{{route('findCate',['id'=>$value['id']])}}" class="btn btn-xs btn btn-primary pull-right" title="Sửa nội dung" data-toggle="modal" data-target="#parentModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                </td>
                @foreach($value['subcategorys'] as $k =>$sub)
                <tr>
                    <td>{{$sub['subcategory_name']}}</td>
                    <td>trangthai</td>
                    <td class="text-center">
                       <button class="btn btn-xs btn-danger button" type="submit" formaction="{{route('DeleteSub',['id'=>$sub['id']])}}" formmethod="post" name="delete" onclick="return confirm('Xóa danh mục con này không?')" data-toggle="tooltip" title="Xóa nội dung"><i class="fa fa-trash" aria-hidden="true"></i></button>	

                       <button type="button" name="editSubCate" value="{{route('findsub',['id'=>$sub['id']])}}" class="btn btn-xs btn btn-primary" title="Sửa nội dung" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                   </td>
               </tr>
               @endforeach
               @else
               <td rowspan="{{count($value['subcategorys'])+1}} ">
                {{$value['category_name']}}
            </td>
            <td></td>
            <td>trangthai cha</td>
            <td class="text-center">
                <button class="btn btn-xs btn-danger button" type="submit" formaction="{{route('DeleteCate',['id'=>$value['id']])}}" formmethod="post" name="delete" onclick="return confirm('Xóa danh mục con này không?')" data-toggle="tooltip" title="Xóa nội dung"><i class="fa fa-trash" aria-hidden="true"></i></button>	

                <button type="button" name="editCate" value="{{route('findCate',['id'=>$value['id']])}}" class="btn btn-xs btn btn-primary" title="Sửa nội dung" data-toggle="modal" data-target="#parentModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
            </td>
            @endif
        </tr>
        @endforeach
    </tbody>
    <tfoot>
    </tfoot>
</table>
@else
<strong>Chưa có danh mục nào</strong><a class="btn btn-xs btn-success mb-2" href="{{route('add')}}" name="addCom">Thêm danh mục, danh mục con</a>
@endif
</form>
</div>

</div>
</div>

{{-- modal --}}
<div class="modal fade bd-example-modal-lg" id="myModal" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content p-4">
         <form method="POST" id="formModal">
            {{csrf_field()}}
            <h3 class="col-form-label">Sửa danh mục con</h3>
            <div class="form-group row">
               <label class="col-sm-3 col-form-label">Chọn DM gốc</label>
               <div class="col-sm-7">
                  <select class="form-control" required="" name="category_id">
                     @foreach($list as $k =>$v)
                     <option value="{{$v['id']}}">{{$v['category_name']}}</option>
                     @endforeach
                 </select>
             </div>
         </div>
         <div class="hr-line-dashed"></div>
         <div class="form-group  row">
           <label class="col-sm-3 col-form-label">Tên danh mục con</label>
           <div class="col-sm-7">
              <input type="text" id="tensub" name="subcategory_name" class="subname form-control" placeholder="Tên danh mục con" required="" 
              value="" autofocus="">
          </div>

      </div>
      <div class="modal-footer">
       <button type="submit" name="edit" class="btn btn-danger">Xác nhận</button>
       <button type="button" class="btn btn-default close_modal"  data-dismiss="modal">Đóng</button>
   </div>
</form>
</div>
</div>
</div>

{{--parent modal --}}

<div class="modal fade bd-example-modal-lg" id="parentModal" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content p-4">
         <form method="POST" id="formParentModal">
            {{csrf_field()}}
            <h3 class="col-form-label text-center"><strong>Sửa danh mục</strong></h3>
            <div class="hr-line-dashed"></div>
            <div class="form-group  row">
               <label class="col-sm-3 col-form-label">Tên danh mục</label>
               <div class="col-sm-7">
                  <input type="text" id="tencate" name="category_name" class="subname form-control" placeholder="Tên danh mục" required="" 
                  value="" autofocus="">
              </div>

          </div>
          <div class="modal-footer">
           <button type="submit" class="btn btn-danger">Xác nhận</button>
           <button type="button" class="btn btn-default close_modal"  data-dismiss="modal">Đóng</button>
       </div>
   </form>
</div>
</div>
</div>
@endsection

