@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Danh sách nhập hàng</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE</a>
		</li>

        <li class="breadcrumb-item active"><strong>Quản lý nhập hàng</strong></li>
    </ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
   <div class="ibox-content">
   		<a class="btn btn-xs btn-success m-2 mb-4" href="{{route('listimport')}}">Danh sách đơn nhập hàng</a>
		<div class="col-12 mb-5">
			<p class="clearfix mb-4 pb-1" style="border-bottom: 1px solid #ebebeb">Đơn nhập hàng 
				@if($import->status == 'accept')
        			<span class="label label-import">Đã xác nhận</span>
        		@elseif($import->status == 'cancel')
        			<span class="label label-danger">Đã huỷ</span>
        		@endif
				<b class="float-right mr-4">Ngày lập: {{$import->created_at}}</b>
			</p>
			<h3 class="text-center">Thông tin nhà cung cấp</h3>
				<div class="col-12 row m-0 justify-content-around mb-4 mt-3 pb-1" style="border-bottom: 1px solid #ebebeb">
					<span>Họ tên: {{$import->provider->provider_name}}</span>
					<span>Số điện thoại: {{$import->provider->provider_phone}}</span>
					<span>Địa chỉ: {{$import->provider->provider_address}}</span>
					<span>Email: {{$import->provider->provider_email}}</span>
				</div>
			<h3 class="text-center">Danh sách sản phẩm nhập</h3>
			<div class="col-12 p-0">
				<table class="table table-striped table-bordered table-hover mb-0">
			        <thead>
			            <tr>
			                <th class="text-center" style="width: 70px">STT</th>
			                <th class="text-center">Tên sản phẩm</th>
			                <th class="text-center">Số lượng nhập</th>
			                <th class="text-center">Đơn giá nhập</th>
			                <th class="text-center">Thành tiền</th>
			            </tr>
			        </thead>
			        <tbody>
			        	@php $total = 0; @endphp
			        	@foreach($import->detail as $prdKey => $prd)
			        		@php $total += ($prd->price*$prd->quatity) @endphp
				        	<tr>
				            	<td class="text-center">{{$prdKey+1}}</td>
				            	<td class="text-center">{{$prd->product_name}}</td>
				            	<td class="text-center">{{$prd->quatity}}</td>
				            	<td class="text-center">{{$prd->price}}</td>
				            	<td class="text-center">
				            		<strong class="change">{{$prd->price * $prd->quatity}}</strong>
				            	</td>
				        	</tr>
				        @endforeach
			    	</tbody>
		    	</table>
		    	<p class="text-right mr-4 mt-3" >Tổng tiền: <strong class="change">{{$total}}</strong></p>
			</div>
		</div>
   </div>
</div>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function(){

			$('.change').each(function(index){
				let money = $(this).html();
				$(this).html(money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +' ₫');
			});

		});
	</script>
@endsection
