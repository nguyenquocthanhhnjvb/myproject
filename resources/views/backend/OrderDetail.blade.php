@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
    <h2>Danh sách nhập hàng</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#">KITE</a>
        </li>

        <li class="breadcrumb-item active"><strong>Quản lý nhập hàng</strong></li>
    </ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
   <div class="ibox-content">
        <a class="btn btn-xs btn-success m-2 mb-4" href="{{route('Alistorder')}}">Danh sách đơn hàng</a>
        <div class="col-12 mb-5">
            <p class="clearfix mb-4 pb-1" style="border-bottom: 1px solid #ebebeb">
                Đơn nhập hàng 
                @if($orderdetail->status == 'noactive')
                    <span class="badge badge-warning p-1">Chờ xác nhận</span>
                @elseif($orderdetail->status == 'cancel')
                    <span class="badge badge-danger p-1">Đã huỷ</span>
                @elseif($orderdetail->status == 'active')
                    <span class="badge badge-info p-1">Đã xác nhận</span>
                @else
                    <span class="badge badge-primary p-1">Chờ nhận hàng</span>
                @endif
                <b class="float-right mr-4">Ngày lập: {{$orderdetail->created_at}}</b>
            </p>
            <h5 class="text-center">Thông tin giao hàng</h5>
                <div class="col-12 row m-0 justify-content-around mb-4 mt-3 pb-1" style="border-bottom: 1px solid #ebebeb;font-size: 14px;">
                    <span>Họ tên: {{$orderdetail->receiver_name}}</span>
                    <span>SĐT: {{$orderdetail->receiver_phone}}</span>
                    <span>Địa chỉ: {{$orderdetail->ship_address}}</span>
                    @if(!empty($orderdetail->note))
                        <span>Ghi chú: {{$orderdetail->note}}</span>
                    @endif
                </div>
            <h5 class="text-center">Danh sách sản phẩm</h5>
            <div class="col-12 p-0">
            <table class="table table-striped table-bordered table-hover mb-0">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 70px">STT</th>
                        <th class="text-center">Ảnh sản phẩm</th>
                        <th class="text-center">Tên sản phẩm</th>
                        <th class="text-center">Số lượng</th>
                        <th class="text-center">Đơn giá</th>
                        <th class="text-center">Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    @php $total = 0; @endphp
                    @foreach($orderdetail->detail as $key => $prd)
                        @php $total += ($prd->price*$prd->quatity) @endphp
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-center">
                                <img style="max-width: 80px;height: auto;" src="{{asset('Image/productImage/thumbnailImage/'.$prd->thumbnail_image)}}">
                            </td>
                            <td class="text-center">{{$prd->product_name}}</td>
                            <td class="text-center">{{$prd->quatity}}</td>
                            <td class="text-center">{{$prd->price}}</td>
                            <td class="text-center">
                                <strong class="change">{{$prd->quatity * $prd->price}}</strong>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <p class="text-right mr-4 mt-3" >Tổng tiền: <strong class="change">{{$total}}</strong></p>
        </div>
    </div>
   </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){

            $('.change').each(function(index){
                let money = $(this).html();
                $(this).html(money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +' ₫');
            });

        });
    </script>
@endsection
