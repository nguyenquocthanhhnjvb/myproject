@extends('layout.Admin')
@section('title', 'Admin Page')
@section('contentTitle')
<div class="col-lg-10">
	<h2>Danh sách tác giả.</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">KITE</a>
		</li>
		<li class="breadcrumb-item active"><strong>Quản lý tác giả.</strong></li>
	</ol>
</div>
<div class="col-lg-2"></div>
@endsection
@section('content')
<div class="ibox ">
	<div class="ibox-content">
		<div class="table-responsive">
			@if($list->isNotEmpty())
			<a class="btn btn-xs btn-success m-2" href="{{route('formAdd')}}">Thêm tác giả.</a>
			<table class="table table-striped table-bordered table-hover dataTables-example" >
				<thead>
					<tr>
						<th>STT</th>
						<th>Tên tác giả</th>
						<th>Thông tin tác giả</th>
						<th>Ảnh đại diện</th>
						<th>Tác vụ</th>
					</tr>
				</thead>
				<tbody>
					@foreach($list as $key => $author)
					<tr id="{{$author->id}}_data">
						<td style="width: 20px">{{$key+1}}</td>
						<td >{{$author->author_name}}</td>
						<td >{{$author->author_info}}</td>
						<td >
							<div class="lightBoxGallery">
								<a href="{{asset('Image/authorImage/'.$author->avatar_image)}}" title="Ảnh tác giả." data-gallery="">
									<img style="max-width: 120px;height: auto;" src="{{asset('Image/authorImage/'.$author->avatar_image)}}">
								</a>
							</div>
						</td>
						<td>
							<button type="button" data-toggle="modal" data-target="#editmodal_{{$key}}" class="btn btn-xs btn-info btnEditAuthor">Sửa</button>
							<div class="modal fade bd-example-modal-lg" id="editmodal_{{$key}}" role="dialog">
								<div class="modal-dialog modal-lg ">
									<div class="modal-content p-4">
										<form method="POST" action="{{route('EditAuthor',$author->id)}}" enctype="multipart/form-data">
											{{csrf_field()}}
											<div class="form-group row md-form"><label class="col-sm-2 col-form-label">Tên tác giả</label>

												<div class="col-sm-10">
													<input type="text" name="author_name" class="form-control @error('author_name') is-invalid @enderror" placeholder="Tên tác giả" 
													value="{{empty(old('author_name')) ? $author->author_name : old('author_name')}}" autofocus="" required="">
													@error('author_name')
													<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
														<div>{{$message}}</div>
													</span>
													@enderror
												</div>

											</div>
											<div class="hr-line-dashed"></div>
											<div class="form-group">
												<label for="tt">Thông tin tác giả</label>
												<textarea class="form-control @error('author_info') is-invalid @enderror" name="author_info" rows="3" >{{empty(old('author_info')) ? $author->author_info : old('author_info')}}</textarea>
												@error('author_info')
												<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
													<div>{{$message}}</div>
												</span>
												@enderror
											</div>
											<div class="hr-line-dashed"></div>
											<div class="form-group">

												<div class="col-sm-6 text-center">
													<img style="max-width: 300px;height: auto;" class="avatar" src="{{asset('image/authorImage/').'/'.$author->avatar_image}}" title="Click để thay đổi ảnh!">
												</div>
												<div class="col-sm-6">
													<input type="file" accept="image/*" hidden="true" class="sr-onl fileupload" name="author_image">
												</div>

												<span>Click vào ảnh để thay đổi!</span>
												@error('author_image')
												<div class="text-danger mt-3" role="alert">{{$message}}</div>
												@enderror

											</div>
											
											<div class="modal-footer">
												<button type="submit" class="btn btn-danger">Xác nhận</button>
												<button type="button" class="btn btn-default close_modal"  data-dismiss="modal">Đóng</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							<button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#dltmodal_{{$key}}">Xoá</button>
							<div class="modal fade" id="dltmodal_{{$key}}" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body text-center mt-3">
											<div>
												<p><strong>Xoá tác giả: </strong>{{ $author->author_name}}?</p>
												<div class="modal-footer">
													<button type="button" value="{{$author->id}}" class="btn btn-danger deleteAuthor">Xác nhận</button>
													<button type="button" class="btn btn-default close_modal" data-dismiss="modal">Đóng</button>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@else
			<strong>Chưa có tác giả nào</strong><a class="btn btn-xs btn-danger" href="{{route('formAdd')}}">Thêm tác giả.</a>
			@endif
		</div>
	</div>
</div>
@endsection
@section('script')
	<script type="text/javascript">
		$(document).ready(function(){
			@if($errors->any())
			$('.btnEditAuthor').click();
			@endif
		});
	</script>
@endsection