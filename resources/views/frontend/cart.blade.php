@extends('layout.Layout-FrontEnd.frontend')
@section('title', 'KITE | Giỏ hàng')
@section('content')
<div class="col-12 m-0 block">
	@if(!auth()->check())
	@if(!empty($PrdInCartNoLogin))
	<h5 class="pl-4" style="border-bottom: 1px solid #ebebeb">Giỏ hàng( {{count($getAllBuyNow)}} sản phẩm)</h5>
	<div class="col-12 row m-0">
		<div class="col-8 m-0">
			@foreach($PrdInCartNoLogin as $key => $value)
			<div class="col-12 row m-0 p-0">
				<a href="{{route('proInfo',['name' => $value->url_product, 'id' => $value->id])}}">
					<img class="img-slider" src="{{asset('Image/productImage/thumbnailImage/'.$value->thumbnail_image)}}">
				</a>
				<div class="info pl-4 pt-2">
					<p><a class="pb-3" style="font-size: 20px;" 
						href="{{route('proInfo',['name' => $value->url_product, 'id' => $value->id])}}">
						{{$value->product_name}}
					</a></p>
					<strong>Giá: </strong>	<b class="change">{{$value->price}}</b>
					<p><strong>Số lượng: </strong>{{$value->buyQuatity}}</p>
					<div class="form-inline p-0">
						<label style="font-size: 15px;" class="m-0" for="{{$value->rowID}}"><strong>Số lượng: </strong></label>
						<input type="number" data-name="{{$value->product_name}}" value="{{$value->buyQuatity}}" min="1" max="{{$value->quatity}}" class="form-control ml-3 mb-3" id="{{$value->rowID}}">
					</div>
					<p class="del_bought">
						<span class="del" data-id="{{$value->rowID}}">Xoá</span>
					</p>
				</div>
			</div>
			@endforeach
		</div>
		<div class="col-4 pt-4" style="border-left: 1px solid #ebebeb">
			<strong>Giá tạm tính: </strong>	<b class="change total">{{Cart::total()}}</b>
			<p class="mb-3"><strong>Thuế: </strong>0</p>
			<strong>Tổng tiền: </strong>	<b class="change total">{{Cart::total()}}</b>
			<div class="col-12 pt-3" style="border-top: 1px solid #ebebeb">
				<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal">Tiến hành đặt hàng</button>

				{{-- modal --}}
				<div class="modal fade bd-example-modal-lg" id="myModal" role="dialog">
				   	<div class="modal-dialog">
				      	<div class="modal-content w-75 m-auto p-4">
				      		<h6 class="text-center mt-3 mb-3">Bạn cần <strong>Đăng nhập</strong> để tiến thành mua hàng!</h6>
					        <form class="m-t" role="form" method="POST" action="{{route('cartlogin')}}">
					        {{ csrf_field()}}
					            <div class="form-group">
					                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Username" required="" 
					                value="{{ old('name') }}" autocomplete="name">
					                @error('name')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					                @enderror
					            </div>
					            <div class="form-group">
					                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required="">
					                @error('password')
					                <span class="invalid-feedback" role="alert">
					                    <strong>{{ $message }}</strong>
					                </span>
					                @enderror
					            </div>
					            <button type="submit" class="btn btn-primary w-100 m-b">Login</button>
					            @error('warning')
						            <div class="alert alert-danger p-2 mt-3 mb-2" role="alert">{{$message}}</div>
						        @enderror
					            <p class="text-muted text-center mb-1"><small>Do not have an account?</small></p>
					            <a class="btn btn-sm btn-white btn-block" href="/register">Create an account</a>
					            <button type="button" class="btn btn-sm btn-default pull-right close_modal"  data-dismiss="modal">Đóng</button>
					        </form>
						</div>
					</div>
				</div>

			</div>
		</div>
		<a href="/" class="btn btn-sm btn-info mt-3 ml-3">Tiếp tục mua sắm!</a>
	</div>
	@else
	<div class="col-12 row m-0 justify-content-center">
		<div class="text-center"><h5>Giỏ hàng của bạn đang trống!</h5><a href="/" class="btn btn-sm btn-info">Mua ngay</a></div>
	</div>
	@endif
	@else
	@if($getAllBuyNow->isNotEmpty())
	<h5 class="pl-4" style="border-bottom: 1px solid #ebebeb">Giỏ hàng( {{count($getAllBuyNow) }} sản phẩm)</h5>
	<div class="col-12 row m-0">
		<div class="col-8 m-0">
			@php $total = 0 @endphp
			@foreach($getAllBuyNow as $key => $value)
				@php $total += $value->quatity * $value->price @endphp
				<div class="col-12 row m-0 p-0" style="border-bottom: 1px solid #ebebeb">
					<a href="{{route('proInfo',['name' => $value->product->url_product, 'id' => $value->product_id])}}">
						<img class="img-slider" src="{{asset('Image/productImage/thumbnailImage/'.$value->thumbnail_image)}}">
					</a>
					<div class="info pl-4 pt-2">
						<p><a class="pb-3" style="font-size: 20px;" 
							href="{{route('proInfo',['name' => $value->product->url_product, 'id' => $value->product_id])}}">
							{{$value->product->product_name}}
						</a></p>
						<strong>Giá: </strong>	<b class="change">{{$value->product->price}}</b>
						<div class="form-inline p-0">
							<label style="font-size: 15px;" class="m-0" for="{{$value->id}}"><strong>Số lượng: </strong></label>
							<input type="number" data-name="{{$value->product->product_name}}" value="{{$value->quatity}}" min="1" max="{{$value->product->quatity}}" class="form-control ml-3 mb-3" id="{{$value->id}}">
						</div>
						<p class="del_bought">
							<span class="del" data-id="{{$value->id}}">Xoá</span>
						</p>
					</div>
				</div>
			@endforeach
		</div>
		<div class="col-4 pt-4" style="border-left: 1px solid #ebebeb">
			{{-- <form action=""> --}}
				<strong>Giá tạm tính: </strong>	<b class="change total">{{$total}}</b>
				<p class="mb-3"><strong>Thuế: </strong>0</p>
				<strong>Tổng tiền: </strong>	<b class="change total">{{$total}}</b>
				<div class="col-12 pt-3" style="border-top: 1px solid #ebebeb">
					<a href="{{route('checkout')}}" class="btn btn-sm btn-danger">Tiến hành đặt hàng</a>
				</div>
			{{-- </form> --}}
		</div>
		<a href="/" class="btn btn-sm btn-info mt-3 ml-3">Tiếp tục mua sắm!</a>
	</div>
	@else
		<div class="col-12 row m-0 justify-content-center">
			<div class="text-center"><h5>Giỏ hàng của bạn đang trống!</h5><a href="/" class="btn btn-sm btn-info">Mua ngay</a></div>
		</div>
	@endif
	@endif
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		$('.change').each(function(index){
			let money = $(this).html();
			$(this).html(money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+ ' ₫');
		});

		$(document).on('change','input[type=number]',function(){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			
			let quatity 	= $(this).val();
			let id 			= $(this).attr('id');
			let maxQuatity  = $(this).attr('max');
			let prdName 	= $(this).data('name');

			$.ajax({

				url:'/UcartQuatity',
				type:'POST',
				data:{
					"id": id,
					"quatity": quatity,
					"maxQuatity": maxQuatity,
				},
				success: function(result){
					if (!result) {
						toastr.info('', 'Sản phẩm ' +prdName+ ' chỉ còn '+maxQuatity+' sản phẩm!');
						$('input[type=number]').val(maxQuatity);
					}
					else{
						$('.total').html(result	.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+ ' ₫');
					}
				}
			});
		});

		$(document).on('click','.del',function(){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			let id 		 = $(this).data('id');
			let menuCart = $('#menuCart').html();
			$.ajax({

				url:'/DcartPrd',
				type:'POST',
				data:{
					"id": id,
				},
				success: function(result){
					location.reload();
				}
			});
		});
	});
</script>
@endsection