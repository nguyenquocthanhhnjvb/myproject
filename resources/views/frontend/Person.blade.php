@extends('frontend.UserInfo')
@section('contentinfo')
@if($person->isNotEmpty())
<form action="{{route('person')}}" method="POST" enctype="multipart/form-data">
	@csrf
	<div class=" col-12 m-0">
		<h5 class="text-center mb-3">Thông tin tài khoản</h5>
		<div class="form-group row">
			<label for="name" class="col-3 p-0 col-form-label">Họ Tên:</label>
			<div class="col-9">
				<input type="text" 

				value="{{empty(old('name')) ? $person[0]->name : old('name')}}" 

				class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Ví dụ: Nguyễn Văn A" required="">
				@error('name')
				<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
					<div>{{$message}}</div>
				</span>
				@enderror
			</div>
		</div>
		<div class="form-group row">
			<label for="phone" class="col-3 p-0 col-form-label">Điện thoại di động:</label>
			<div class="col-9">
				<input type="text" 

				value="{{empty(old('phone')) ? $person[0]->phone : old('phone')}}" 

				class="form-control @error('phone') is-invalid @enderror" name="phone" id="phone" placeholder="Ví dụ: 0123456789" required="">
				@error('phone')
				<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
					<div>{{$message}}</div>
				</span>
				@enderror
			</div>
		</div>
		<div class="form-group row">
			<label for="address" class="col-3 p-0 form-label">Địa chỉ:</label>
			<div class="col-9">
				<input type="text" 

				value="{{empty(old('address')) ? $person[0]->address : old('address')}}" 

				class="form-control @error('address') is-invalid @enderror " name="address" id="address" placeholder="Ví dụ: 58 Tố Hữu" required="">
				@error('address')
				<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
					<div>{{$message}}</div>
				</span>
				@enderror
			</div>
		</div>
		<div class="form-group row">
			<label for="note" class="col-3 p-0 col-form-label">Ảnh đại diện:<br><span style="font-size: 12px;color: red">Click vào ảnh để thay đổi!</span></label>
			<div class="col-sm-6 text-center">
				<img style="max-width: 200px;height: auto;" class="avatar" 
				src="{{asset('image/authorImage/'.$person[0]->avatar_person)}}"
				alt="{{$person[0]->avatar_person}}">
			</div>
			<div class="col-sm-6">
				<input type="file" id="avt" accept="image/*" hidden="true" class="sr-onl fileupload" name="avatar">
			</div>
			@error('avatar')
			<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
				<div class="text-danger mt-3" role="alert">{{$message}}</div>
			</span>
			@enderror
		</div>
		<div class="col-12 row justify-content-center"><button class="btn btn-sm btn-info">Cập nhật thông tin</button></div>
	</div>
</form>
@else
<form action="{{route('person')}}" method="POST" enctype="multipart/form-data">
	@csrf
	<div class=" col-12 m-0">
		<h5 class="text-center mb-3">Thông tin tài khoản</h5>
		<div class="form-group row">
			<label for="name" class="col-3 p-0 col-form-label">Họ Tên:</label>
			<div class="col-9">
				<input type="text" 

				value="{{old('name')}}" 

				class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Ví dụ: Nguyễn Văn A" required="">
				@error('name')
				<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
					<div>{{$message}}</div>
				</span>
				@enderror
			</div>
		</div>
		<div class="form-group row">
			<label for="phone" class="col-3 p-0 col-form-label">Điện thoại di động:</label>
			<div class="col-9">
				<input type="text" 

				value="{{old('phone')}}" 

				class="form-control @error('phone') is-invalid @enderror" name="phone" id="phone" placeholder="Ví dụ: 0123456789" required="">
				@error('phone')
				<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
					<div>{{$message}}</div>
				</span>
				@enderror
			</div>
		</div>
		<div class="form-group row">
			<label for="address" class="col-3 p-0 form-label">Địa chỉ:</label>
			<div class="col-9">
				<input type="text" 

				value="{{old('address')}}" 

				class="form-control @error('address') is-invalid @enderror " name="address" id="address" placeholder="Ví dụ: 58 Tố Hữu" required="">
				@error('address')
				<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
					<div>{{$message}}</div>
				</span>
				@enderror
			</div>
		</div>
		<div class="form-group row">
			<label for="note" class="col-3 p-0 col-form-label">Ảnh đại diện:<br><span style="font-size: 12px;color: red">Click vào ảnh để thay đổi!</span></label>
			<div class="col-sm-6 text-center">
				<img style="max-width: 200px;height: auto;" class="avatar" 
				src="{{asset('image/authorImage/default.jpg')}}"
				title="Click để thay đổi ảnh!">
			</div>
			<div class="col-sm-6">
				<input type="file" id="avt" accept="image/*" hidden="true" class="sr-onl fileupload" name="avatar">
			</div>
			@error('avatar')
			<span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
				<div class="text-danger mt-3" role="alert">{{$message}}</div>
			</span>
			@enderror
		</div>
		<div class="col-12 row justify-content-center"><button class="btn btn-sm btn-info">Cập nhật thông tin</button></div>
	</div>
</form>
@endif
@endsection
@section('script')
	<script type="text/javascript">
		$('#person').addClass('active');
	</script>
@endsection