@extends('frontend.UserInfo')
@section('contentinfo')
@if($orders->isNotEmpty())
	<h5 class="text-center mb-3">Danh sách đơn hàng</h5>
		<div class="col-12 p-0">
			<form action="">
				@csrf
				<table id="ordertable" class="table table-striped table-bordered table-hover mb-0">
			        <thead>
			            <tr>
			                <th class="text-center" style="width: 40px">STT</th>
			                <th class="text-center">Ngày lập</th>
			                <th class="text-center">Thông tin giao hàng</th>
			                <th class="text-center">Số sản phẩm</th>
			                <th class="text-center">Tổng tiền</th>
			                <th class="text-center">Thanh toán</th>
			                <th class="text-center">Trạng thái</th>
			                <th class="text-center">Tác vụ</th>
			            </tr>
			        </thead>
			        <tbody>
			        	@foreach($orders as $key => $order)
				        	<tr>
				            	<td class="text-center">{{$key+1}}</td>
				            	<td class="text-center">{{$order->created_at}}</td>
								@if(empty($order->note))
					            	<td class="text-center">
					            		{{$order->receiver_name}} - {{$order->receiver_phone}} - {{$order->ship_address}}
					            	</td>
								@else
									<td class="text-center">
					            		{{$order->receiver_name}} - {{$order->receiver_phone}} - {{$order->ship_address}}<br>
					            		(<b>Ghi chú: {{$order->note}}</b>)
					            	</td>
								@endif
				            	<td class="text-center">{{count($order->detail)}}</td>
				            	<td class="text-center">
				            		<strong class="change">{{$order->total}}</strong>
				            	</td>
				            	<td class="text-center">
				          			@if($order->checkout == 'noactive')
										Chưa thanh toán
				          			@else
				          				Đã thanh toán
				          			@endif
				            	</td>
				            	<td class="text-center">
				            		@if($order->status == 'noactive')
										<span class="badge badge-warning p-2">Chờ xác nhận</span>
				            		@elseif($order->status == 'cancel')
										<span class="badge badge-danger p-2">Đã huỷ</span>
									@elseif($order->status == 'active')
										<span class="badge badge-info p-2">Đã xác nhận</span>
									@elseif($order->status == 'ship')
										<span class="badge badge-primary">Chờ nhận hàng</span>
									@else
										<span class="badge badge-primary">Đã hoàn thành</span>
				            		@endif
				            	</td>
				            	<td class="text-center">
				          			<a href="{{route('orderdetail',['id' => $order->id])}}" class="btn btn-sm btn-info " title="Xem chi tiết">
				          				<i class="fa fa-eye" aria-hidden="true"></i>
				          			</a>
				          			@if($order->status != 'ship' && $order->status != 'cancel' && $order->status != 'done' && $order->checkout != 'active')
										<button class="btn btn-sm btn-warning button" type="submit" 
											formaction="{{route('cancel',['id' => $order->id])}}" 
											formmethod="post" name="cancel"
											onclick="return confirm('Bạn muốn huỷ đơn hàng này?');" 
											title="Huỷ đơn">
											<i class="fa fa-times" aria-hidden="true"></i>
										</button>
									@endif
				            	</td>
				        	</tr>
				        @endforeach
			    	</tbody>
		    	</table>
	    	</form>
		</div>
		<div class="box-footer pt-3 clearfix">
			<div class="row">
				<div class="col-md-7">
					<!-- Pagination -->
					<div class="float-right">
						<div class="no-margin text-center">
							{{$orders->links()}}
						</div>
					</div>
					<!-- / End Pagination -->
				</div>
			</div>
		</div>
	@else
		<div class="col-12 row m-0 justify-content-center">
			<div class="text-center">
				<h5>Bạn chưa có đơn đặt hàng nào!</h5>
				<a href="/" class="btn btn-sm btn-info">Mua ngay</a>
			</div>
		</div>
	@endif
@endsection
@section('script')
<script type="text/javascript">
	$('#order').addClass('active');
	$('.change').each(function(index){
		let money = $(this).html();
		$(this).html(money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+ ' ₫');
	});
</script>
@endsection