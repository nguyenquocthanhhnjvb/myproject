@extends('layout.Layout-FrontEnd.frontend')
@section('title', 'KITE | Chi tiết sản phẩm')
@section('content')
<div class="col-12 row m-0 block">
	<div class="col-12 row m-0 p-0" style="border-bottom: 1px solid #ebebeb">
		<div class="col-5 imgpro">
			<div class="col-12 p-0">
				@foreach($productinfo['product_images'] as $prdinfoKey => $value)
				@if($value['status'] == 'avatar')
				<a href="{{asset('Image/productImage/thumbnailMedium/'.$value['thumbnail_image'])}}" title="Click xem ảnh sản phẩm." data-gallery="">
					<img src="{{asset('Image/productImage/thumbnailMedium/'.$value['thumbnail_image'])}}">
				</a>
				@endif
				@endforeach
			</div>
			<div class="lightBoxGallery">
				@foreach($productinfo['product_images'] as $prdKey => $listImg)
				@if($listImg['status'] != 'avatar')
				<a href="{{asset('Image/productImage/'.$listImg['image'])}}" title="Click xem ảnh sản phẩm." data-gallery="">
					<img src="{{asset('Image/productImage/thumbnailImage/'.$listImg['thumbnail_image'])}}">
				</a>
				@endif
				@endforeach
			</div>
		</div>
		<div class="col-7 pt-4 pb-3 prodinfo">
			<h4>{{$productinfo['product_name']}}</h4>
			<div class="col-12 p-0 m-0">
				@if(!empty($productinfo['averageCmt']))
				@if($productinfo['averageCmt']['topAverage'] == 5)
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				@elseif($productinfo['averageCmt']['topAverage'] == 4)
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star-o"></span>
				@elseif($productinfo['averageCmt']['topAverage'] == 3)
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				@elseif($productinfo['averageCmt']['topAverage'] == 2)
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				@else
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				@endif
				<a href="#cmt" class="totalrate">({{$productinfo['averageCmt']['total']}} nhận xét)</a>
				@else
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<a href="#cmt" class="totalrate">(0 nhận xét)</a>
				@endif
			</div>
			<div class="col-12 p-0 pt-3 pb-3 m-0" style="border-bottom: 1px solid #ebebeb">
				<span class="pr-2">Tác giả: </span><a class="mr-3" href="#">{{$productinfo['author']['author_name']}}</a>
				<span class="pr-2">Nhà xuất bản: </span><a href="#">{{$productinfo['company']['company_name']}}</a>
			</div>
			<p><b>Giá: </b>{{$productinfo['price']}}</p>
			<p><b>Thể loại: </b>{{$productinfo['subcategorys']['subcategory_name']}}</p>
			<p><b>Số trang: </b>{{$productinfo['page_number']}}</p>
			<p style="border-bottom: 1px solid #ebebeb;" class="m-0"><b>Sản phẩm có sẵn: </b>{{$productinfo['quatity']}}</p>
			<div class="col-12 m-0 p-0 row">
				<div class="form-check col-3">
					<label style="font-size: 15px;" class="m-0" for="quatity">Số lượng</label>
					<input type="number" value="1" min="1" max="{{$productinfo['quatity']}}" class="form-control" id="quatity">
				</div>
				<div class="col-3 buybtn"><button id="addPrd" value="{{$productinfo['id']}}" class="btn btn-danger w-100"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Chọn mua</button></div>
			</div>
		</div>
	</div>
	<div class="col-12 pt-4 pl-4">
		<h3>Thông tin mô tả</h3>
		<p>{!!$productinfo['description']!!}</p>
	</div>
</div>
<h3 class="title-pro">Sản phẩm cùng loại</h3>
<div class="col-12 m-0 block">
	<div class="col-12 row m-0 p-0" id="prd-Cate">
		@foreach($productinfo['prdCategory'] as $prdCateKey => $prdCategory)
		@if($prdCateKey % 2 == 0)
		<div  data-wow-delay="0.3s" data-wow-offset="10" class="wow fadeInDown product">
			<div class="overlay-outer">
				<a class="hover-imgPRD" href="{{route('proInfo',['name' => $prdCategory['url_product'], 'id' => $prdCategory['id']])}}">
					<div class="overlay"></div>
					<img class="img-slider" src="{{asset('Image/productImage/thumbnailImage/'.$prdCategory['thumbnailImage'])}}">
					<div class="content-details fadeIn_bottom">
						<p class="content-title">Xem chi tiết</p>
					</div>
				</a>
			</div>
			<div class="col-12 p-0 pt-2 pb-2 product-footer">
				<a href="{{route('proInfo',['name' => $prdCategory['url_product'], 'id' => $prdCategory['id']])}}" title="Chi tiết sản phẩm">
					<h4>{{$prdCategory['product_name']}}</h4>
				</a>
				<div class=" col-12 p-0 m-0 row">
					<span class="col-6 p-0">{{$prdCategory['price']}} ₫</span>
					<p class=" col-6 p-0 m-0 text-right" style="font-size: 14px">
						@if(!empty($prdCategory['averageCmt']))
						@if($prdCategory['averageCmt']['topAverage'] == 5)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						@elseif($prdCategory['averageCmt']['topAverage'] == 4)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						@elseif($prdCategory['averageCmt']['topAverage'] == 3)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@elseif($prdCategory['averageCmt']['topAverage'] == 2)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@else
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@endif
						@else
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>	
						@endif
					</p>
				</div>

			</div>
		</div>
		@else
		<div data-wow-delay="0.5s" data-wow-offset="10" class="wow fadeInUp product">
			<div class="overlay-outer">
				<a class="hover-imgPRD" href="{{route('proInfo',['name' => $prdCategory['url_product'], 'id' => $prdCategory['id']])}}">
					<div class="overlay"></div>
					<img class="img-slider" src="{{asset('Image/productImage/thumbnailImage/'.$prdCategory['thumbnailImage'])}}">
					<div class="content-details fadeIn_bottom">
						<p class="content-title">Xem chi tiết</p>
					</div>
				</a>
			</div>
			<div class="col-12 p-0 pt-2 pb-2 product-footer">
				<a href="{{route('proInfo',['name' => $prdCategory['url_product'], 'id' => $prdCategory['id']])}}" title="Chi tiết sản phẩm">
					<h4>{{$prdCategory['product_name']}}</h4>
				</a>
				<div class=" col-12 p-0 m-0 row">
					<span class="col-6 p-0">{{$prdCategory['price']}} ₫</span>
					<p class=" col-6 p-0 m-0 text-right" style="font-size: 14px">
						@if(isset($prdCategory['averageCmt']))
						@if($prdCategory['averageCmt']['topAverage'] == 5)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						@elseif($prdCategory['averageCmt']['topAverage'] == 4)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						@elseif($prdCategory['averageCmt']['topAverage'] == 3)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@elseif($prdCategory['averageCmt']['topAverage'] == 2)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@else
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@endif
						@else
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>	
						@endif
					</p>
				</div>

			</div>
		</div>
		@endif
		@endforeach
	</div>
</div>
<h3 id="cmt" class="title-pro">Khách hàng nhận xét</h3>
<div class="col-12 row m-0 block">
	<div class="col-12 row m-0 p-0" style="border-bottom: 1px solid #ebebeb">
		<div class="col-5" id="rateScore">
			@if(!empty($productinfo['averageCmt']))
			<div class="col-12 p-0 m-0">
				<h6>Đánh giá trung bình</h6>
				<p>{{$productinfo['averageCmt']['topAverage']}}/5</p>
				@if($productinfo['averageCmt']['topAverage'] == 5)
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				@elseif($productinfo['averageCmt']['topAverage'] == 4)
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star-o"></span>
				@elseif($productinfo['averageCmt']['topAverage'] == 3)
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				@elseif($productinfo['averageCmt']['topAverage'] == 2)
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				@else
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				@endif
				<span>({{$productinfo['averageCmt']['total']}} nhận xét)</span>
			</div>
			<div class="col-12 p-0 m-0">
				<div class="star-progress-bar">
					<p><span>5 <i class="fa fa-star" aria-hidden="true"></i></span></p>
					<div class="progress-bar">
						<div class="progress" style="width: {{$productinfo['averageCmt']['5']}}%"></div>
					</div>
					<p><span>{{$productinfo['averageCmt']['5']}}%</span></p>
				</div>
				<div class="star-progress-bar">
					<p><span>4 <i class="fa fa-star" aria-hidden="true"></i></span></p>
					<div class="progress-bar">
						<div class="progress" style="width: {{$productinfo['averageCmt']['4']}}%"></div>
					</div>
					<p><span>{{$productinfo['averageCmt']['4']}}%</span></p>
				</div>
				<div class="star-progress-bar">
					<p><span>3 <i class="fa fa-star" aria-hidden="true"></i></span></p>
					<div class="progress-bar">
						<div class="progress" style="width: {{$productinfo['averageCmt']['3']}}%"></div>
					</div>
					<p><span>{{$productinfo['averageCmt']['3']}}%</span></p>
				</div>
				<div class="star-progress-bar">
					<p><span>2 <i class="fa fa-star" aria-hidden="true"></i></span></p>
					<div class="progress-bar">
						<div class="progress" style="width: {{$productinfo['averageCmt']['2']}}%"></div>
					</div>
					<p><span>{{$productinfo['averageCmt']['2']}}%</span></p>
				</div>
				<div class="star-progress-bar">
					<p><span>1 <i class="fa fa-star" aria-hidden="true"></i></span></p>
					<div class="progress-bar">
						<div class="progress" style="width: {{$productinfo['averageCmt']['1']}}%"></div>
					</div>
					<p><span>{{$productinfo['averageCmt']['1']}}%</span></p>
				</div>
			</div>
			@else
			<h6>Đánh giá trung bình</h6>
			<div class="col-12 p-0 m-0">
				<h6>Đánh giá trung bình</h6>
				<p>0/5</p>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span>(0 nhận xét)</span>
			</div>
			<div class="col-12 p-0 m-0">
				<div class="star-progress-bar">
					<p><span>5 <i class="fa fa-star" aria-hidden="true"></i></span></p>
					<div class="progress-bar">
						<div class="progress" style="width: 0%"></div>
					</div>
					<p><span>0%</span></p>
				</div>
				<div class="star-progress-bar">
					<p><span>4 <i class="fa fa-star" aria-hidden="true"></i></span></p>
					<div class="progress-bar">
						<div class="progress" style="width: 0%"></div>
					</div>
					<p><span>0%</span></p>
				</div>
				<div class="star-progress-bar">
					<p><span>3 <i class="fa fa-star" aria-hidden="true"></i></span></p>
					<div class="progress-bar">
						<div class="progress" style="width: 0%"></div>
					</div>
					<p><span>0%</span></p>
				</div>
				<div class="star-progress-bar">
					<p><span>2 <i class="fa fa-star" aria-hidden="true"></i></span></p>
					<div class="progress-bar">
						<div class="progress" style="width: 0%"></div>
					</div>
					<p><span>0%</span></p>
				</div>
				<div class="star-progress-bar">
					<p><span>1 <i class="fa fa-star" aria-hidden="true"></i></span></p>
					<div class="progress-bar">
						<div class="progress" style="width: 0%"></div>
					</div>
					<p><span>0%</span></p>
				</div>
			</div>
			@endif
		</div>
		<div class="col-7" id="rate">

			<form 
			@if(auth()->check())
			action="{{route('addComments',['userid' => auth()->user()->id,'prdid' => $productinfo['id']])}}" method="POST"
			@endif>
			@csrf
			<h5>Gửi nhận xét của bạn</h5>
			<p class="m-0">1. Đánh giá về sản phẩm</p>
			<div class="pl-4">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="rateStar" id="1" value="1" required>
					<label class="form-check-label" for="1">1 sao</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="rateStar" id="2" value="2">
					<label class="form-check-label" for="2">2 sao</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="rateStar" id="3" value="3">
					<label class="form-check-label" for="3">3 sao</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="rateStar" id="4" value="4">
					<label class="form-check-label" for="4">4 sao</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="rateStar" id="5" value="5">
					<label class="form-check-label" for="5">5 sao</label>
				</div>
			</div>
			<div class="form-group">
				<label for="title">2. Tiêu đề của nhận xét</label>
				<input type="text" name="rateTitle" class="form-control  @error('rateTitle') is-invalid @enderror" id="title" placeholder="Nhập tiêu đề nhận xét!" value="{{old('rateTitle')}}" required="">
				@error('rateTitle')
				<span class="invalid-feedback mt-3" role="alert">
					<div>conmemay</div>
				</span>
				@enderror
			</div>
			<div class="form-group">
				<label for="content">3. Nội dung của nhận xét</label>
				<textarea class="form-control" id="content" rows="3" name="rateContent" required placeholder="Nhận xét của bạn về sản phẩm này..."></textarea>
			</div>
			@if(!auth()->check())
			<p style="color: red;font-size: 14px;">Bạn phải đăng nhập để xử dụng tính năng này!</p>
			@else
			<button type="submit" class="mb-3 btn btn-success">Gửi nhận xét</button>
			@endif
		</form>
	</div>
</div>
@foreach($productinfo['comments'] as $cmtKey => $comment)
<div class="col-12 row m-0 p-0" style="border-bottom: 1px solid #ebebeb;">
	<div class="col-3 user-info">
		@if(!empty($comment['useravatar']))
		<div class="avatar"><img style="width: 100%" src="{{asset('Image/authorImage/'.$comment['useravatar'])}}" /></div>
		@else
		<div class="avatar"><img style="width: 100%" src="{{asset('Image/authorImage/default.jpg')}}" /></div>
		@endif
		<b>{{$comment['username']}}</b>
	</div>
	<div class="col-9 rateInfomation">
		<div class="titleInfomation">
			<p>
				@if($comment['vote'] == 5)
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				@elseif($comment['vote'] == 4)
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star-o"></span>
				@elseif($comment['vote'] == 3)
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				@elseif($comment['vote'] == 2)
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				@else
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span>
				@endif
			</p>
			<span><strong>{{$comment['title']}}</strong></span>
		</div>
		<p class="content">{{$comment['content']}}</p>
	</div>
</div>
@endforeach
</div>
@endsection
@section('script')
<script type="text/javascript">
	$('.lightBoxGallery').slick({
		slidesToShow: 5,
		slidesToScroll: 2,
		autoplay: false,
	});
	$('#prd-Cate').slick({
		slidesToShow: 5,
		slidesToScroll: 2,
		autoplay: true,
		autoplaySpeed: 2200
	});

	let totalQuantity = {{$productinfo['quatity']}};


	$('#addPrd').on('click',function(e){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		e.preventDefault();
		let id = $(this).val();
		let quatity = $('#quatity').val();
		$.ajax({
			url:'/buy',
			type:'POST',
			data:{
				"id": id,
				"quatity" : quatity
			},
			success: function(result){
				if (!result) {
					toastr.info('', 'Sản phẩm "{{$productinfo['product_name']}}" chỉ còn '+totalQuantity+' sản phẩm!');
				}
				else{
					toastr.success('', 'Thêm vào giỏ hàng thành công!');
					$('#menuCart').html('('+(result)+')');
				}
			}
		});
	});
</script>
@endsection