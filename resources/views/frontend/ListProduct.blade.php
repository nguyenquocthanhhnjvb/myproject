@extends('layout.Layout-FrontEnd.frontend')
@section('title', 'KITE | Danh sách sản phẩm')
@section('content')
<div class="col-12 row m-0 block">
	<div class="col-3">
		<div id="accordion">
			<nav class="navbar bg-light">
				<ul class="navbar-nav w-100">
					@foreach($data as $menuKey => $value)
					@if($menuKey == 'cate')
					<li class="nav-item">
						<a class="nav-link parent" href="#" data-toggle="collapse" data-target="#Cate" aria-expanded="false" aria-controls="Cate">Danh mục sản phẩm <i class="fa fa-plus float-right" aria-hidden="true"></i></a>
						<div id="Cate" class="navbar-nav collapse show"  data-parent="#accordion">
							@foreach($value as $cateKey => $cate)
							<p class="m-0 dropdown-item" href="#"></i>{{$cate['category_name']}}</p>
							@foreach($cate['subcategorys'] as $subKey => $sub)
							<a class="dropdown-item @if($data['name'] == $sub['url_subcategory_name']) active @endif" 
							href="{{route('proListGetWhere',
							[	
							'where' => 'cate',
							'name' 	=> $sub['url_subcategory_name'],
							'id' 	=> $sub['id'],
							'key'	=> $subKey,
							] )}}">
							{{$sub['subcategory_name']}}
						</a>
						@endforeach
						@endforeach
					</div>
				</li>
				@else
				@if($menuKey == 'author')
				<li class="nav-item">
					<a class="nav-link parent" href="#" data-toggle="collapse" data-target="#Author" aria-expanded="false" aria-controls="Author">Tác giả <i class="fa fa-plus float-right" aria-hidden="true"></i></a>
					<div id="Author" class="navbar-nav collapse"  data-parent="#accordion">
						@foreach($value as $authorKey => $author)
						<a class="dropdown-item @if($data['name'] == $author['url_author']) active @endif" 
						href="{{route('proListGetWhere',
						[	
						'where' => 'author',
						'name' 	=> $author['url_author'],
						'id' 	=> $author['id'],
						'key'	=> $authorKey,
						] )}}">
						{{$author['author_name']}}
					</a>
					@endforeach
				</div>
			</li>
			@else
			@if($menuKey == 'comp')	
			<li class="nav-item">
				<a class="nav-link parent" href="#" data-toggle="collapse" data-target="#Comp" aria-expanded="false" aria-controls="Comp">Nhà xuất bản <i class="fa fa-plus float-right" aria-hidden="true"></i></a>
				<div id="Comp" class="navbar-nav collapse"  data-parent="#accordion">
					@foreach($value as $compKey => $comp)
					<a class="dropdown-item @if($data['name'] == $comp['url_company']) active @endif" 
					href="{{route('proListGetWhere',
					[	
					'where' => 'comp',
					'name' 	=> $comp['url_company'],
					'id' 	=> $comp['id'],
					'key'	=> $compKey,
					] )}}">
					{{$comp['company_name']}}
				</a>
				@endforeach
			</div>
		</li>
		@endif
		@endif
		@endif
		@endforeach
	</ul>
</nav>
</div>
</div>
<div class="col-9  p-0">
	<h5 id="title" class="text-center text-uppercase pt-3">Danh sách sản phẩm</h5>
	<div class="col-12 m-0 pt-0 pb-0 block">
		<div class="col-12 row m-0 p-0" id="list-prd">
			@if($data['product']->isNotEmpty())
			@if($data['where'] == 'author_id')
			<div class="col-12 author-info-block row m-0">
				<div data-wow-delay="0.5s" data-wow-offset="10" class="col-3 author-img wow fadeInDown" style="background-image: url({{asset('Image/authorImage/'.$data['author'][$data['key']]['avatar_image'])}});"></div>
				<div data-wow-delay="0.7s" data-wow-offset="10" class="col-9 author-info wow rotateIn">
					<p style="max-height: 100%;overflow: auto;">{{$data['author'][$data['key']]['author_info']}}</p>
				</div>
			</div>
			<h6 class="col-12 mt-3 mb-3 text-center">Sản phẩm tiêu biểu</h6>
			@foreach($data['product'] as $prdKey => $prd)
				<div data-wow-delay="0.7s" data-wow-offset="10" class="wow rollIn mb-3 product">
					<div class="overlay-outer">
						<a class="hover-imgPRD" href="{{route('proInfo',['name' => $prd['url_product'],'id' => $prd['id'] ])}}">
							<div class="overlay"></div>
							<img class="img-slider" src="{{asset('Image/productImage/thumbnailImage/'.$prd['thumbnail_image'])}}">
							<div class="content-details fadeIn_bottom">
								<p class="content-title">Xem chi tiết</p>
							</div>
						</a>
					</div>
					<div class="col-12 p-0 pt-2 pb-2 product-footer">
						<a href="" title="Chi tiết sản phẩm">
							<h4>{{$prd['product_name']}}</h4>
						</a>
						<div class=" col-12 p-0 m-0 row">
							<span class="col-6 p-0" style="font-size: 14px">{{$prd['price']}}</span>
							<p class=" col-6 p-0 m-0 text-right" style="font-size: 12px">
								<span class="fa fa-star-o"></span>
								<span class="fa fa-star-o"></span>
								<span class="fa fa-star-o"></span>
								<span class="fa fa-star-o"></span>
								<span class="fa fa-star-o"></span>
							</p>
						</div>
					</div>
				</div>
			@endforeach
			@else
			@foreach($data['product'] as $prdKey => $prd)
			<div data-wow-delay="0.7s" data-wow-offset="10" class="wow rollIn mb-3 product">
				<div class="overlay-outer">
					<a class="hover-imgPRD" href="{{route('proInfo',['name' => $prd['url_product'],'id' => $prd['id'] ])}}">
						<div class="overlay"></div>
						<img class="img-slider" src="{{asset('Image/productImage/thumbnailImage/'.$prd['thumbnail_image'])}}">
						<div class="content-details fadeIn_bottom">
							<p class="content-title">Xem chi tiết</p>
						</div>
					</a>
				</div>
				<div class="col-12 p-0 pt-2 pb-2 product-footer">
					<a href="" title="Chi tiết sản phẩm">
						<h4>{{$prd['product_name']}}</h4>
					</a>
					<div class=" col-12 p-0 m-0 row">
						<span class="col-6 p-0" style="font-size: 14px">{{$prd['price']}}</span>
						<p class=" col-6 p-0 m-0 text-right" style="font-size: 12px">
							<span class="fa fa-star-o"></span>
							<span class="fa fa-star-o"></span>
							<span class="fa fa-star-o"></span>
							<span class="fa fa-star-o"></span>
							<span class="fa fa-star-o"></span>
						</p>
					</div>
				</div>
			</div>
			@endforeach
			@endif
			@else
			<h5 class="text-center">Chưa có sản phẩm!</h5>
			@endif
		</div>
		<div class="box-footer clearfix">
			<div class="row">
				<div class="col-md-7">
					<!-- Pagination -->
					<div class="float-right">
						<div class="no-margin text-center">
							{{$data['product']->links()}}
						</div>
					</div>
					<!-- / End Pagination -->
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		$('div.show').parent().find('a').first().attr("aria-expanded","true");
		if($('a.dropdown-item').hasClass('active')){
			$('div.show').parent().find('a').first().attr("aria-expanded","false");
			$('div.show').removeClass('show');
			$('a.dropdown-item.active').parent('div').toggleClass("show").parent().find('a').first().attr("aria-expanded","true");
			$('#title').html($('a.dropdown-item.active').html());
		}
	})
</script>
@endsection