<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" value="{{ csrf_token() }}">
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <Example></Example>
            <home></home>
        </div>

        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>