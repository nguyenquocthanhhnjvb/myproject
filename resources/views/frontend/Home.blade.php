@extends('layout.Layout-FrontEnd.frontend')
@section('title', 'KITE | Trang chủ')
@section('header')
<div class="col-12 p-0">
	<section class="hero-wrap hero-wrap-2" style="background-image:url({{asset('Image/frontend/bg_1.jpg')}});">
		<div class="overlay"></div>
		<div class="container">
			<div class="row slider-text justify-content-center align-items-center">
				<div data-wow-delay="1.3s" class="col-md-7 col-sm-12 text-center ftco-animate ftco-animated wow fadeInUp">
					<h1 class="mb-3 bread">Welcome to KITE</h1>
					<p class="breadcrumbs"><span class="mr-2"><a href="\">Home</a></span> <span>Page</span></p>
				</div>

			</div>
		</div>
	</section>
</div>
@endsection
@section('content')

<h3 data-wow-delay="0.3s" data-wow-offset="10" class="title-pro wow fadeInDown">Đọc gì hôm nay?</h3>
<div class="col-12 m-0 block">
	<div class="col-12 row m-0 p-0" id="slick-item">
		@if(!empty($data['product']))
		@foreach($data['product'] as $proKey => $product)
		@if($proKey % 2 == 0)
		<div  data-wow-delay="0.3s" data-wow-offset="10" class="wow fadeInDown product">
			<div class="overlay-outer">
				<a class="hover-imgPRD" href="{{route('proInfo',['name' => $product['url_product'], 'id' => $product['id']])}}">
					<div class="overlay"></div>
					<img class="img-slider" src="{{asset('Image/productImage/thumbnailImage/'.$product['thumbnailImage'])}}">
					<div class="content-details fadeIn_bottom">
						<p class="content-title">Xem chi tiết</p>
					</div>
				</a>
			</div>
			<div class="col-12 p-0 pt-2 pb-2 product-footer">
				<a href="{{route('proInfo',['name' => $product['url_product'], 'id' => $product['id']])}}" title="Chi tiết sản phẩm">
					<h4>{{$product['product_name']}}</h4>
				</a>
				<div class=" col-12 p-0 m-0 row">
					<span class="col-6 p-0">{{$product['price']}} ₫</span>
					<p class=" col-6 p-0 m-0 text-right" style="font-size: 14px">
						@if(!empty($product['averageCmt']))
						@if($product['averageCmt']['topAverage'] == 5)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						@elseif($product['averageCmt']['topAverage'] == 4)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						@elseif($product['averageCmt']['topAverage'] == 3)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@elseif($product['averageCmt']['topAverage'] == 2)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@else
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@endif
						@else
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>	
						@endif
					</p>
				</div>
			</div>
		</div>
		@else
		<div data-wow-delay="0.3s" data-wow-offset="10" class="wow fadeInUp product">
			<div class="overlay-outer">
				<a class="hover-imgPRD" href="{{route('proInfo',['name' => $product['url_product'], 'id' => $product['id']])}}">
					<div class="overlay"></div>
					<img class="img-slider" src="{{asset('Image/productImage/thumbnailImage/'.$product['thumbnailImage'])}}">
					<div class="content-details fadeIn_bottom">
						<p class="content-title">Xem chi tiết</p>
					</div>
				</a>
			</div>
			<div class="col-12 p-0 pt-2 pb-2 product-footer">
				<a href="{{route('proInfo',['name' => $product['url_product'], 'id' => $product['id']])}}" title="Chi tiết sản phẩm">
					<h4>{{$product['product_name']}}</h4>
				</a>
				<div class=" col-12 p-0 m-0 row">
					<span class="col-6 p-0">{{$product['price']}} ₫</span>
					<p class=" col-6 p-0 m-0 text-right" style="font-size: 14px">
						@if(!empty($product['averageCmt']))
						@if($product['averageCmt']['topAverage'] == 5)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						@elseif($product['averageCmt']['topAverage'] == 4)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						@elseif($product['averageCmt']['topAverage'] == 3)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@elseif($product['averageCmt']['topAverage'] == 2)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@else
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@endif
						@else
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>	
						@endif
					</p>
				</div>
			</div>
		</div>
		@endif
		@endforeach
		@endif
	</div>
</div>


<h3 data-wow-delay="0.5s" class="title-pro wow fadeInDown">Sách hay</h3>

<div class="col-12 m-0 block">
	<div class="col-12 row m-0 p-0" id="list-prd">
		@if(!empty($data['product']))
		@php $settime = 1.2; @endphp
		@foreach($data['product'] as $proKey2 => $listprd)
		@if($proKey2 == 5) @php $settime = 1.2 @endphp @endif
		<div data-wow-delay="{{$settime}}s" data-wow-offset="10" class="wow rollIn product">
			<div class="overlay-outer">
				<a class="hover-imgPRD" href="{{route('proInfo',['name' => $listprd['url_product'], 'id' => $listprd['id']])}}">
					<div class="overlay"></div>
					<img class="img-slider" src="{{asset('Image/productImage/thumbnailImage/'.$listprd['thumbnailImage'])}}">
					<div class="content-details fadeIn_bottom">
						<p class="content-title">Xem chi tiết</p>
					</div>
				</a>
			</div>
			<div class="col-12 p-0 pt-2 pb-2 product-footer">
				<a href="{{route('proInfo',['name' => $listprd['url_product'], 'id' => $listprd['id']])}}" title="Chi tiết sản phẩm">
					<h4>{{$listprd['product_name']}}</h4>
				</a>
				<div class=" col-12 p-0 m-0 row">
					<span class="col-6 p-0">{{$product['price']}} ₫</span>
					<p class=" col-6 p-0 m-0 text-right" style="font-size: 14px">
					@if(!empty($listprd['averageCmt']))
						@if($listprd['averageCmt']['topAverage'] == 5)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						@elseif($listprd['averageCmt']['topAverage'] == 4)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						@elseif($listprd['averageCmt']['topAverage'] == 3)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@elseif($listprd['averageCmt']['topAverage'] == 2)
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@else
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						@endif
						@else
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>
						<span class="fa fa-star-o"></span>	
						@endif
					</p>
				</div>
			</div>
		</div>
		@php $settime = $settime - 0.2; @endphp
		@endforeach
		@endif
	</div>
	<div class=" text-center"><a class="view-more" href="{{route('proList')}}">Xem thêm</a></div>
</div>
<h3 data-wow-delay="0.3s" data-wow-offset="10" class="title-pro wow fadeInDown">Tác giả nổi tiếng</h3>
<div class="col-12 m-0 block">
	<div class="author-info-block row m-0">
		<div data-wow-delay="0.5s" data-wow-offset="10" class="col-3 author-img wow fadeInDown" style="background-image: url({{asset('Image/authorImage/'.$data['author'][0]['avatar_image'])}});"></div>
		<div data-wow-delay="0.7s" data-wow-offset="10" class="col-9 author-info wow rotateIn">
			<h4>{{$data['author'][0]['author_name']}}</h4>
			<p>{{$data['author'][0]['author_info']}}</p>
			<div class=" text-center"><a class="view-more" href="#">Chi tiết</a></div>
		</div>
	</div>
	<hr>
	<div class="author-list-block row m-0">
		@if(!empty($data['product']))
		@foreach($data['author'] as $authorKey => $author)
		@if($authorKey != 0)
		@if($authorKey % 2 != 0)
		<div data-wow-delay="0.7s" data-wow-offset="10" class="col-4 p-0 wow fadeInLeft">
			<div class="imgAuthor-hover">
				<a href="#" title="Chi tiết tác giả">
					<figure class="rounded-circle"><img src="{{asset('Image/authorImage/'.$author['avatar_image'])}}" /></figure>
				</a>
			</div>
			<h4>{{$author['author_name']}}</h4>
			<div class="author-info-mini">
				<p>{{$author['author_info']}}</p>
			</div>
		</div>
		@else
		<div data-wow-delay="0.9s" data-wow-offset="10" class="col-4 p-0 wow fadeInUp">
			<div class="imgAuthor-hover">
				<a href="" title="Chi tiết tác giả">
					<figure class="rounded-circle"><img src="{{asset('Image/authorImage/'.$author['avatar_image'])}}" /></figure>
				</a>
			</div>
			<h4>{{$author['author_name']}}</h4>
			<div class="author-info-mini">
				<p>{{$author['author_info']}}</p>
			</div>
		</div>
		@endif
		@endif
		@endforeach
		@endif
	</div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		$('#slick-item').slick({
			slidesToShow: 5,
			slidesToScroll: 3,
			autoplay: true,
			autoplaySpeed: 2200,
		});

		$('.author-list-block').slick({
			slidesToShow: 3,
			slidesToScroll: 2,
			autoplay: true,
			autoplaySpeed: 2700,
		});
	});
</script>
@endsection