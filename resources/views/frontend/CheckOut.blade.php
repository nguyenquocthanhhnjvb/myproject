@extends('layout.Layout-FrontEnd.frontend')
@section('title', 'KITE | Thanh toán')
@section('content')
<div class="col-12 m-0 block">
	<h6 class="text-center mb-3"><strong>Thông tin giao hàng</strong></h6>
	<div class="col-8 m-auto">
		<form action="{{route('addOrder')}}" method="POST">
			@csrf
			<div class="form-group row">
				<label for="name" class="col-3 p-0 col-form-label">Họ Tên:</label>
				<div class="col-9">
					<input type="text" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Ví dụ: Nguyễn Văn A" required="">
					@error('name')
	                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
	                        <div>{{$message}}</div>
	                    </span>
                    @enderror
				</div>
			</div>
			<div class="form-group row">
				<label for="phone" class="col-3 p-0 col-form-label">Điện thoại di động:</label>
				<div class="col-9">
					<input type="text" value="{{ old('phone') }}" class="form-control @error('phone') is-invalid @enderror" name="phone" id="phone" placeholder="Ví dụ: 0123456789" required="">
					@error('phone')
	                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
	                        <div>{{$message}}</div>
	                    </span>
                    @enderror
				</div>
			</div>
			<div class="form-group row">
				<label for="address" class="col-3 p-0 form-label">Địa chỉ giao hàng:</label>
				<div class="col-9">
					<input type="text" value="{{ old('address') }}" class="form-control @error('address') is-invalid @enderror " name="address" id="address" placeholder="Ví dụ: 58 Tố Hữu" required="">
					@error('address')
	                    <span class="invalid-feedback mt-3" style="font-size: 95%" role="alert">
	                        <div>{{$message}}</div>
	                    </span>
                    @enderror
				</div>
			</div>
			<div class="form-group row">
				<label for="note" class="col-3 p-0 col-form-label">Ghi chú:</label>
				<div class="col-9">
					<textarea class="form-control" name="note" id="note" rows="3" placeholder="Ví dụ: Giao hàng trong giờ hành chính!">{{ old('note') }}</textarea>
				</div>
			</div>
			<h6 class="text-center mb-3 mt-3"><strong>Danh sách sản phẩm</strong></h6>
			@php $total = 0; @endphp
			@foreach($getAllBuyNow as $key => $prd)
				@php $total += ($prd->price*$prd->quatity) @endphp
				<div class="row	m-0 mb-3">
					<img src="{{asset('Image/productImage/thumbnailImage/'.$prd->thumbnail_image)}}" alt="" style="max-width: 100px;height: auto;">
					<div class="pl-3"	>
						<p class="mb-2">Tên: {{$prd->product->product_name}}</p>
						<p class="mb-2">Số lượng: {{$prd->quatity}}</p>
						<p class="mb-2">Giá: <span class="change">{{$prd->price}}</span> ₫</p>
					</div>
				</div>
			@endforeach
				<input type="hidden" name="total" value="{{$total}}" >
			<h6 class="text-center mb-3 mt-3"><strong>Hình thức thanh toán</strong></h6>
			<div class=" col-12 m-0 mb-3 mt-3">
				<div class="custom-control custom-radio">
				  	<input type="radio" id="offline" name="checkout" checked="" value="offline" class="custom-control-input">
				  	<label class="custom-control-label" for="offline">Thanh toán khi nhận hàng.</label>
				</div>
				<div class="custom-control custom-radio">
				  	<input type="radio" id="online" name="checkout" value="online" class="custom-control-input">
				  	<label class="custom-control-label" for="online">Thanh toán trực tuyến (ngân hàng NCB - VNPAY)</label>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-10">
					<button type="submit" class="btn btn-primary">Thanh toán</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('script')

@endsection