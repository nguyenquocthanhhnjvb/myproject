@extends('layout.Layout-FrontEnd.frontend')
@section('title', 'KITE | Thông tin cá nhân')
@section('content')
<div class="col-12 row m-0 block">
	<div class="col-3" id="userinfo">
		<nav class="navbar bg-light p-0">
			<ul class="navbar-nav w-100">
				<li class="nav-item">
					<span class="nav-link">Tài khoản: {{auth()->user()->name}}</span>
				</li>
{{-- 				<li class="nav-item pl-3">
					<a class="nav-link" id="person" href="{{route('userinfo')}}?href=person">Thông tin tài khoản</a>
				</li> --}}
				<li class="nav-item pl-3">
					<a class="nav-link" id="order" href="{{route('userinfo')}}?href=order">Quản lý đơn hàng</a>
				</li>
				<li class="nav-item pl-3">
					<a class="nav-link" id="chat" href="{{route('userinfo')}}?href=chat">Hỏi đáp, tư vấn</a>
				</li>
				<li class="nav-item pl-3">
					<a class="nav-link" id="cmt" href="{{route('userinfo')}}?href=cmt">Nhận xét của tôi</a>
				</li>
			</ul>
		</nav>
	</div>
	<div class="col-9">
		@yield('contentinfo')
	</div>
</div>
@endsection
